/**CFile***********************************************************************

  FileName    [Expr.c]

  PackageName [fsm.sexp]

  Synopsis    [Abstraction for expression type impemented as node_ptr]

  Description []

  SeeAlso     []

  Author      [Roberto Cavada]

  Copyright   [
  This file is part of the ``fsm.sexp'' package of NuSMV version 2.
  Copyright (C) 2003 by FBK-irst.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/


#include "Expr.h"

#include "parser/symbols.h"
#include "enc/operators.h"
#include "compile/compile.h"
#include "utils/WordNumber.h"
#include "utils/error.h"

static char rcsid[] UTIL_UNUSED = "$Id: Expr.c,v 1.1.2.5.4.1.6.61 2010/02/17 19:43:41 nusmv Exp $";


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
/* Variables                                                                 */
/*---------------------------------------------------------------------------*/

/**Variable********************************************************************

  Synopsis           [This local variable is used here to simplify
  expressions when it is needed to recognize enumerative constants]

  Description        [This is an hack to avoid to pass a symbol table
  to each Expr_... functions, and to avoid accessing a global variable.
  It is set by simplifiers like Expr_resolve and Expr_simplify]

******************************************************************************/
static SymbTable_ptr expr_st = NULL;

void fsm_sexp_expr_init() { expr_st = NULL; }
void fsm_sexp_expr_quit() { expr_st = NULL; }

/* Don't change these values (used to handle NEXT untimed case and FROZEN) */
const int UNTIMED_CURRENT = -2;
const int UNTIMED_NEXT = -1;
const int UNTIMED_DONTCARE = -3;
const int _TIME_OFS = 10;

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

static Expr_ptr expr_simplify_aux ARGS((SymbTable_ptr st, Expr_ptr expr,
                                        hash_ptr hash));
static Expr_ptr expr_bool_const_to_number ARGS((const Expr_ptr a));
static Expr_ptr expr_bool_to_bool ARGS((const Expr_ptr a));
static Expr_ptr expr_bool_to_word1 ARGS((const Expr_ptr a));

static int expr_get_curr_time ARGS((SymbTable_ptr st,
                                    node_ptr expr,
                                    hash_ptr cache));

static Expr_ptr expr_timed_to_untimed ARGS((SymbTable_ptr st, Expr_ptr expr,
                                            int curr_time, boolean in_next,
                                            hash_ptr cache));

static boolean expr_is_timed_aux ARGS((Expr_ptr expr, hash_ptr cache));

static boolean expr_is_booleanizable_aux ARGS((Expr_ptr expr,
                                               const SymbTable_ptr st,
                                               boolean word_unbooleanizable,
                                               hash_ptr cache));

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Returns the true expression value]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_true()
{
  return EXPR( find_node(TRUEEXP, Nil, Nil) );
}


/**Function********************************************************************

  Synopsis           [Returns the false expression value]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_false()
{
  return EXPR( find_node(FALSEEXP, Nil, Nil) );
}


/**Function********************************************************************

  Synopsis           [Checkes whether given value is the true value]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
boolean Expr_is_true(const Expr_ptr expr)
{
  return node_get_type(expr_bool_to_bool(expr)) == TRUEEXP;
}


/**Function********************************************************************

  Synopsis           [Checkes whether given value is the false value]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
boolean Expr_is_false(const Expr_ptr expr)
{
  return node_get_type(expr_bool_to_bool(expr)) == FALSEEXP;
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise AND of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_and(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (a == Nil && b == Nil) return Expr_true();
  if (a == Nil || Expr_is_true(a))  return b;
  if (b == Nil || Expr_is_true(b))  return a;
  if (Expr_is_false(a)) return a;
  if (Expr_is_false(b)) return b;
  if (a == b)           return a;
  {
    int ta = node_get_type(a); int tb = node_get_type(b);
    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_false();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_and(WORD_NUMBER(car(a)),
                                                 WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(AND, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise AND of given operators,
  considering Nil as the true value]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_and_nil(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr result;
  Expr_ptr atmp, btmp;

  atmp = (Nil != NODE_PTR(a)) ? a : Expr_true();
  btmp = (Nil != NODE_PTR(b)) ? b : Expr_true();
  result = Expr_and(atmp, btmp);

  return result;
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise AND of all elements in the
  list]

  Description        [Performs local syntactic simplification.
  Nil value is considered as true value]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_and_from_list(node_ptr list)
{
  int type;
  if (list == Nil) return Expr_true();

  type = node_get_type(list);
  if (CONS != type && AND != type) {
    return Expr_resolve(SYMB_TABLE(NULL),
                        type, car(list), cdr(list));
  }

  /* recursive step */
  return Expr_and_nil(car(list), Expr_and_from_list(cdr(list)));
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise NOT of given operator]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_not(const Expr_ptr expr)
{
  /* boolean */
  if (Expr_is_true(expr)) return Expr_false();
  if (Expr_is_false(expr)) return Expr_true();

  {
    int ta = node_get_type(NODE_PTR(expr));
    if (NOT == ta) return EXPR(car(NODE_PTR(expr)));

    /* bitwise */
    if (ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
      return find_node(ta,
                       (node_ptr) WordNumber_not(WORD_NUMBER(car(expr))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(NOT, NODE_PTR(expr), Nil) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise OR of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_or(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a)) return a;
  if (Expr_is_true(b)) return b;
  if (Expr_is_false(a)) return b;
  if (Expr_is_false(b)) return a;
  if (a==b) return a;
  {
    int ta = node_get_type(a); int tb = node_get_type(b);

    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_true();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_or(WORD_NUMBER(car(a)),
                                                WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(OR, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise XOR of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_xor(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a)) return Expr_not(b);
  if (Expr_is_true(b)) return Expr_not(a);
  if (Expr_is_false(a)) return b;
  if (Expr_is_false(b)) return a;

  {
    int ta = node_get_type(a); int tb = node_get_type(b);
    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_true();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_xor(WORD_NUMBER(car(a)),
                                                 WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(XOR, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise XNOR of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_xnor(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a)) return b;
  if (Expr_is_true(b)) return a;
  if (Expr_is_false(a)) return Expr_not(b);
  if (Expr_is_false(b)) return Expr_not(a);

  {
    int ta = node_get_type(a); int tb = node_get_type(b);

    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_false();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_xnor(WORD_NUMBER(car(a)),
                                                  WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(XNOR, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise IFF of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_iff(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a)) return b;
  if (Expr_is_true(b)) return a;
  if (Expr_is_false(a)) return Expr_not(b);
  if (Expr_is_false(b)) return Expr_not(a);

  {
    int ta = node_get_type(a); int tb = node_get_type(b);
    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_false();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_iff(WORD_NUMBER(car(a)),
                                                 WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(IFF, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise IFF of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_iff(const SymbTable_ptr st,
                           const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a)) return b;
  if (Expr_is_true(b)) return a;
  if (Expr_is_false(a)) return Expr_not(b);
  if (Expr_is_false(b)) return Expr_not(a);

  {
    int ta = node_get_type(a); int tb = node_get_type(b);
    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return Expr_false();

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_iff(WORD_NUMBER(car(a)),
                                                 WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr at, bt;
    TypeChecker_ptr tc = SymbTable_get_type_checker(st);

    at = TypeChecker_get_expression_type(tc, a, Nil);
    bt = TypeChecker_get_expression_type(tc, b, Nil);

    if (!SymbType_is_word(at) || !SymbType_is_word(bt)) {
      /* For non word expressions A <-> A is true */
      if (a == b) {
        return Expr_true();
      }
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(IFF, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the logical/bitwise IMPLIES of given operators]

  Description        [Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_implies(const Expr_ptr a, const Expr_ptr b)
{
  /* boolean */
  if (Expr_is_true(a))  return b;
  if (Expr_is_false(a)) return Expr_true();
  if (Expr_is_true(b))  return Expr_true();
  if (Expr_is_false(b)) return Expr_not(a);

  {
    int ta = node_get_type(a); int tb = node_get_type(b);

    if ((ta == NOT && car(a) == b) ||
        (tb == NOT && car(b) == a)) return b;

    /* bitwise */
    if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
        (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
      return find_node(ta,
                       (node_ptr) WordNumber_implies(WORD_NUMBER(car(a)),
                                                     WORD_NUMBER(car(b))),
                       Nil);
    }
  }

  /* no simplification is possible */
  return Expr_or(Expr_not(a), b);
}


/**Function********************************************************************

  Synopsis           [Builds the If-Then-Else node with given operators]

  Description [Performs local syntactic simplification. 'cond' is the
  case/ite condition, 't' is the THEN expression, 'e' is the ELSE
  expression]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_ite(const Expr_ptr cond, const Expr_ptr t, const Expr_ptr e)
{
  node_ptr tmp;
  node_ptr _cond = expr_bool_to_bool(cond);

  if (Expr_is_true(_cond)) return t;
  if (Expr_is_false(_cond)) return e;

  if (t == e) return t;

  /* here Expr_true and Expr_false cannot be used, as expressions t
     and e are to be evaluated in a non boolean flavour */
  if (node_get_type(t) == FALSEEXP) {
    if (FAILURE == node_get_type(e)) {
      warning_failure_node(e);
      return Expr_not(_cond);
    }
    else {
      return Expr_and(Expr_not(_cond), e);
    }
  }
  if (node_get_type(t) == TRUEEXP) {
    if (FAILURE == node_get_type(e)) {
      warning_failure_node(e);
      return _cond;
    }
    else {
      return Expr_or(_cond, e);
    }
  }
  if (node_get_type(e) == FALSEEXP) return Expr_and(_cond, t);
  if (node_get_type(e) == TRUEEXP) return Expr_or(Expr_not(_cond), t);

  /*
     case
         cond1 : case
                   cond1 : expr1;
                   ...

                 esac;
         ...
     esac;

     simplifies into

     case
         cond1 : expr1;
         ...
     esac
  */
  if (((CASE == node_get_type(t)) ||
       (IFTHENELSE == node_get_type(t))) &&
      (_cond == car(car(t)))) {
    tmp = find_node(COLON, NODE_PTR(_cond), cdr(car(NODE_PTR(t))));
  }
  else {
    tmp = find_node(COLON, NODE_PTR(_cond), NODE_PTR(t));
  }
  return EXPR( find_node(CASE, tmp, NODE_PTR(e)) );
}


/**Function********************************************************************

  Synopsis           [Constructs a NEXT node of given expression]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_next(const Expr_ptr a)
{
  node_ptr _next = expr_bool_to_bool(a);
  int ta;

  /* boolean constant */
  if (Expr_is_true(_next) || Expr_is_false(_next)) return a;

  /* scalar constants */
  ta = node_get_type(_next);
  if (ta == NUMBER || ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
    return a;
  }

  /* a range? */
  if (ta == TWODOTS &&
      NUMBER == node_get_type(car(_next)) &&
      NUMBER == node_get_type(cdr(_next))) {
    return a;
  }

  /* enumerative? */
  if (expr_st != SYMB_TABLE(NULL) &&
      SymbTable_is_symbol_constant(expr_st, a)) {
    return a;
  }

  /* set of constants ? */
  if (expr_st != SYMB_TABLE(NULL) && UNION == node_get_type(_next)) {
    Set_t set = Set_MakeFromUnion(_next);
    boolean is_const = true;
    Set_Iterator_t iter;
    SET_FOREACH(set, iter) {
      if (!SymbTable_is_symbol_constant(expr_st,
                                        (node_ptr) Set_GetMember(set, iter))) {
        is_const = false;
        break;
      }
    }

    Set_ReleaseSet(set);
    if (is_const) return a;
  }

  /* fall back */
  return EXPR( find_node(NEXT, NODE_PTR(a), Nil)  );
}


/**Function********************************************************************

  Synopsis           [Builds the logical EQUAL of given operators]

  Description [Works with boolean, scalar and words. Performs local
  syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_equal(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_true();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_true();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_false();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    if ((ta == NOT && car(_a) == _b) ||
        (tb == NOT && car(_b) == _a)) return Expr_false();

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va == vb) ? Expr_true() : Expr_false();
    }
    /* words */
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);
      /* if both are constants => evaluate */
      if ((NUMBER_UNSIGNED_WORD == ta && NUMBER_UNSIGNED_WORD == tb) ||
          (NUMBER_SIGNED_WORD == ta && NUMBER_SIGNED_WORD == tb)) {
        WordNumber_ptr va = WORD_NUMBER(car(_a));
        WordNumber_ptr vb = WORD_NUMBER(car(_b));
        return WordNumber_equal(va, vb)
          ? Expr_true() : Expr_false();
      }
      /* go to no-simplification return */
    }
    /* enumerative? */
    else if (expr_st != SYMB_TABLE(NULL) &&
             SymbTable_is_symbol_constant(expr_st, a) &&
             SymbTable_is_symbol_constant(expr_st, b)) {
      return (a == b) ? Expr_true() : Expr_false();
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(EQUAL, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical EQUAL of given operators]

  Description [Works with boolean, scalar and words. Performs local
  syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_equal(const SymbTable_ptr st,
                             const Expr_ptr a, const Expr_ptr b)
{
  /* NOTE that we cannot use Expr_equal as there
     global expr_st is used! */
  if (a == b) return Expr_true();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_true();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_false();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    if ((ta == NOT && car(_a) == _b) ||
        (tb == NOT && car(_b) == _a)) return Expr_false();

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va == vb) ? Expr_true() : Expr_false();
    }
    /* words */
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);
      /* if both are constants => evaluate */
      if ((NUMBER_UNSIGNED_WORD == ta && NUMBER_UNSIGNED_WORD == tb) ||
          (NUMBER_SIGNED_WORD == ta && NUMBER_SIGNED_WORD == tb)) {
        WordNumber_ptr va = WORD_NUMBER(car(_a));
        WordNumber_ptr vb = WORD_NUMBER(car(_b));
        return WordNumber_equal(va, vb)
          ? Expr_true() : Expr_false();
      }
      /* go to no-simplification return */
    }
  }

  /* additional simplifications */
  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr ta, tb;
    TypeChecker_ptr tc = SymbTable_get_type_checker(st);

    /* enumerative? */
    if (SymbTable_is_symbol_constant(st, a) &&
        SymbTable_is_symbol_constant(st, b)) {
      return (a == b) ? Expr_true() : Expr_false();
    }

    /* TRUE = B --------> B */
    if (Expr_is_true(a)) {
      tb = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(tb)) {
        return b;
      }
    }
    /* A = TRUE --------> A */
    else if (Expr_is_true(b)) {
      ta = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(ta)) {
        return a;
      }
    }
    /* FALSE = B --------> !B */
    else if (Expr_is_false(a)) {
      tb = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(tb)) {
        return Expr_not(b);
      }
    }
    /* A = FALSE --------> !A */
    else if (Expr_is_false(b)) {
      ta = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(ta)) {
        return Expr_not(a);
      }
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(EQUAL, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the logical NOTEQUAL of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_notequal(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_false();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_false();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_true();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    if ((ta == NOT && car(_a) == _b) ||
        (tb == NOT && car(_b) == _a)) return Expr_true();

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va != vb) ? Expr_true() : Expr_false();
    }
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);
      /* if both are constants => evaluate */
      if ((NUMBER_UNSIGNED_WORD == ta && NUMBER_UNSIGNED_WORD == tb) ||
          (NUMBER_SIGNED_WORD == ta && NUMBER_SIGNED_WORD == tb)) {

      WordNumber_ptr va = WORD_NUMBER(car(_a));
      WordNumber_ptr vb = WORD_NUMBER(car(_b));
      return WordNumber_not_equal(va, vb)
        ? Expr_true() : Expr_false();
      }
      /* go to no-simplification return */
    }
    /* enumerative? */
    else if (expr_st != SYMB_TABLE(NULL) &&
             SymbTable_is_symbol_constant(expr_st, a) &&
             SymbTable_is_symbol_constant(expr_st, b)) {
      return (a == b) ? Expr_false() : Expr_true();
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(NOTEQUAL, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the logical NOTEQUAL of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_notequal(const SymbTable_ptr st,
                                const Expr_ptr a, const Expr_ptr b)
{
  /* NOTE that we cannot use Expr_equal as there
     global expr_st is used! */
  if (a == b) return Expr_false();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_false();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_true();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    if ((ta == NOT && car(_a) == _b) ||
        (tb == NOT && car(_b) == _a)) return Expr_true();

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va != vb) ? Expr_true() : Expr_false();
    }
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);
      /* if both are constants => evaluate */
      if ((NUMBER_UNSIGNED_WORD == ta && NUMBER_UNSIGNED_WORD == tb) ||
          (NUMBER_SIGNED_WORD == ta && NUMBER_SIGNED_WORD == tb)) {

      WordNumber_ptr va = WORD_NUMBER(car(_a));
      WordNumber_ptr vb = WORD_NUMBER(car(_b));
      return WordNumber_not_equal(va, vb)
        ? Expr_true() : Expr_false();
      }
      /* go to no-simplification return */
    }
  }

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr ta, tb;
    TypeChecker_ptr tc = SymbTable_get_type_checker(st);

    /* enumerative? */
    if (SymbTable_is_symbol_constant(st, a) &&
        SymbTable_is_symbol_constant(st, b)) {
      return (a == b) ? Expr_false() : Expr_true();
    }

    /* TRUE != B --------> !B */
    if (Expr_is_true(a)) {
      tb = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(tb)) {
        return Expr_not(b);
      }
    }
    /* A != TRUE --------> !A */
    else if (Expr_is_true(b)) {
      ta = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(ta)) {
        return Expr_not(a);
      }
    }
    /* FALSE != B --------> B */
    else if (Expr_is_false(a)) {
      tb = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(tb)) {
        return b;
      }
    }
    /* A != FALSE --------> A */
    else if (Expr_is_false(b)) {
      ta = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(ta)) {
        return a;
      }
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(NOTEQUAL, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the predicate LT (less-then) of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_lt(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_false();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_false();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_true();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va < vb) ? Expr_true() : Expr_false();
    }
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);

      WordNumber_ptr va =
        (NUMBER_UNSIGNED_WORD == ta || NUMBER_SIGNED_WORD == ta)
        ? WORD_NUMBER(car(_a)) : WORD_NUMBER(NULL);
      WordNumber_ptr vb =
        (NUMBER_UNSIGNED_WORD == tb || NUMBER_SIGNED_WORD == tb)
        ? WORD_NUMBER(car(_b)) : WORD_NUMBER(NULL);

      /* if both are constants => evaluate */
      if (va != NULL && vb != NULL) {
        nusmv_assert(ta == tb); /* signess has to be the same by type rules */

        return (NUMBER_UNSIGNED_WORD == ta
                ? WordNumber_unsigned_less(va, vb)
                : WordNumber_signed_less(va, vb))
          ? Expr_true() : Expr_false();
      }
      /* expr < uwconst(<size>,0)  =========> FALSE
         uwconst(<size>,max_value) < expr =========> FALSE
         swconst(<size>,max_value) < expr =========> FALSE */
      else if ((tb == NUMBER_UNSIGNED_WORD &&
                WordNumber_is_zero(vb))
               ||
               (ta == NUMBER_UNSIGNED_WORD &&
                WordNumber_get_unsigned_value(va) ==
                WordNumber_max_unsigned_value(WordNumber_get_width(va)))
               ||
               (ta == NUMBER_SIGNED_WORD &&
                WordNumber_get_signed_value(va) ==
                WordNumber_max_signed_value(WordNumber_get_width(va)))) {
        return Expr_false();
      }
      /* go to no-simplification return */
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(LT, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the predicate LT (less-then) of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_lt(const SymbTable_ptr st,
                          const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res = Expr_lt(a, b);
  if (Expr_is_true(res) || Expr_is_false(res)) return res;

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr type;
    TypeChecker_ptr tc;

    tc = SymbTable_get_type_checker(st);

    /* bexpr < FALSE -------->  FALSE */
    if (Expr_is_false(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_false();
      }
    }
    /* FALSE < bexpr -------->  bexpr */
    else if (Expr_is_false(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return b;
      }
    }
    /* bexpr < TRUE  --------> !bexpr */
    else if (Expr_is_true(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_not(a);
      }
    }
    /* TRUE  < bexpr -------->  FALSE */
    else if (Expr_is_true(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_false();
      }
    }
  }

  /* no simplification is possible */
  return res;
}

/**Function********************************************************************

  Synopsis           [Builds the predicate LE (less-then-equal)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_le(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_true();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_true();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_true();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va <= vb) ? Expr_true() : Expr_false();
    }
    /* words */
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);

      WordNumber_ptr va =
        (NUMBER_UNSIGNED_WORD == ta || NUMBER_SIGNED_WORD == ta)
        ? WORD_NUMBER(car(_a)) : WORD_NUMBER(NULL);
      WordNumber_ptr vb =
        (NUMBER_UNSIGNED_WORD == tb || NUMBER_SIGNED_WORD == tb)
        ? WORD_NUMBER(car(_b)) : WORD_NUMBER(NULL);

      /* if both are constants => evaluate */
      if (va != NULL && vb != NULL) {
        nusmv_assert(ta == tb); /* signess has to be the same by type rules */

        return (NUMBER_UNSIGNED_WORD == ta
                ? WordNumber_unsigned_less_or_equal(va, vb)
                : WordNumber_signed_less_or_equal(va, vb))
          ? Expr_true() : Expr_false();
      }
      /* expr <= uwconst(<size>,0) =========> expr = uwconst(<size>,0) */
      else if (tb == NUMBER_UNSIGNED_WORD &&
               WordNumber_is_zero(vb)) {
        return Expr_equal(a,b);
      }
      /* uwconst(<size>,0) <= expr =========> TRUE
         expr <= uwconst(<size>,max_value) =========> TRUE
         expr <= swconst(<size>,max_value) =========> TRUE */
      else if ((ta == NUMBER_UNSIGNED_WORD &&
                WordNumber_is_zero(va))
               ||
               (tb == NUMBER_UNSIGNED_WORD &&
                WordNumber_get_unsigned_value(vb) ==
                WordNumber_max_unsigned_value(WordNumber_get_width(vb)))
               ||
               (tb == NUMBER_SIGNED_WORD &&
                WordNumber_get_signed_value(vb) ==
                WordNumber_max_signed_value(WordNumber_get_width(vb)))) {
        return Expr_true();
      }
      /* go to no-simplification return */
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(LE, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the predicate LE (less-then-equal)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_le(const SymbTable_ptr st,
                          const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res = Expr_le(a, b);
  if (Expr_is_true(res) || Expr_is_false(res)) return res;

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr type;
    TypeChecker_ptr tc;

    tc = SymbTable_get_type_checker(st);

    /* bexpr <= FALSE -------->  !bexpr */
    if (Expr_is_false(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_not(a);
      }
    }
    /* FALSE <= bexpr -------->  TRUE */
    else if (Expr_is_false(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_true();
      }
    }
    /* bexpr <= TRUE  --------> TRUE */
    else if (Expr_is_true(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_true();
      }
    }
    /* TRUE  <= bexpr -------->  bexpr */
    else if (Expr_is_true(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return b;
      }
      /* go to no-simplification return */
    }
  }

  /* no simplification is possible */
  return res;
}

/**Function********************************************************************

  Synopsis           [Builds the predicate GT (greater-then)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_gt(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_false();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_false();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_false();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_false();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va >= vb) ? Expr_true() : Expr_false();
    }
    /* words */
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);

      WordNumber_ptr va =
        (NUMBER_UNSIGNED_WORD == ta || NUMBER_SIGNED_WORD == ta)
        ? WORD_NUMBER(car(_a)) : WORD_NUMBER(NULL);
      WordNumber_ptr vb =
        (NUMBER_UNSIGNED_WORD == tb || NUMBER_SIGNED_WORD == tb)
        ? WORD_NUMBER(car(_b)) : WORD_NUMBER(NULL);

      /* if both are constants => evaluate */
      if (va != NULL && vb != NULL) {
        nusmv_assert(ta == tb); /* signess has to be the same by type rules */

        return (NUMBER_UNSIGNED_WORD == ta
                ? WordNumber_unsigned_greater(va, vb)
                : WordNumber_signed_greater(va, vb))
          ? Expr_true() : Expr_false();
      }
      /* uwconst(<size>,0) > expr =========> FALSE
         expr > uwconst(<size>,max_value) =========> FALSE
         expr > swconst(<size>,max_value) =========> FALSE */
      else if ((ta == NUMBER_UNSIGNED_WORD &&
                WordNumber_is_zero(va))
               ||
               (tb == NUMBER_UNSIGNED_WORD &&
                WordNumber_get_unsigned_value(vb) ==
                WordNumber_max_unsigned_value(WordNumber_get_width(vb)))
               ||
               (tb == NUMBER_SIGNED_WORD &&
                WordNumber_get_signed_value(vb) ==
                WordNumber_max_signed_value(WordNumber_get_width(vb)))) {
        return Expr_false();
      }
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(GT, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the predicate GT (greater-then)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_gt(const SymbTable_ptr st,
                          const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res = Expr_gt(a, b);
  if (Expr_is_true(res) || Expr_is_false(res)) return res;

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr type;
    TypeChecker_ptr tc;

    tc = SymbTable_get_type_checker(st);

    /* bexpr > FALSE -------->  bexpr */
    if (Expr_is_false(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return a;
      }
    }
    /* FALSE > bexpr -------->  FALSE */
    else if (Expr_is_false(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_false();
      }
    }
    /* bexpr > TRUE  --------> FALSE */
    else if (Expr_is_true(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_false();
      }
    }
    /* TRUE  > bexpr -------->  !bexpr */
    else if (Expr_is_true(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_not(b);
      }
    }
  }

  /* no simplification is possible */
  return res;
}

/**Function********************************************************************

  Synopsis           [Builds the predicate GE (greater-then-equal)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_ge(const Expr_ptr a, const Expr_ptr b)
{
  if (a == b) return Expr_true();
  if (Expr_is_true(a) && Expr_is_true(b)) return Expr_true();
  if (Expr_is_true(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_false(b)) return Expr_true();
  if (Expr_is_false(a) && Expr_is_true(b)) return Expr_false();

  {
    Expr_ptr _a = expr_bool_const_to_number(a);
    Expr_ptr _b = expr_bool_const_to_number(b);
    int ta, tb;
    ta = node_get_type(_a); tb = node_get_type(_b);

    /* scalar constants */
    if (NUMBER == ta && NUMBER == tb) {
      int va = node_get_int(_a);
      int vb = node_get_int(_b);
      return (va >= vb) ? Expr_true() : Expr_false();
    }
    /* words */
    else if (NUMBER_UNSIGNED_WORD == ta || NUMBER_UNSIGNED_WORD == tb ||
             NUMBER_SIGNED_WORD == ta || NUMBER_SIGNED_WORD == tb) {
      /* if one of operands is word the other is also word or
         operands are word[1]*bool or bool*word[1] */
      _a = expr_bool_to_word1(_a);
      _b = expr_bool_to_word1(_b);
      ta = node_get_type(_a); tb = node_get_type(_b);

      WordNumber_ptr va =
        (NUMBER_UNSIGNED_WORD == ta || NUMBER_SIGNED_WORD == ta)
        ? WORD_NUMBER(car(_a)) : WORD_NUMBER(NULL);
      WordNumber_ptr vb =
        (NUMBER_UNSIGNED_WORD == tb || NUMBER_SIGNED_WORD == tb)
        ? WORD_NUMBER(car(_b)) : WORD_NUMBER(NULL);

      /* if both are constants => evaluate */
      if (va != NULL && vb != NULL) {
        nusmv_assert(ta == tb); /* signess has to be the same by type rules */

        return (NUMBER_UNSIGNED_WORD == ta
                ? WordNumber_unsigned_greater_or_equal(va, vb)
                : WordNumber_signed_greater_or_equal(va, vb))
          ? Expr_true() : Expr_false();
      }
      /*  uwconst(<size>,0) >= expr =========> uwconst(<size>,0) = expr*/
      else if (ta == NUMBER_UNSIGNED_WORD &&
               WordNumber_is_zero(va)) {
        return Expr_equal(a,b);
      }
      /* expr >= uwconst(<size>,0) =========> TRUE
         uwconst(<size>,max_value) >= expr =========> TRUE
         swconst(<size>,max_value) >= expr=========> TRUE */
      else if ((tb == NUMBER_UNSIGNED_WORD &&
                WordNumber_is_zero(vb))
               ||
               (ta == NUMBER_UNSIGNED_WORD &&
                WordNumber_get_unsigned_value(va) ==
                WordNumber_max_unsigned_value(WordNumber_get_width(va)))
               ||
               (ta == NUMBER_SIGNED_WORD &&
                WordNumber_get_signed_value(va) ==
                WordNumber_max_signed_value(WordNumber_get_width(va)))) {
        return Expr_true();
      }
      /* go to no-simplification return */
    }
  }

  /* no simplification is possible */
  return EXPR( find_node(GE, NODE_PTR(a), NODE_PTR(b)) );
}

/**Function********************************************************************

  Synopsis           [Builds the predicate GE (greater-then-equal)
  of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_ge(const SymbTable_ptr st,
                          const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res = Expr_ge(a, b);
  if (Expr_is_true(res) || Expr_is_false(res)) return res;

  if (SYMB_TABLE(NULL) != st) {
    SymbType_ptr type;
    TypeChecker_ptr tc;

    tc = SymbTable_get_type_checker(st);

    /* bexpr >= FALSE -------->  TRUE */
    if (Expr_is_false(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_true();
      }
    }
    /* FALSE >= bexpr -------->  !bexpr */
    else if (Expr_is_false(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_not(b);
      }
    }
    /* bexpr >= TRUE  --------> bexpr */
    else if (Expr_is_true(b)) {
      type = TypeChecker_get_expression_type(tc, a, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return a;
      }
    }
    /* TRUE  >= bexpr -------->  TRUE */
    else if (Expr_is_true(a)) {
      type = TypeChecker_get_expression_type(tc, b, Nil);
      if (SymbType_is_boolean_enum(type)) {
        return Expr_true();
      }
    }
  }

  /* no simplification is possible */
  return res;
}

/**Function********************************************************************

  Synopsis           [Builds the scalar node for PLUS of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_plus(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  Expr_ptr _b = expr_bool_const_to_number(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if (ta == NUMBER && tb == NUMBER) {
    return find_node(NUMBER,
                     NODE_FROM_INT((node_get_int(_a) +
                                   node_get_int(_b))),
                     Nil);
  }

  if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
      (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
    return find_node(ta,
                     (node_ptr) WordNumber_plus(WORD_NUMBER(car(_a)),
                                                WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* 0 + A = A */
  if (((ta == NUMBER) && (0 == node_get_int(_a))) ||
      (((ta == NUMBER_SIGNED_WORD) || (ta == NUMBER_UNSIGNED_WORD)) &&
       WordNumber_is_zero(WORD_NUMBER(car(_a))))) {
    return b;
  }
  /* A + 0 = A */
  if (((tb == NUMBER) && (0 == node_get_int(_b))) ||
      (((tb == NUMBER_SIGNED_WORD) || (tb == NUMBER_UNSIGNED_WORD)) &&
       WordNumber_is_zero(WORD_NUMBER(car(_b))))) {
    return a;
  }

  /* no simplification is possible */
  return EXPR( find_node(PLUS, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the scalar node for MINUS of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_minus(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  Expr_ptr _b = expr_bool_const_to_number(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if (ta == NUMBER && tb == NUMBER) {
    return find_node(NUMBER,
                     NODE_FROM_INT((node_get_int(_a) -
                                   node_get_int(_b))),
                     Nil);
  }

  if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
      (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
    return find_node(ta,
                     (node_ptr) WordNumber_minus(WORD_NUMBER(car(_a)),
                                                 WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* 0 - A = -A */
  if (((ta == NUMBER) && (0 == node_get_int(_a))) ||
      (((ta == NUMBER_SIGNED_WORD) || (ta == NUMBER_UNSIGNED_WORD)) &&
       WordNumber_is_zero(WORD_NUMBER(car(_a))))) {
    return Expr_unary_minus(b);
  }
  /* A - 0 = A */
  if (((tb == NUMBER) && (0 == node_get_int(_b))) ||
      (((tb == NUMBER_SIGNED_WORD) || (tb == NUMBER_UNSIGNED_WORD))  &&
       WordNumber_is_zero(WORD_NUMBER(car(_b))))) {
    return a;
  }

  /* no simplification is possible */
  return EXPR( find_node(MINUS, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the scalar node for TIMES of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_times(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  Expr_ptr _b = expr_bool_const_to_number(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if (ta == NUMBER && tb == NUMBER) {
    return find_node(NUMBER,
                     NODE_FROM_INT((node_get_int(_a) *
                                   node_get_int(_b))),
                     Nil);
  }
  if ((ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) ||
      (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD)) {
    return find_node(ta,
                     (node_ptr) WordNumber_times(WORD_NUMBER(car(_a)),
                                                 WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* 0 * A = 0 */
  if (((ta == NUMBER) && (0 == node_get_int(_a))) ||
      ((tb == NUMBER) && (0 == node_get_int(_b)))) {
    return find_node(NUMBER, NODE_FROM_INT(0), Nil);
  }
  /* A * 0 = 0 */
  if ((((ta == NUMBER_SIGNED_WORD) || (ta == NUMBER_UNSIGNED_WORD)) &&
       WordNumber_is_zero(WORD_NUMBER(car(_a)))) ||
      (((tb == NUMBER_SIGNED_WORD) || (tb == NUMBER_UNSIGNED_WORD)) &&
       WordNumber_is_zero(WORD_NUMBER(car(_b))))) {
    return ((ta == NUMBER_SIGNED_WORD) ||
            (ta == NUMBER_UNSIGNED_WORD)) ? a : b;
  }

  /* no simplification is possible */
  return EXPR( find_node(TIMES, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the scalar node for DIVIDE of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_divide(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  Expr_ptr _b = expr_bool_const_to_number(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if (ta == NUMBER && tb == NUMBER) {
    int vb = node_get_int(_b);
    if (vb == 0) error_div_by_zero(b);
    return find_node(NUMBER,
                     NODE_FROM_INT((node_get_int(_a) / vb)),
                     Nil);
  }
  if (ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) {
    if (WordNumber_is_zero(WORD_NUMBER(car(_b)))) error_div_by_zero(b);
    return find_node(ta,
            (node_ptr) WordNumber_unsigned_divide(WORD_NUMBER(car(_a)),
                                                  WORD_NUMBER(car(_b))),
                     Nil);
  }
  if (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD) {
    if (WordNumber_is_zero(WORD_NUMBER(car(_b)))) error_div_by_zero(b);
    return find_node(ta,
            (node_ptr) WordNumber_signed_divide(WORD_NUMBER(car(_a)),
                                                WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* no simplification is possible */
  return EXPR( find_node(DIVIDE, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the scalar node for MODule of given operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_mod(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  Expr_ptr _b = expr_bool_const_to_number(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if (ta == NUMBER && tb == NUMBER) {
    int vb = node_get_int(_b);
    if (vb == 0) error_div_by_zero(b);
    if (vb == 2) {
      /* integer mod 2 -> boolean */
      return ((node_get_int(_a) % 2) == 0) ? Expr_false() : Expr_true();
    }
    return find_node(NUMBER,
                     NODE_FROM_INT((node_get_int(_a) % vb)),
                     Nil);
  }
  if (ta == NUMBER_UNSIGNED_WORD && tb == NUMBER_UNSIGNED_WORD) {
    if (WordNumber_is_zero(WORD_NUMBER(car(_b)))) error_div_by_zero(b);
    return find_node(ta,
            (node_ptr) WordNumber_unsigned_mod(WORD_NUMBER(car(_a)),
                                               WORD_NUMBER(car(_b))),
                     Nil);
  }
  if (ta == NUMBER_SIGNED_WORD && tb == NUMBER_SIGNED_WORD) {
    if (WordNumber_is_zero(WORD_NUMBER(car(_b)))) error_div_by_zero(b);
    return find_node(ta,
            (node_ptr) WordNumber_signed_mod(WORD_NUMBER(car(_a)),
                                             WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* no simplification is possible */
  return EXPR( find_node(MOD, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis [Builds the scalar node for UMINUS (unary minus) of given
  operators]

  Description        [Works with boolean, scalar and words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_unary_minus(const Expr_ptr a)
{
  Expr_ptr _a = expr_bool_const_to_number(a);
  switch (node_get_type(_a)) {
  case NUMBER: return find_node(NUMBER,
                                NODE_FROM_INT(-node_get_int(_a)), Nil);
  case NUMBER_UNSIGNED_WORD:
  case NUMBER_SIGNED_WORD:
    return find_node(node_get_type(_a),
                     (node_ptr) WordNumber_unary_minus(WORD_NUMBER(car(_a))),
                     Nil);
  }

  /* no simplification is possible */
  return EXPR( find_node(UMINUS, NODE_PTR(a), Nil) );
}


/**Function********************************************************************

  Synopsis           [Builds the node left shifting of words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_left_shift(const Expr_ptr a, const Expr_ptr b)
{
  int ta = node_get_type(a);

  if (ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
    Expr_ptr _b = expr_bool_const_to_number(b);
    int bits;
    switch (node_get_type(_b)) {
    case NUMBER: bits = node_get_int(_b); break;
    case NUMBER_UNSIGNED_WORD:
      bits = WordNumber_get_unsigned_value(WORD_NUMBER(car(_b))); break;
    case NUMBER_SIGNED_WORD:
      bits = WordNumber_get_signed_value(WORD_NUMBER(car(_b))); break;
    default: bits = -1;
    }

    if (bits == 0) return a;
    if (bits > 0) {
      if (bits > WordNumber_get_width(WORD_NUMBER(car(a)))) {
        error_wrong_word_operand("Right operand of shift is out of range", b);
      }
      return find_node(ta, (node_ptr) WordNumber_left_shift(WORD_NUMBER(car(a)),
                                                            bits),
                       Nil);
    }
    /* b here is not a constant */
  }

  /* no simplification is possible */
  return EXPR( find_node(LSHIFT, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the node right shifting of words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_right_shift(const Expr_ptr a, const Expr_ptr b)
{
  int ta = node_get_type(a);

  if (ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
    Expr_ptr _b = expr_bool_const_to_number(b);
    int bits;
    switch (node_get_type(_b)) {
    case NUMBER: bits = node_get_int(_b); break;
    case NUMBER_UNSIGNED_WORD:
      bits = WordNumber_get_unsigned_value(WORD_NUMBER(car(_b))); break;
    case NUMBER_SIGNED_WORD:
      bits = WordNumber_get_signed_value(WORD_NUMBER(car(_b))); break;
    default: bits = -1;
    }

    if (bits == 0) return a;
    if (bits > 0) {
      WordNumber_ptr rs;

      if (bits > WordNumber_get_width(WORD_NUMBER(car(a)))) {
        error_wrong_word_operand("Right operand of shift is out of range", b);
      }
      if (ta == NUMBER_UNSIGNED_WORD) {
        rs = WordNumber_unsigned_right_shift(WORD_NUMBER(car(a)), bits);
      }
      else {
        rs = WordNumber_signed_right_shift(WORD_NUMBER(car(a)), bits);
      }

      return find_node(ta, (node_ptr) rs, Nil);
    }
    /* b here is not a constant */
  }

  /* no simplification is possible */
  return EXPR( find_node(RSHIFT, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the node left rotation of words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_left_rotate(const Expr_ptr a, const Expr_ptr b)
{
  int ta = node_get_type(a);

  if (ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
    Expr_ptr _b = expr_bool_const_to_number(b);
    int bits;
    switch (node_get_type(_b)) {
    case NUMBER: bits = node_get_int(_b); break;
    case NUMBER_UNSIGNED_WORD:
      bits = WordNumber_get_unsigned_value(WORD_NUMBER(car(_b))); break;
    case NUMBER_SIGNED_WORD:
      bits = WordNumber_get_signed_value(WORD_NUMBER(car(_b))); break;
    default: bits = -1;
    }

    if (bits == 0) return a;
    if (bits > 0) {
      if (bits > WordNumber_get_width(WORD_NUMBER(car(a)))) {
        error_wrong_word_operand("Right operand of rotate is out of range", b);
      }
      return find_node(ta, (node_ptr) WordNumber_left_rotate(WORD_NUMBER(car(a)),
                                                             bits),
                       Nil);
    }
    /* b here is not a constant */
  }

  /* no simplification is possible */
  return EXPR( find_node(LROTATE, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the node right rotation of words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_right_rotate(const Expr_ptr a, const Expr_ptr b)
{
  int ta = node_get_type(a);

  if (ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) {
    Expr_ptr _b = expr_bool_const_to_number(b);
    int bits;
    switch (node_get_type(_b)) {
    case NUMBER: bits = node_get_int(_b); break;
    case NUMBER_UNSIGNED_WORD:
      bits = WordNumber_get_unsigned_value(WORD_NUMBER(car(_b))); break;
    case NUMBER_SIGNED_WORD:
      bits = WordNumber_get_signed_value(WORD_NUMBER(car(_b))); break;
    default: bits = -1;
    }

    if (bits == 0) return a;
    if (bits > 0) {
      if (bits > WordNumber_get_width(WORD_NUMBER(car(a)))) {
        error_wrong_word_operand("Right operand of rotate is out of range", b);
      }
      return find_node(ta, (node_ptr) WordNumber_right_rotate(WORD_NUMBER(car(a)),
                                                              bits),
                       Nil);
    }
    /* b here is not a constant */
  }

  /* no simplification is possible */
  return EXPR( find_node(RROTATE, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the node for bit selection of words.

  Description        [Works with words. Performs local syntactic
                      simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_bit_select(const Expr_ptr w, const Expr_ptr r)
{
  /* Expr_ptr _r = expr_bool_const_to_number(r); */
  if (/* Simplification can be done iff the range is constant. If the
         range is a constant expression, we simply return the
         BIT_SELECTION node */
      (NUMBER == node_get_type(car(r)) &&
       NUMBER == node_get_type(cdr(r))) &&

      (((node_get_type(w) == UNSIGNED_WORD ||
         node_get_type(w) == SIGNED_WORD) &&
        (node_word_get_width(w) > 0))
       || (node_get_type(w) == NUMBER_UNSIGNED_WORD)
       || (node_get_type(w) == NUMBER_SIGNED_WORD))) {
    return EXPR(node_word_selection(w, r));
  }

  return EXPR(find_node(BIT_SELECTION, w, r));
}


/**Function********************************************************************

  Synopsis           [Builds the node for bit selection of words.

  Description        [Works with words. Performs local semantic and syntactic
                      simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_simplify_word_bit_select(const SymbTable_ptr st,
                                       const Expr_ptr w, const Expr_ptr r)
{
  if (SYMB_TABLE(NULL) != st) {
    nusmv_assert(SYMB_TABLE(NULL) != st);
    TypeChecker_ptr tc = SymbTable_get_type_checker(st);
    SymbType_ptr wt = TypeChecker_get_expression_type(tc, w, Nil);
    int argt_width = SymbType_get_word_width(wt);
    node_ptr msb, lsb;

    /* Simplify constant expressions */
    msb = CompileFlatten_resolve_number(st, car(r), Nil);
    lsb = CompileFlatten_resolve_number(st, cdr(r), Nil);

    nusmv_assert(COLON == node_get_type(r));
    nusmv_assert(Nil != msb && Nil != lsb &&
                 NUMBER == node_get_type(msb) &&
                 NUMBER == node_get_type(lsb));

    int sel_msb = node_get_int(msb);
    int sel_lsb = node_get_int(lsb);

    /* these simplification apply to unsigned words only */
    if (SymbType_is_unsigned_word(wt)) {

      /* Discard useless bit selection operations */
      if (0 == sel_lsb && (argt_width -1) == sel_msb) return w;

      if (EXTEND == node_get_type(w)) {
        Expr_ptr _w = car(w);
        SymbType_ptr _wt = TypeChecker_get_expression_type(tc, _w, Nil);

        int orig_width = SymbType_get_word_width(_wt);
        nusmv_assert(0 < orig_width && argt_width >= orig_width);

        {
          Expr_ptr res = Nil;
          int pivot = orig_width; /* starting bit position for '0' padding */


          /* if the selection is from the extension only rewrite as as
             0 word constant of appropriate width */
          if (sel_lsb >= pivot) {
            res = \
              find_node(NUMBER_UNSIGNED_WORD,
                        (node_ptr) WordNumber_from_integer(0LL,
                                                           sel_msb - sel_lsb +1),
                        Nil);
          }
          /* if the selection is from the original word only, discard the
             EXTEND operation */
          else if (sel_msb < pivot) {
            res = Expr_simplify_word_bit_select(st, _w, r);
          }
          /* if the selection is from both the extension and the original
             word, rewrite it as the extension to appropriate size of the
             selection of the relevant part of the word. */
          else {
            nusmv_assert(sel_msb >= pivot && pivot > sel_lsb);
            res = Expr_simplify_word_extend(st,
                    Expr_simplify_word_bit_select(st, _w,
                         find_node(COLON,
                                   find_node(NUMBER,
                                             NODE_FROM_INT(pivot-1), Nil),
                                   find_node(NUMBER,
                                             NODE_FROM_INT(sel_lsb), Nil))),
                    find_node(NUMBER,
                              NODE_FROM_INT(sel_msb - pivot +1), Nil));
          }

          return res;
        }
      }
    }
  }
  /* fallback */
  return Expr_word_bit_select(w, r);
}


/**Function********************************************************************

  Synopsis           [Builds the node for word concatenation.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_concatenate(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr _a = expr_bool_to_word1(a);
  Expr_ptr _b = expr_bool_to_word1(b);
  int ta = node_get_type(_a);
  int tb = node_get_type(_b);

  if ((ta == NUMBER_UNSIGNED_WORD || ta == NUMBER_SIGNED_WORD) &&
      (tb == NUMBER_UNSIGNED_WORD || tb == NUMBER_SIGNED_WORD)) {
    return find_node(NUMBER_UNSIGNED_WORD,
                     (node_ptr) WordNumber_concatenate(WORD_NUMBER(car(_a)),
                                                       WORD_NUMBER(car(_b))),
                     Nil);
  }

  /* no simplification is possible */
  return EXPR( find_node(CONCATENATION, NODE_PTR(a), NODE_PTR(b)) );
}


/**Function********************************************************************

  Synopsis           [Builds the node for casting word1 to boolean.

  Description        [Works with words with width 1.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word1_to_bool(Expr_ptr w)
{
  int tw = node_get_type(w);
  if (tw == NUMBER_UNSIGNED_WORD || tw == NUMBER_SIGNED_WORD) {
    WordNumber_ptr wn = WORD_NUMBER(car(w));
    return (WordNumber_get_unsigned_value(wn) != 0)? Expr_true() : Expr_false();
  }

  /* no simplification is possible */
  return EXPR( find_node(CAST_BOOL, NODE_PTR(w), Nil) );
}


/**Function********************************************************************

  Synopsis           [Builds the node for casting boolean to word1.

  Description        [Works with booleans.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_bool_to_word1(Expr_ptr a)
{
  Expr_ptr _a = expr_bool_to_word1(a);
  if (_a != a) return _a;

   /* no simplification is possible */
  return EXPR( find_node(CAST_WORD1, NODE_PTR(a), Nil) );
}


/**Function********************************************************************

  Synopsis [Builds the node for casting signed words to unsigned
  words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_signed_word_to_unsigned(Expr_ptr w)
{
  if (node_get_type(w) == NUMBER_SIGNED_WORD) {
    return find_node(NUMBER_UNSIGNED_WORD, car(w), cdr(w));
  }

   /* no simplification is possible */
  return EXPR( find_node(CAST_UNSIGNED, NODE_PTR(w), Nil) );
}


/**Function********************************************************************

  Synopsis [Builds the node for casting unsigned words to signed words.

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_unsigned_word_to_signed(Expr_ptr w)
{
  if (node_get_type(w) == NUMBER_UNSIGNED_WORD) {
    return find_node(NUMBER_SIGNED_WORD, car(w), cdr(w));
  }

   /* no simplification is possible */
  return EXPR( find_node(CAST_SIGNED, NODE_PTR(w), Nil) );
}



/**Function********************************************************************

  Synopsis    [Builds the node for resizing a word.]

  Description [Works with words.
  Performs local syntactic simplification]

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
Expr_ptr Expr_simplify_word_resize(const SymbTable_ptr st,
                                   Expr_ptr w, Expr_ptr i)
{
  Expr_ptr _i;

  _i = CompileFlatten_resolve_number(st, i, Nil);
  nusmv_assert(Nil != _i && node_get_type(_i) == NUMBER);

  if (NUMBER_UNSIGNED_WORD == node_get_type(w) || \
      NUMBER_SIGNED_WORD == node_get_type(w)) {

    int m = WordNumber_get_width(WORD_NUMBER(car(w)));
    int n = node_get_int(i); nusmv_assert(0 < n);

    if (m == n) { return w; }
    else if (m < n) {
      return Expr_simplify_word_extend(st, w,
                                find_node(NUMBER, NODE_FROM_INT(n - m), NULL));
    }
    else { /* n < m */
      if (NUMBER_UNSIGNED_WORD == node_get_type(w)) { /* unsigned */

        return Expr_word_bit_select(w, find_node(COLON,
                                    find_node(NUMBER, NODE_FROM_INT(n - 1), Nil),
                                    find_node(NUMBER, NODE_FROM_INT(0), Nil)));
      }
      else { /* signed */
        nusmv_assert(NUMBER_SIGNED_WORD ==  node_get_type(w));

        node_ptr msb_sel = \
          find_node(COLON,
                    find_node(NUMBER, NODE_FROM_INT(m-1), Nil),
                    find_node(NUMBER, NODE_FROM_INT(m-1), Nil));

        node_ptr rightmost_sel = \
          find_node(COLON,
                    find_node(NUMBER, NODE_FROM_INT(n-2), Nil),
                    find_node(NUMBER, NODE_FROM_INT(0), Nil));

        node_ptr nexpr = \
          Expr_word_concatenate(Expr_word_bit_select(w, msb_sel),
                                Expr_word_bit_select(w, rightmost_sel));

        return Expr_unsigned_word_to_signed(nexpr);
      }
    }
  }

  /* no simplification possible */
  return find_node(WRESIZE, w, _i);
}


/**Function********************************************************************

  Synopsis [Builds the node for extending a word.]

  Description        [Works with words.
  Performs local syntactic simplification]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_word_extend(Expr_ptr w, Expr_ptr i)
{
  Expr_ptr _i = expr_bool_const_to_number(i);
  int tw = node_get_type(w);

  _i = CompileFlatten_resolve_number(expr_st, i, Nil);
  nusmv_assert(Nil != _i && node_get_type(_i) == NUMBER);

  if (tw == NUMBER_UNSIGNED_WORD) {
    return find_node(NUMBER_UNSIGNED_WORD,
                     (node_ptr) WordNumber_unsigned_extend(WORD_NUMBER(car(w)),
                                                           node_get_int(_i)),
                     Nil);
  }
  if (tw == NUMBER_SIGNED_WORD) {
    return find_node(NUMBER_SIGNED_WORD,
                     (node_ptr) WordNumber_signed_extend(WORD_NUMBER(car(w)),
                                                         node_get_int(_i)),
                     Nil);
  }

   /* no simplification is possible */
  return EXPR( find_node(EXTEND, NODE_PTR(w), NODE_PTR(_i)) );
}


/**Function********************************************************************

  Synopsis    [Builds the node for extending a word.]

  Description [Works with words. Performs local syntactic
               simplification]

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
Expr_ptr Expr_simplify_word_extend(const SymbTable_ptr st,
                                   Expr_ptr w, Expr_ptr i)
{
  Expr_ptr _i;
  int tw = node_get_type(w);

  _i = CompileFlatten_resolve_number(st, i, Nil);
  nusmv_assert(Nil != _i && node_get_type(_i) == NUMBER);

  if (tw == NUMBER_UNSIGNED_WORD) {
    return find_node(NUMBER_UNSIGNED_WORD,
                     (node_ptr) WordNumber_unsigned_extend(WORD_NUMBER(car(w)),
                                                           node_get_int(_i)),
                     Nil);
  }
  if (tw == NUMBER_SIGNED_WORD) {
    return find_node(NUMBER_SIGNED_WORD,
                     (node_ptr) WordNumber_signed_extend(WORD_NUMBER(car(w)),
                                                         node_get_int(_i)),
                     Nil);
  }

   /* no simplification is possible */
  return EXPR( find_node(EXTEND, NODE_PTR(w), NODE_PTR(_i)) );
}


/**Function********************************************************************

  Synopsis           [Creates a ATTIME node]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_attime(Expr_ptr e, int time)
{
  node_ptr _e = expr_bool_to_bool(e);
  int te;

  /* boolean constant */
  if (Expr_is_true(_e) || Expr_is_false(_e)) return e;

  /* scalar constants */
  te = node_get_type(_e);
  if (te == NUMBER || te == NUMBER_UNSIGNED_WORD || te == NUMBER_SIGNED_WORD) {
    return e;
  }

  /* a range? */
  if (te == TWODOTS &&
      NUMBER == node_get_type(car(_e)) && NUMBER == node_get_type(cdr(_e))) {
    return e;
  }

  /* enumerative? */
  if (expr_st != SYMB_TABLE(NULL) && SymbTable_is_symbol_constant(expr_st, e)) {
    return e;
  }

  /* set of constants ? */
  if (expr_st != SYMB_TABLE(NULL) && UNION == node_get_type(_e)) {
    Set_t set = Set_MakeFromUnion(_e);
    boolean is_const = true;
    Set_Iterator_t iter;
    SET_FOREACH(set, iter) {
      if (!SymbTable_is_symbol_constant(expr_st,
                                        (node_ptr) Set_GetMember(set, iter))) {
        is_const = false;
        break;
      }
    }

    Set_ReleaseSet(set);
    if (is_const) return e;
  }

  /* fallback */
  return find_node(ATTIME, e,
                   find_node(NUMBER, NODE_FROM_INT(time), Nil));
}


/**Function********************************************************************

  Synopsis           [Retrieves the time out of an ATTIME node]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
int Expr_attime_get_time(Expr_ptr e)
{
  nusmv_assert(ATTIME == node_get_type(e));
  return node_get_int(cdr(e));
}


/**Function********************************************************************

  Synopsis           [Retrieves the untimed node out of an ATTIME node]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_attime_get_untimed(Expr_ptr e)
{
  nusmv_assert(ATTIME == node_get_type(e));
  return car(e);
}


/**Function********************************************************************

  Synopsis           [Makes a union node]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_union(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res;

  if (Nil == a) return b;
  if (Nil == b) return a;
  if (a == b) return a;

  res = find_node(UNION, a, b);

  /* If expression is recursively constructed of many UNION
     the below Set_MakeFromUnion will be applied many times on the
     same elements.
     All the uses of this function have to be checked.

     A better implementation is to construct UNION from Set_t of elements.

     Note that addition of new elements to Set_t has to be done
     with Set_MakeFromUnion as it normalizes elements.
     It is also better to make Set_MakeFromUnion have a parameter
     Set_t where to add new elements instead of creating new Set_t
     every time and then unite.
  */

  { /* checks if cardinality is 1 */
    Set_t set = Set_MakeFromUnion(res);
    if (Set_GiveCardinality(set) == 1) {
      res = (Expr_ptr) Set_GetMember(set, Set_GetFirstIter(set));
    }
    Set_ReleaseSet(set);
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Makes a TWODOTS node, representing an integer range]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_range(const Expr_ptr a, const Expr_ptr b)
{
  if (Nil == a) return b;
  if (Nil == b) return a;
  if (a == b) return a;

  if (NUMBER == node_get_type(a) && NUMBER == node_get_type(b) &&
      node_get_int(a) == node_get_int(b)) {
    return a;
  }

  return find_node(TWODOTS, a, b);
}


/**Function********************************************************************

  Synopsis           [Makes a setin node, with possible syntactic
                      simplification.]

  Description        []

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_setin(const Expr_ptr a, const Expr_ptr b)
{
  Expr_ptr res;
  Set_t seta = Set_MakeFromUnion(a);
  Set_t setb = Set_MakeFromUnion(b);

  /* checks if it can syntactically resolve it */
  if (Set_Contains(setb, seta)) res = Expr_true();
  else {
    if (expr_st != SYMB_TABLE(NULL)) {
      /* see if the sets are made of only constants */
      boolean a_b_const = true;
      Set_Iterator_t iter;
      SET_FOREACH(seta, iter) {
        a_b_const = SymbTable_is_symbol_constant(expr_st,
                              (node_ptr) Set_GetMember(seta, iter));
        if (!a_b_const) break;
      }
      if (a_b_const) {
        SET_FOREACH(setb, iter) {
          a_b_const = SymbTable_is_symbol_constant(expr_st,
                              (node_ptr) Set_GetMember(setb, iter));
          if (!a_b_const) break;
        }
      }

      if (a_b_const) {
        /* both sets contain only constants, so since seta is not
           contained into setb, seta is not containted in setb */
        res = Expr_false();
      }
      else res = find_node(SETIN, a, b); /* fallback */
    }
    else {
      /* symbol table is not available, so nothing can be said */
      res = find_node(SETIN, a, b);
    }
  }

  Set_ReleaseSet(setb);
  Set_ReleaseSet(seta);

  return res;
}


/**Function********************************************************************

  Synopsis           [Builds an Uninterpreted function]

  Description        [Builds an uninterpreted function named "name" with
                      "params" as parameters. "params" must be a cons
                      list of expressions (Created with find_node)]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
Expr_ptr Expr_function(const Expr_ptr name, const Expr_ptr params)
{
  return EXPR(find_node(NFUNCTION, name, params));
}


/**Function********************************************************************

  Synopsis [This is the top-level function that simplifiers can use to
  simplify expressions. This evaluates constant values in operands
  left and right with respect to the operation required with parameter type.]

  Description [Given an expression node E (handled at
  simplifier-level) the simplifier call this function in post order
  after having simplified car(E) and cdr(E). It calls it by passing
  node_get_type(E) as type, and simplified sub expressions for left and right.
  The function Expr_resolve does not traverses further the structures, it simply
  combine given operation encoded in type with given already simplified
  operands left and right.

  For example, suppose E is AND(exp1, exp2). The simplifier:

  1. Simplifies recursively exp1 to exp1' and exp2 to exp2' (lazyness
  might be taken into account if exp1 is found to be a false
  constant).

  2. Calls in postorder Expr_resolve(AND, exp1', exp2')

  Expr_resolve will simplify sintactically the conjunction of (exp1', exp2')]

  SideEffects        [None]

  SeeAlso            [Expr_simplify]

******************************************************************************/
Expr_ptr Expr_resolve(SymbTable_ptr st,
                      int type, Expr_ptr left, Expr_ptr right)
{
  switch (type) {
    /* boolean leaves */
  case TRUEEXP: return Expr_true();
  case FALSEEXP: return Expr_false();

    /* other leaves */
  case NUMBER:
  case NUMBER_UNSIGNED_WORD:
  case NUMBER_SIGNED_WORD:
  case NUMBER_FRAC:
  case NUMBER_REAL:
  case NUMBER_EXP:
  case BIT:
  case DOT:
  case ATOM:
  case ARRAY:
  case FAILURE:
    return find_node(type, left, right);

  case UWCONST:
  case SWCONST:
    /* gets rid of wconst() */
    return Compile_FlattenSexp(st, find_node(type, left, right),
                               (node_ptr) NULL);

  case WSIZEOF:
    /* get rids of sizeof() */
    return Compile_FlattenSexp(st, find_node(type, left, right),
                               (node_ptr) NULL);

  case WTOINT:
    /* get rids of toint() */
    return Compile_FlattenSexp(st, find_node(type, left, right),
                               (node_ptr) NULL);

  case WRESIZE: return Expr_simplify_word_resize(st, left, right);

    /* boolean */
  case AND: return Expr_and(left, right);
  case OR: return Expr_or(left, right);
  case NOT: return Expr_not(left);
  case IMPLIES: return Expr_implies(left, right);
  case IFF: return Expr_simplify_iff(st, left, right);
  case XOR: return Expr_xor(left, right);
  case XNOR: return Expr_xnor(left, right);

    /* predicates */
  case EQUAL: return Expr_simplify_equal(st, left, right);
  case NOTEQUAL: return Expr_simplify_notequal(st, left, right);
  case LT: return Expr_simplify_lt(st, left, right);
  case LE: return Expr_simplify_le(st, left, right);
  case GT: return Expr_simplify_gt(st, left, right);
  case GE: return Expr_simplify_ge(st, left, right);

    /* case */
  case IFTHENELSE:
  case CASE:
    nusmv_assert(node_get_type(left) == COLON);
    return Expr_ite(car(left), cdr(left), right);

  case NEXT: return Expr_next(left);

    /* scalar */
  case UMINUS: return Expr_unary_minus(left);
  case PLUS: return Expr_plus(left, right);
  case MINUS: return Expr_minus(left, right);
  case TIMES: return Expr_times(left, right);
  case DIVIDE: return Expr_divide(left, right);
  case MOD: return Expr_mod(left, right);

    /* word-specific */
  case CAST_WORD1: return Expr_bool_to_word1(left);
  case CAST_BOOL: return Expr_word1_to_bool(left);
  case CAST_SIGNED: return Expr_unsigned_word_to_signed(left);
  case CAST_UNSIGNED: return Expr_signed_word_to_unsigned(left);
  case EXTEND: return Expr_simplify_word_extend(st, left, right);
  case LSHIFT: return Expr_word_left_shift(left, right);
  case RSHIFT: return Expr_word_right_shift(left, right);
  case LROTATE: return Expr_word_left_rotate(left, right);
  case RROTATE: return Expr_word_right_rotate(left, right);
  case BIT_SELECTION: return Expr_simplify_word_bit_select(st, left, right);
  case CONCATENATION: return Expr_word_concatenate(left, right);

    /* wants number rsh */
  case ATTIME:
    nusmv_assert(node_get_type(right) == NUMBER);
    return Expr_attime(left, node_get_int(right));

    /* sets are simplified when involving constants only */
  case UNION: return Expr_union(left, right);

    /* sets are simplified when involving constants only */
  case SETIN: return Expr_setin(left, right);

    /* ranges are simplified when low and high coincide */
  case TWODOTS: return Expr_range(left, right);

    /* no simplification */
  case EQDEF:
  case CONS:
  case CONTEXT:
  case COLON:
  case SMALLINIT:

    /* no simplification in current implementation: */
  case EX:
  case AX:
  case EG:
  case AG:
  case EF:
  case AF:
  case OP_NEXT:
  case OP_PREC:
  case OP_NOTPRECNOT:
  case OP_FUTURE:
  case OP_ONCE:
  case OP_GLOBAL:
  case OP_HISTORICAL:
  case EBF:
  case ABF:
  case EBG:
  case ABG:
  case EBU:
  case ABU:
  case EU:
  case AU:
  case MINU:
  case MAXU:
  case UNTIL:
  case RELEASES:
  case SINCE:
  case TRIGGERED:
    return find_node(type, left, right);

  default:
    return find_node(type, left, right);
  }

  return EXPR(Nil);
}


/**Function********************************************************************

  Synopsis   [Top-level simplifier that evaluates constants and
  simplifies syntactically the given expression]

  Description [Top-level simplifier that evaluates constants and
  simplifies syntactically the given expression. Simplification is trivial,
  no lemma learning nor sintactic implication is carried out at the moment.

  WARNING:
  the results of simplifications are memoized in a hash stored
  in the symbol table provided. Be very careful not to free/modify the input
  expression or make sure that the input expressions are find_node-ed.
  Otherwise, it is very easy to introduce a bug which will be
  difficult to catch.
  The hash in the symbol table is reset when any layer is removed.

  NOTE FOR DEVELOPERS: if you think that memoization the simplification
  results may cause some bugs you always can try without global
  memoization. See the function body below for info.

  ]

  SideEffects [None]

  SeeAlso     []

******************************************************************************/
Expr_ptr Expr_simplify(SymbTable_ptr st, Expr_ptr expr)
{
  SymbTable_ptr old_st = expr_st;
  Expr_ptr res;
  hash_ptr hash_memoize;

  /* NOTE FOR DEVELOPERS. In order to disable global memoization of
     simplification results make the below macro be equal to 0 instead of 1.
     This is purely debugging feature
  */
#define _ENABLE_GLOBAL_SIMPLIFICATION_MEMOIZATION_ 1

#if _ENABLE_GLOBAL_SIMPLIFICATION_MEMOIZATION_
  hash_memoize = SymbTable_get_simplification_hash(st);
#else
  hash_memoize = new_assoc();
#endif

  CATCH {
    expr_st = st;
    res = expr_simplify_aux(st, expr, hash_memoize);
    expr_st = old_st;
  }
  FAIL {
    expr_st = old_st;
    rpterr("An error occurred during Expr_simplify");
  }

#if ! _ENABLE_GLOBAL_SIMPLIFICATION_MEMOIZATION_
  free_assoc(hash_memoize);
#endif

  return res;
}


/**Function********************************************************************

   Synopsis           [Check if an expr is of a finite range type]

   Description        [Check if an expr is of a finite range type.

                       REMARK: Words are considered finite only if
                       word_unbooleanizable is set to false

                       If cache is not null whenever we encounter a formula in
                       the cache we simply return the previously computed value,
                       otherwise an internal and temporary map is used.

                       NOTE: the internal representation of cache is private so
                             the user should provide only caches generated by
                             this function!]

   SideEffects        [none]

******************************************************************************/
boolean Expr_is_booleanizable(Expr_ptr expr, const SymbTable_ptr st,
                              boolean word_unbooleanizable, hash_ptr cache)
{
  hash_ptr to_use;
  boolean res;

  if((hash_ptr)NULL == cache) {
    to_use = new_assoc();
  }
  else {
    to_use = cache;
  }

  res = expr_is_booleanizable_aux(expr, st, word_unbooleanizable, to_use);

  if((hash_ptr)NULL == cache) {
    free_assoc(to_use);
  }

  return res;
}


/**Function********************************************************************

   Synopsis           [Determines whether a formula has ATTIME nodes in it]

   Description        [Determines whether a formula has ATTIME nodes in it
                       If cache is not null whenever we encounter a formula in
                       the cache we simply return the previously computed value,
                       otherwise an internal and temporary map is used.

                       NOTE: the internal representation of cache is private so
                             the user should provide only caches generated by
                             this function!]

   SideEffects        [cache can be updated]

******************************************************************************/
boolean Expr_is_timed(Expr_ptr expr, hash_ptr cache)
{
  boolean res;
  if((hash_ptr)NULL == cache) {
    cache = new_assoc();
    res = expr_is_timed_aux(expr, cache);
    free_assoc(cache);
  }
  else {
    res = expr_is_timed_aux(expr, cache);
  }

  return res;
}


/**Function********************************************************************

   Synopsis           [Obtain the base time of an expression]

   Description        [Current time is recursively calculated as follows:

                       1. UNTIMED_CURRENT for Nil and leaves;
                       2. UNTIMED_FROZEN if all vars are frozen;
                       3. Time specified for an ATTIME node, assuming
                       that the inner expression is untimed.

                       Nesting of ATTIME nodes is _not_ allowed;
                       4. Minimum time for left and right children
                       assuming

                       UNTIMED_CURRENT <
                       UNTIMED_NEXT <
                       t, for any t >= 0.]

   SideEffects        [None]

******************************************************************************/
int Expr_get_time(SymbTable_ptr st, Expr_ptr expr)
{
  hash_ptr h;
  int res;

  h = new_assoc();
  res = expr_get_curr_time(st, expr, h);
  free_assoc(h);

  return res;
}

/**Function********************************************************************

   Synopsis           [Returns true if the time (obtained by Expr_get_time) is
                       dont't care]

   Description        []

   SideEffects        [Expr_get_time]

******************************************************************************/
boolean Expr_time_is_dont_care(int time)
{
  return time == UNTIMED_DONTCARE;
}


/**Function********************************************************************

   Synopsis           [Returns true if the time (obtained by Expr_get_time) is
                       current]

   Description        []

   SideEffects        [Expr_get_time]

******************************************************************************/
boolean Expr_time_is_current(int time)
{
  return time == UNTIMED_CURRENT;
}


/**Function********************************************************************

   Synopsis           [Returns true if the time (obtained by Expr_get_time) is
                       next]

   Description        []

   SideEffects        [Expr_get_time]

******************************************************************************/
boolean Expr_time_is_next(int time)
{
  return time == UNTIMED_NEXT;
}


/**Function********************************************************************

   Synopsis           [Returns the untimed version of an expression]

   Description        []

   SideEffects        [Expr_get_time]

******************************************************************************/
Expr_ptr Expr_untimed(SymbTable_ptr st, Expr_ptr expr)
{
  int time;

  time = Expr_get_time(st, expr);
  return Expr_untimed_explicit_time(st, expr, time);
}


/**Function********************************************************************

   Synopsis           [Returns the untimed version of an expression without
                       searching for the current time]

   Description        [Returns the untimed version of an expression using the
                       current time provided as an argument.]

   SideEffects        [Expr_get_time]

******************************************************************************/
Expr_ptr Expr_untimed_explicit_time(SymbTable_ptr st, Expr_ptr expr,
                                    int curr_time)
{
  hash_ptr h;
  Expr_ptr res;

  h = new_assoc();
  res = expr_timed_to_untimed(st, expr, curr_time, false, h);
  free_assoc(h);

  return res;
}

/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

   Synopsis           [Converts a timed node into an untimed node]

   Description        [Converts a timed node into an untimed node]

   SideEffects        [None]

******************************************************************************/
static Expr_ptr
expr_timed_to_untimed(SymbTable_ptr st, Expr_ptr expr, int curr_time,
                      boolean in_next, hash_ptr cache)
{
  if (expr == Nil) return Nil;
  const node_ptr key = in_next ? find_node(NEXT, expr, Nil) : expr;
  node_ptr res = find_assoc(cache, key);
  if (Nil != res) return res;

  switch (node_get_type(expr)) {
    /* leaves */
  case FAILURE:
  case ARRAY:
  case BIT:
  case DOT:
  case ATOM:
  case NUMBER_SIGNED_WORD:
  case NUMBER_UNSIGNED_WORD:
  case UWCONST:
  case SWCONST:
  case WORDARRAY:
  case NUMBER:
  case NUMBER_REAL:
  case NUMBER_FRAC:
  case NUMBER_EXP:
  case TRUEEXP:
  case FALSEEXP:
    res  = expr;
    break;

  case ATTIME:
    {
      /* a frozen var must be time compatible with any time */
      int time2 = SymbTable_is_symbol_frozen_var(st, car(expr))
        ? curr_time : node_get_int(cdr(expr));

      if (time2 == UNTIMED_CURRENT || time2 == curr_time) {
        res = expr_timed_to_untimed(st, car(expr), curr_time,
                                    in_next, cache);
      }
      else if (time2 == UNTIMED_NEXT || time2 == curr_time+1) {
        if (in_next) {
          internal_error("%s:%d:%s: Invalid nested NEXT (%s)",
                         __FILE__, __LINE__, __func__,
                         sprint_node(expr));
        }
        res = find_node(NEXT,
                        expr_timed_to_untimed(st, car(expr),
                                              curr_time,
                                              true, cache),
                        Nil);
      }
      else {
        internal_error("%s:%d:%s: Invalid ATTIME node (%s)",
                       __FILE__, __LINE__, __func__, sprint_node(expr));
      }

      break;
    }

  case NEXT:
    if (in_next) {
      internal_error("%s:%d:%s: Invalid nested NEXT (%s)",
                     __FILE__, __LINE__, __func__,
                     sprint_node(expr));
    }
    res = find_node(NEXT,
                    expr_timed_to_untimed(st, car(expr),
                                          curr_time,
                                          true, cache),
                    Nil);

    break;

  default:
    {
      node_ptr lt = expr_timed_to_untimed(st, car(expr),
                                          curr_time,
                                          in_next, cache);

      node_ptr rt = expr_timed_to_untimed(st, cdr(expr),
                                          curr_time,
                                          in_next, cache);

      res = find_node(node_get_type(expr), lt, rt);

      break;
    }

  } /* switch */

  insert_assoc(cache, key, res);
  nusmv_assert(Nil != res);
  return res;
}


/**Function********************************************************************

   Synopsis           [Calculates current time for an expression]

   Description        [Private service of Expr_get_time]

   SideEffects        [None]

******************************************************************************/
static int expr_get_curr_time(SymbTable_ptr st, node_ptr expr, hash_ptr cache)
{
  node_ptr tmp = find_assoc(cache, expr);
  if (Nil != tmp) return NODE_TO_INT(tmp) - _TIME_OFS;

  int res = 0;

  if (expr == Nil) {
    return UNTIMED_DONTCARE;
  }

  switch (node_get_type(expr)) {

    /* leaves */
  case DOT:
  case ATOM:
    /* handle frozenvars as a special case with lookahead */
    if (SymbTable_is_symbol_frozen_var(st,
          CompileFlatten_resolve_name(st, find_atom(expr), Nil))) {
      return UNTIMED_DONTCARE;
    }

  case FAILURE:
  case ARRAY:
  case BIT:
  case NUMBER_SIGNED_WORD:
  case NUMBER_UNSIGNED_WORD:
  case UWCONST:
  case SWCONST:
  case WORDARRAY:
  case NUMBER:
  case NUMBER_REAL:
  case NUMBER_FRAC:
  case NUMBER_EXP:
  case TRUEEXP:
  case FALSEEXP:
      return UNTIMED_CURRENT;

  case ATTIME: {
    int time1 = node_get_int(cdr(expr));
    int time2 = expr_get_curr_time(st, car(expr), cache);

    if (time2 == UNTIMED_DONTCARE) {
      res = UNTIMED_DONTCARE;
    }
    else if (time2 == UNTIMED_CURRENT) {
      res = time1; /* time1 is absolute */
    }
    else if (time2 == UNTIMED_NEXT) {
      internal_error("%s:%d:%s: Unexpected NEXT",
                     __FILE__, __LINE__, __func__);
    }
    else { /* time2 is absolute and this is wrong */
      nusmv_assert(0 <= time2);
      internal_error("%s:%d:%s: Invalid nested ATTIME",
                     __FILE__, __LINE__, __func__);
    }

    break;
  }

  default:
    {
      int time1 = expr_get_curr_time(st, car(expr), cache);
      int time2 = expr_get_curr_time(st, cdr(expr), cache);

      /* both are DON'T CARE? */
      if ((UNTIMED_DONTCARE == time1) && (UNTIMED_DONTCARE == time2)) {
        res = UNTIMED_DONTCARE;
      }

      /* one (but not both) is DON'T CARE? */
      else if (UNTIMED_DONTCARE == time1) {
        res = time2;
      }

      else if (UNTIMED_DONTCARE == time2) {
        res = time1;
      }

      /* both are CURRENT? */
      else if ((UNTIMED_CURRENT == time1) &&
          (UNTIMED_CURRENT == time2)) {
        res = UNTIMED_CURRENT;
      }

      /* one is CURRENT, the other is not */
      else if (UNTIMED_CURRENT == time1) {
        res = time2;
      }

      else if (UNTIMED_CURRENT == time2) {
        res = time1;
      }

      else {
        /* times can only be absolute beyond this point */
        nusmv_assert ((0 <= time1) && (0 <= time2));
        res = MIN(time1, time2);
      }

      break;
    }

  } /* switch */

  /* Cache */
  insert_assoc(cache, expr, NODE_FROM_INT(res + _TIME_OFS));
  return res;
}


static Expr_ptr expr_simplify_aux(SymbTable_ptr st, Expr_ptr expr,
                                  hash_ptr hash)
{
  node_ptr res = (node_ptr) NULL;
  int type;

  if (expr == Nil) return Nil;

  /* check memoization */
  res = find_assoc(hash, expr);
  if (res != (node_ptr) NULL) {
    return res;
  }

  type = node_get_type(expr);
  switch (type) {
    /* boolean leaves */
  case TRUEEXP: return Expr_true();
  case FALSEEXP: return Expr_false();

    /* name leaves */
  case ATOM:
  case BIT:
    return find_node(type, car(expr), cdr(expr));

    /* other leaves */
  case NUMBER:
  case NUMBER_UNSIGNED_WORD:
  case NUMBER_SIGNED_WORD:
  case NUMBER_FRAC:
  case NUMBER_REAL:
  case NUMBER_EXP:
  case FAILURE:
    return find_node(type, car(expr), cdr(expr));

  case UWCONST:
  case SWCONST:
    /* gets rid of wconst() */
    res = Compile_FlattenSexp(st, expr, (node_ptr) NULL);
    break;

  case WSIZEOF:
    /* get rids of sizeof() */
    res = Compile_FlattenSexp(st, expr, (node_ptr) NULL);
    break;

  case WTOINT:
    /* get rids of toint() */
    res = Compile_FlattenSexp(st, expr, (node_ptr) NULL);
    break;

  case WRESIZE:
    {
      Expr_ptr left = expr_simplify_aux(st, car(expr), hash);
      Expr_ptr right = expr_simplify_aux(st, cdr(expr), hash);
      res = Expr_resolve(st, type, left, right);
      break;
    }
  case DOT:
  case ARRAY:
    return find_node(type,
                     expr_simplify_aux(st, car(expr), hash),
                     expr_simplify_aux(st, cdr(expr), hash));

    /* unary */
  case NOT:
  case NEXT:
  case UMINUS:
  {
    Expr_ptr left = expr_simplify_aux(st, car(expr), hash);
    res = Expr_resolve(st, type, left, Nil);
    break;
  }

    /* binary with lazy eval */
  case AND:
  {
    Expr_ptr left = expr_simplify_aux(st, car(expr), hash);
    if (Expr_is_false(left)) res = left;
    else res = Expr_resolve(st, type, left, expr_simplify_aux(st, cdr(expr), hash));
    break;
  }

  case OR:
  {
    Expr_ptr left = expr_simplify_aux(st, car(expr), hash);
    if (Expr_is_true(left)) res = left;
    else res = Expr_resolve(st, type, left, expr_simplify_aux(st, cdr(expr), hash));
    break;
  }

  case IMPLIES:
  {
    Expr_ptr left = expr_simplify_aux(st, car(expr), hash);
    if (Expr_is_false(left)) res = Expr_true();
    else res = Expr_resolve(st, type, left, expr_simplify_aux(st, cdr(expr), hash));
    break;
  }

  /* binary, no lazyness */
  case IFF:
  case XOR:
  case XNOR:
  case EQUAL:
  case NOTEQUAL:
  case LT:
  case LE:
  case GT:
  case GE:
  case PLUS:
  case MINUS:
  case TIMES:
  case DIVIDE:
  case MOD:
  case CAST_WORD1:
  case CAST_BOOL:
  case CAST_SIGNED:
  case CAST_UNSIGNED:
  case EXTEND:
  case LSHIFT:
  case RSHIFT:
  case LROTATE:
  case RROTATE:
  case BIT_SELECTION:
  case CONCATENATION:
    res = Expr_resolve(st, type,
                       expr_simplify_aux(st, car(expr), hash),
                       expr_simplify_aux(st, cdr(expr), hash));
    break;

    /* case with lazyness on condition */
  case IFTHENELSE:
  case CASE:
  {
    Expr_ptr cond = expr_bool_to_bool(expr_simplify_aux(st, car(car(expr)), hash));
    Expr_ptr _then, _else;

    if (Expr_is_true(cond)) {
      _then = expr_simplify_aux(st, cdr(car(expr)), hash);
      _else = cdr(expr);
    }
    else if (Expr_is_false(cond)) {
      _then = cdr(car(expr));
      _else = expr_simplify_aux(st, cdr(expr), hash);
    }
    else {
      _then = expr_simplify_aux(st, cdr(car(expr)), hash);
      _else = expr_simplify_aux(st, cdr(expr), hash);
    }

    res = Expr_resolve(st, type, find_node(COLON, cond, _then), _else);
    break;
  }

  /* sets are simplified when possible */
  case SETIN:
  case UNION:
    res = Expr_resolve(st, type,
                       expr_simplify_aux(st, car(expr), hash),
                       expr_simplify_aux(st, cdr(expr), hash));
    break;

    /* ranges are simplified */
  case TWODOTS:
    res = Expr_resolve(st, type,
                       expr_simplify_aux(st, car(expr), hash),
                       expr_simplify_aux(st, cdr(expr), hash));
    break;

    /* no simplification */
  case EQDEF:
  case CONS:
  case CONTEXT:

    /* no simplification in current implementation: */
  case EX:
  case AX:
  case EG:
  case AG:
  case EF:
  case AF:
  case OP_NEXT:
  case OP_PREC:
  case OP_NOTPRECNOT:
  case OP_FUTURE:
  case OP_ONCE:
  case OP_GLOBAL:
  case OP_HISTORICAL:
  case EBF:
  case ABF:
  case EBG:
  case ABG:
  case EBU:
  case ABU:
  case EU:
  case AU:
  case MINU:
  case MAXU:
  case UNTIL:
  case RELEASES:
  case SINCE:
  case TRIGGERED:
    res = find_node(type,
                    expr_simplify_aux(st, car(expr), hash),
                    expr_simplify_aux(st, cdr(expr), hash));
    break;

  default:
    res = find_node(type,
                    expr_simplify_aux(st, car(expr), hash),
                    expr_simplify_aux(st, cdr(expr), hash));
  }

  /* memoize */
  insert_assoc(hash, expr, res);
  return EXPR(res);
}


static Expr_ptr expr_bool_const_to_number(const Expr_ptr a)
{
  if (Expr_is_false(a)) return find_node(NUMBER, NODE_FROM_INT(0), Nil);
  if (Expr_is_true(a)) return find_node(NUMBER, NODE_FROM_INT(1), Nil);
  return a;
}

/* evaluates expression in boolean context, i.e. casts
   NUMBER 0/1 to TRUE/FALSE.*/
static Expr_ptr expr_bool_to_bool(const Expr_ptr a)
{
  if (node_get_type(a) == NUMBER) {
    if (node_get_int(a) == 1) return Expr_true();
    if (node_get_int(a) == 0) return Expr_false();
  }
  return a;
}

/* casts boolean constants to WORD[1] */
static Expr_ptr expr_bool_to_word1(const Expr_ptr a)
{
  Expr_ptr _a = expr_bool_to_bool(a);
  if (Expr_is_true(_a)) {
    return find_node(NUMBER_UNSIGNED_WORD,
                     (node_ptr) WordNumber_from_integer(1,1), Nil);
  }

  if (Expr_is_false(_a)) {
    return find_node(NUMBER_UNSIGNED_WORD,
                     (node_ptr) WordNumber_from_integer(0,1), Nil);
  }

  return a;
}


/**Function********************************************************************

   Synopsis           [true if expression is timed]

   Description        [Private service of Expr_is_timed.
                       To represent 'true' in cache we use the constant 2 for
                       'false' we use 1 to avoid representation problems wrt Nil]

   SideEffects        [cache can be updated]

******************************************************************************/
static boolean expr_is_timed_aux(Expr_ptr expr, hash_ptr cache)
{
  Expr_ptr tmp;
  boolean result;

  nusmv_assert((hash_ptr)NULL != cache);

  if (expr == Nil) return false;

  tmp = find_assoc(cache, expr);
  if(Nil != tmp) {
    return (NODE_TO_INT(tmp) == 2);
  }

  switch (node_get_type(expr)) {
    /* leaves */
  case FAILURE:
  case ARRAY:
  case BIT:
  case DOT:
  case ATOM:
  case NUMBER_SIGNED_WORD:
  case NUMBER_UNSIGNED_WORD:
  case UWCONST:
  case SWCONST:
  case WORDARRAY:
  case NUMBER:
  case NUMBER_REAL:
  case NUMBER_FRAC:
  case NUMBER_EXP:
  case TRUEEXP:
  case FALSEEXP:
    return false;

  case ATTIME:
    result = true;
    break;
  case NEXT:
    result = false;
    break;

  default:
    {
      boolean ll;

      ll = expr_is_timed_aux(car(expr), cache);
      if(ll) {
        result = true;
      }
      else {
        result = expr_is_timed_aux(cdr(expr), cache);
      }
    }
  } /* switch */

  if(result) {
    insert_assoc(cache, expr, NODE_FROM_INT(2));
  }
  else {
    insert_assoc(cache, expr, NODE_FROM_INT(1));
  }

  return result;
}


/**Function********************************************************************

   Synopsis           [true if expression is booleanizable]

   Description        [Private service of Expr_is_booleanizable.
                       To represent 'true' in cache we use the constant 2 for
                       'false' we use 1 to avoid representation problems wrt Nil]

   SideEffects        [cache can be updated]

******************************************************************************/
static boolean expr_is_booleanizable_aux(Expr_ptr expr, const SymbTable_ptr st,
                                         boolean word_unbooleanizable,
                                         hash_ptr cache)
{
  node_ptr tmp;
  boolean res;

  if (Nil == expr) {
    return true;
  }

  tmp = find_assoc(cache, expr);
  if (Nil != tmp) {
    /* 2 means true */
    return (NODE_TO_INT(tmp) == 2);
  }

  res = true;
  switch (node_get_type(expr)) {
  case NUMBER:
  case NUMBER_SIGNED_WORD:
  case NUMBER_UNSIGNED_WORD:
  case TRUEEXP:
  case FALSEEXP:
  case FAILURE:
    res = true;
    break;

  case NUMBER_FRAC:
  case NUMBER_REAL:
  case NUMBER_EXP:
    res = false;
    break;

  case NFUNCTION:
    res = false;
    break;

  case BIT:
    res = true;
    break;

  case ATOM:
  case DOT:
  case ARRAY:
    if (SymbTable_is_symbol_var(st, expr)) {
      SymbType_ptr t = SymbTable_get_var_type(st, expr);
      res = !SymbType_is_infinite_precision(t);
      if(res) {
        if (SymbType_is_word(t)) {
          res = !word_unbooleanizable;
        }
      }
    }
    else if (SymbTable_is_symbol_define(st, expr)) {
      node_ptr body = SymbTable_get_define_flatten_body(st, expr);
      res = expr_is_booleanizable_aux(body, st, word_unbooleanizable, cache);
    }
    else if (SymbTable_is_symbol_function(st, expr)) {
      res = false;
    }
    else if (SymbTable_is_symbol_constant(st, expr)) {
      TypeChecker_ptr tc = SymbTable_get_type_checker(st);
      SymbType_ptr ty = TypeChecker_get_expression_type(tc, expr, Nil);
      /* Only real constants are unbooleanizable */
      res = !SymbType_is_real(ty);
    }
    else {
      rpterr("Unexpected symbol in Expr_is_booleanizable.");
      nusmv_assert(false);
    }
    break;

  default:
    {
      /* Lazy evaluation */
      res = expr_is_booleanizable_aux(car(expr), st, word_unbooleanizable,
                                      cache);
      if(res) {
        /* Check the cdr iff car is booleanizable */
        res = expr_is_booleanizable_aux(cdr(expr), st, word_unbooleanizable,
                                        cache);
      }
    }
  }

  if(res) {
    /* 2 means true */
    insert_assoc(cache, expr, NODE_FROM_INT(2));
  }
  else {
    /* 1 means false */
    insert_assoc(cache, expr, NODE_FROM_INT(1));
  }

  return res;
}
