/**CFile***********************************************************************

  FileName    [traceEval.c]

  PackageName [trace]

  Synopsis    [This module contains defines evaluation code]

  Description []

  SeeAlso     []

  Author      [Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace'' package of NuSMV version 2.
  Copyright (C) 2010 by FBK.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/
#include "pkg_traceInt.h"
#include "Trace.h"
#include "Trace_private.h"
#include "node/node.h"
#include "parser/symbols.h"
#include "enc/bdd/BddEnc.h" /* For BDD Encoder */
#include "utils/assoc.h"
#include "utils/NodeList.h"
#include "bmc/bmcConv.h"

static char rcsid[] UTIL_UNUSED = "$Id: $";

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

inline static Expr_ptr
trace_evaluate_define ARGS((const Trace_ptr trace, const TraceIter step,
                            node_ptr define, hash_ptr cache));

static Expr_ptr
trace_evaluate_expr_recur ARGS((const Trace_ptr trace,
                                const TraceIter step,
                                Expr_ptr expr, boolean in_next,
                                hash_ptr cache));

static node_ptr
trace_make_failure ARGS((const char* tmpl, node_ptr symbol));


/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/
boolean trace_step_check_defines(const Trace_ptr self, const TraceIter step,
                                 NodeList_ptr failures)
{
  hash_ptr cache = new_assoc();
  TraceSymbolsIter sym_iter;
  node_ptr sym;
  node_ptr val;
  boolean res = true; /* no error */

  CHECK(TRACE_END_ITER != step);
  NODE_LIST_CHECK_INSTANCE(failures);
  nusmv_assert(0 == NodeList_get_length(failures));

  /* two distinct phases: state and transitional defines. 'State' belong
     to current step, 'transitional' belong to next */

  /* 1. state defines */
  TRACE_SYMBOLS_FOREACH(self, TRACE_ITER_S_DEFINES, sym_iter, sym) {
    if (Nil != trace_step_get_value(self, step, sym)) {
      val = trace_evaluate_define(self, step, sym, cache);
      if (FAILURE != node_get_type(val)) {
        node_ptr exp_val = trace_step_get_value(self, step, sym);
        if (exp_val != val) {
          SymbCategory cat = trace_section_to_category(sym_iter.section);

          const char* fail_tmpl = \
            "Value mismatch for symbol %s (%s) calculated: %s, expected: %s";

          const char* cat_repr = trace_symb_category_to_string(cat);

          char *symb_repr = sprint_node(sym);
          char* calc_repr = sprint_node(val);
          char* expd_repr = sprint_node(exp_val);

          char *fail_repr = ALLOC(char, 1 +            \
                                  strlen(fail_tmpl) +  \
                                  strlen(symb_repr) +  \
                                  strlen(cat_repr)  +  \
                                  strlen(calc_repr) +  \
                                  strlen(expd_repr));

          sprintf(fail_repr,
                  fail_tmpl, symb_repr, cat_repr, calc_repr, expd_repr);

          NodeList_append(failures, trace_make_failure(fail_repr, Nil));

          FREE(symb_repr);
          FREE(calc_repr);
          FREE(expd_repr);
          FREE(fail_repr);

          res = false;
        }
      }
    }
  }

  /* 2. transitional defines (exist only if there's next state) */
  TraceIter next = trace_iter_get_next(step);
  if (TRACE_END_ITER != next) {
    TRACE_SYMBOLS_FOREACH(self, TRACE_ITER_TRANSITIONAL, sym_iter, sym) {
      if (Nil != trace_step_get_value(self, next, sym)) {
        /* this is a bit tricky: evaluation takes place in 'step'
           but being transitional, the resulting value belongs to
           next and must be checked accordingly.
        */
        val = trace_evaluate_define(self, step, sym, cache); /* calc */
        if (FAILURE != node_get_type(val)) {
          node_ptr exp_val = trace_step_get_value(self, next, sym);
          if (exp_val != val) {
            SymbCategory cat = trace_section_to_category(sym_iter.section);

            const char* fail_tmpl = \
              "Value mismatch for symbol %s (%s) calculated: %s, expected: %s";

            const char* cat_repr = trace_symb_category_to_string(cat);

            char *symb_repr = sprint_node(sym);
            char* calc_repr = sprint_node(val);
            char* expd_repr = sprint_node(exp_val);

            char *fail_repr = ALLOC(char, 1 +            \
                                    strlen(fail_tmpl) +  \
                                    strlen(symb_repr) +  \
                                    strlen(cat_repr)  +  \
                                    strlen(calc_repr) +  \
                                    strlen(expd_repr));

            sprintf(fail_repr,
                    fail_tmpl, symb_repr, cat_repr, calc_repr, expd_repr);

            NodeList_append(failures, trace_make_failure(fail_repr, Nil));

            /* cleanup */
            FREE(symb_repr);
            FREE(calc_repr);
            FREE(expd_repr);
            FREE(fail_repr);

            res = false;
          }
        }
      }
    }
  }

  free_assoc(cache);

  return res;
} /* trace_step_check_defines */


/**Function********************************************************************

  Synopsis    [Evaluates defines for a trace]

  Description [Evaluates define for a trace, based on assignments to
               state, frozen and input variables.

               If a previous value exists for a define, The mismatch
               is reported to the caller by appending a failure node
               describing the error to the "failures" list. If
               "failures" is NULL failures are silently discarded.  If
               no previous value exists for a given define, assigns
               the define to the calculated value according to vars
               assignments. The "failures" list must be either NULL
               or a valid, empty list.

               0 is returned if no mismatching were detected, 1
               otherwise ]

  SideEffects [The trace is filled with defines, failures list is
               populated as necessary.]

  SeeAlso     []

******************************************************************************/
void trace_step_evaluate_defines(Trace_ptr self, const TraceIter step)
{
  hash_ptr cache = new_assoc();
  TraceSymbolsIter sym_iter;
  node_ptr sym;
  node_ptr val;

  CHECK(TRACE_END_ITER != step);
  /* two distinct phases: state and transitional defines. 'State' belong
     ro current step, 'transitional' belong to next */

  /* 1. state defines */
  TRACE_SYMBOLS_FOREACH(self, TRACE_ITER_S_DEFINES, sym_iter, sym) {
    if (Nil == trace_step_get_value(self, step, sym)) {
      val = trace_evaluate_define(self, step, sym, cache);
      if (FAILURE != node_get_type(val)) {
        trace_step_put_value(self, step, sym, val);
      }
    }
  } /* foreach state define */

    /* 2. transitional defines (exist only if there's next state) */
  TraceIter next = trace_iter_get_next(step);
  if (TRACE_END_ITER != next) {
    TRACE_SYMBOLS_FOREACH(self, TRACE_ITER_TRANSITIONAL, sym_iter, sym) {
      if (Nil == trace_step_get_value(self, next, sym)) {
        val = trace_evaluate_define(self, step, sym, cache); /* step */
        if (FAILURE != node_get_type(val)) {
          trace_step_put_value(self, next, sym, val);
        }
      }
    }
  } /* foreach transitional define */

  free_assoc(cache);
} /* trace_step_evaluate_defines */

/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis           [Evaluates a define in a given environment]

  Description        [Evaluates a define in a given environment. Missing
                      dependencies are represented by FAILURE nodes.

                      This function is a private service of
                      Trace_evaluate_defines]

  SideEffects        [None]

  SeeAlso            [Trace_evaluate_defines]

******************************************************************************/
inline static Expr_ptr trace_evaluate_define(const Trace_ptr trace,
                                             const TraceIter step,
                                             node_ptr define,
                                             hash_ptr cache)
{
  SymbTable_ptr st = trace_get_symb_table(trace);
  TypeChecker_ptr tc = SymbTable_get_type_checker(st);
  Expr_ptr res, expr = SymbTable_get_define_flatten_body(st, define);

  /* Defines on arrays must be skipped (see issue #1075) */
  SymbType_ptr expr_type = TypeChecker_get_expression_type(tc, expr, Nil);
  if (SymbType_is_array(expr_type)) {
      const char* fail_msg = "Array defines not currenty supported";
      return trace_make_failure(fail_msg, Nil);
  }

  res = trace_evaluate_expr_recur(trace, step, expr, false, cache);
  CHECK(Nil != res);

  if (FAILURE == node_get_type(res)) { return res; }

  return trace_normalize_value(trace, define, res);
} /* trace_evaluate_define */


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Evaluates a define in a given environment]

  Description        [This function is a private service of
                      Trace_evaluate_expr]

  SideEffects        [None]

  SeeAlso            [Trace_evaluate_expr]

******************************************************************************/
static Expr_ptr trace_evaluate_expr_recur(const Trace_ptr trace,
                                          const TraceIter step,
                                          Expr_ptr expr, boolean in_next,
                                          hash_ptr cache)
{
  SymbTable_ptr st = trace_get_symb_table(trace);
  Expr_ptr res = Nil;

  /* corner cases: they can all be handled together */
  if ((Nil == expr)  || FAILURE == node_get_type(expr) ||
      SymbTable_is_symbol_constant(st, expr) ||
      Compile_is_symbol_constant(expr)) {

    return expr;
  }

  /* key for result memoization */
  const node_ptr key = in_next ? find_node(NEXT, expr, Nil) : expr;

  /* if a memoized result is available, return */
  res = find_assoc(cache, key);
  if (Nil != res) return res;

  switch(node_get_type(expr)) {

  case ARRAY:
  case ATOM:
  case DOT:
    {
      TraceSection section;
      unsigned dummy;

      if (trace_symbol_fwd_lookup(trace, expr, &section, &dummy)) {
        const char* fail_msg = "Unresolved dependency: '%s'";

        /* if var is input, fetch its value from next step */
        if (TRACE_SECTION_INPUT_VAR == section) {
          CHECK(TRACE_END_ITER != step);
          res = trace_step_get_value(trace, trace_iter_get_next(step), expr);
        }

        /* if var is state, fetch its value according to in_next */
        else if (TRACE_SECTION_STATE_VAR == section) {
          CHECK(TRACE_END_ITER != step);
          if (in_next) {
            TraceIter next_step = trace_iter_get_next(step);
            if (TRACE_END_ITER != next_step) {
              res = trace_step_get_value(trace, next_step, expr);
            }
          } else res = trace_step_get_value(trace, step, expr);
        }

        else if (TRACE_SECTION_FROZEN_VAR == section) {
          /* if var is frozen, step doesn't matter */
          res = trace_step_get_value(trace, TRACE_END_ITER, expr);
        }

        else {
          SymbCategory cat = trace_section_to_category(section);
          internal_error("%s:%d:%s: unexpected symbol '%s' (%s)",
                         __FILE__, __LINE__, __func__, sprint_node(expr),
                         trace_symb_category_to_string(cat));
        }

        if (Nil == res) { res = trace_make_failure(fail_msg, key); }
      } /* symbol in language */
      else  {
        const char* fail_msg = "Uknown symbol: '%s'";
        res = trace_make_failure(fail_msg, key);
      }

      break;
    } /* array, atom, dot */

    /** unary ops */
  case NOT:
  case UMINUS:
    {
      Expr_ptr tmp;
      node_ptr left_expr = car(expr);

      /* handle failure conditions */
      left_expr = trace_evaluate_expr_recur(trace, step,
                                            left_expr, in_next, cache);
      if (FAILURE == node_get_type(left_expr)) return left_expr;

      tmp = find_node(node_get_type(expr), left_expr, Nil);
      res = find_assoc(cache, tmp);
      if (Nil == res) res = trace_simplify_expr(st, tmp);

      break;
    }

  case NEXT:
    {
      nusmv_assert(!in_next); /* no nested NEXTs allowed */
      return trace_evaluate_expr_recur(trace, step, car(expr), true, cache);
    }

    /** binary ops */
  case LT: case GT: case LE: case GE:
  case NOTEQUAL: case EQUAL: case EQDEF:
  case CONS: case AND: case OR: case XOR:
  case XNOR: case IFF: case IMPLIES: case PLUS:
  case MINUS: case TIMES: case DIVIDE: case MOD:
  case LSHIFT: case RSHIFT: case UNION: case SETIN:

    /** word bin ops */
  case COLON:
  case CONCATENATION: case EXTEND: case LROTATE: case RROTATE:
    {
      Expr_ptr tmp;
      node_ptr left_expr = car(expr);
      node_ptr right_expr = cdr(expr);

      /* handle failure conditions */
      if (Nil != left_expr) {
        left_expr = trace_evaluate_expr_recur(trace, step,
                                              left_expr, in_next, cache);
        if (FAILURE == node_get_type(left_expr)) return left_expr;
      }

      if (Nil != right_expr) {
        right_expr = trace_evaluate_expr_recur(trace, step,
                                               right_expr, in_next, cache);
        if (FAILURE == node_get_type(right_expr)) return right_expr;
      }

      tmp = find_node(node_get_type(expr), left_expr, right_expr);
      res = find_assoc(cache, tmp);

      if (SETIN == node_get_type(expr)) {
        /* evaluate SETIN predicates semantically, by building set representation
           for left and right and verifying that left is contained into right */
        Set_t left = Set_MakeFromUnion(left_expr);
        Set_t right = Set_MakeFromUnion(right_expr);

        res = Set_Contains(right, left) ? Expr_true() : Expr_false();
        Set_ReleaseSet(left);
        Set_ReleaseSet(right);
      }
      if (Nil == res) res = trace_simplify_expr(st, tmp);

      break;
     } /* binary ops */
  case BIT_SELECTION:
    {
      Expr_ptr m, M, tmp;
      Expr_ptr w = car(expr);
      Expr_ptr bits = cdr(expr);

      nusmv_assert(COLON == node_get_type(bits));

      m = car(bits);
      M = cdr(bits);

      /* To avoid recursing on the COLON of the BIT selection */
      if (Nil != m) {
        m = trace_evaluate_expr_recur(trace, step, car(bits), in_next, cache);
        if (FAILURE == node_get_type(m)) return m;
      }
      if (Nil != M) {
        M = trace_evaluate_expr_recur(trace, step, cdr(bits), in_next, cache);
        if (FAILURE == node_get_type(M)) return M;
      }
      if (Nil != w) {
        w = trace_evaluate_expr_recur(trace, step, w, in_next, cache);
        if (FAILURE == node_get_type(w)) return w;
      }
      tmp = find_node(node_get_type(expr), w, find_node(COLON, m, M));

      res = find_assoc(cache, tmp);

      if (Nil == res) res = trace_simplify_expr(st, tmp);

      break;
    }
  case WRESIZE:
    {
      Expr_ptr tmp;
      Expr_ptr wexpr = car(expr);
      Expr_ptr wsize = cdr(expr);

      /* handle failure conditions */
      if (Nil != wexpr) {
        wexpr = trace_evaluate_expr_recur(trace, step,
                                          wexpr, in_next, cache);
        if (FAILURE == node_get_type(wexpr)) return wexpr;
      }
      if (Nil != wsize) {
        wsize = trace_evaluate_expr_recur(trace, step,
                                          wsize, in_next, cache);
        if (FAILURE == node_get_type(wsize)) return wsize;
      }

      tmp = find_node(node_get_type(expr), wexpr, wsize);
      res = find_assoc(cache, tmp);

      if (Nil == res) res = trace_simplify_expr(st, tmp);

      break;
    }
  case CAST_SIGNED:
  case CAST_UNSIGNED:
  case CAST_WORD1:
    {
      Expr_ptr tmp;
      Expr_ptr e = car(expr);

      /* handle failure conditions */
      if (Nil != e) {
        e = trace_evaluate_expr_recur(trace, step, e, in_next, cache);
        if (FAILURE == node_get_type(e)) return e;
      }

      tmp = find_node(node_get_type(expr), e, Nil);
      res = find_assoc(cache, tmp);

      if (Nil == res) res = trace_simplify_expr(st, tmp);
      break;
    }
  case CAST_BOOL:
    {
      Expr_ptr tmp;
      Expr_ptr w = car(expr);

      /* handle failure conditions */
      if (Nil != w) {
        w = trace_evaluate_expr_recur(trace, step, w, in_next, cache);
        if (FAILURE == node_get_type(w)) return w;
      }

      tmp = find_node(node_get_type(expr), w, Nil);
      res = find_assoc(cache, tmp);

      if (Nil == res) res = trace_simplify_expr(st, tmp);
      break;
    }
    /** ternary ops */
  case IFTHENELSE:
  case CASE:
    {
      Expr_ptr cond_expr = caar(expr);
      Expr_ptr cond_value = trace_evaluate_expr_recur(trace, step, cond_expr,
                                                      in_next, cache);
      if (FAILURE == node_get_type(cond_value)) {
        return cond_value;
      }

      /* condition is a predicate */
      nusmv_assert (Expr_is_true(cond_value) || Expr_is_false(cond_value));
      if (Expr_is_true(cond_value)) {
        res = trace_evaluate_expr_recur(trace, step, cdar(expr), in_next, cache);
      }
      else {
        res = trace_evaluate_expr_recur(trace, step, cdr(expr), in_next, cache);
      }

      break;
    }

  case FAILURE:
    return expr;

  default: internal_error("%s:%d:%s Unsupported node type (%d)",
                          __FILE__, __LINE__, __func__,
                          node_get_type(expr));
  } /* of switch */

  /* memoize results */
  insert_assoc(cache, key, res);
  return res;
}


/**Function********************************************************************

  Synopsis           [Private service of trace_evaluate_expr_recur]

  Description        [Private service of trace_evaluate_expr_recur]

  SideEffects        []

  SeeAlso            [Private service of trace_evaluate_expr_recur]

******************************************************************************/
static node_ptr
trace_make_failure(const char* tmpl, node_ptr symbol)
{
  char *symb_str = sprint_node(symbol);
  char *buf = ALLOC(char, 1 + strlen(tmpl) + strlen(symb_str));
  node_ptr res;

  sprintf(buf, tmpl, symb_str);
  res = failure_make(buf, FAILURE_UNSPECIFIED, -1);

  FREE(buf);
  FREE(symb_str);

  return res;
}


/**Function********************************************************************

  Synopsis           [Simplifies an expression using core simplifier and
                      MathSAT]

  Description        [MathSAT is currently required to evaluate expressions
                      over reals]

******************************************************************************/
Expr_ptr trace_simplify_expr(const SymbTable_ptr st, Expr_ptr expr)
{
  Expr_ptr res = Expr_simplify(st, expr);

  switch (node_get_type(res)) {
  case FALSEEXP:
  case TRUEEXP:
  case NUMBER:
  case NUMBER_SIGNED_WORD:
  case NUMBER_UNSIGNED_WORD:
  case UMINUS:
  case UNION:
    return res;
  }

  internal_error ("%s:%d:%s Evaluation failure for expr %s",
                  __FILE__, __LINE__, __func__,
                  sprint_node(expr));

  return Nil; /* unreachable */
}
