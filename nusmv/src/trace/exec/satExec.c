/**CFile***********************************************************************

  FileName    [satExec.c]

  PackageName [trace.exec]

  Synopsis    [This module contains the functions to support SAT basd
               trace re-execution]

  Description [optional]

  SeeAlso     [optional]

  Author      [Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace.exec'' package of NuSMV version 2.
  Copyright (C) 2009 by FBK.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation;
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/
#include "bmc/bmc.h"
#include "compile/compile.h"
#include "traceExec.h"

#include "pkg_trace.h"
#include "pkg_traceInt.h"

#include "parser/symbols.h"
#include "Trace_private.h"

static char rcsid[] UTIL_UNUSED = "$Id: satExec.c,v 1.1.2.15 2010-02-24 18:35:42 nusmv Exp $";

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static be_ptr
trace_exec_get_initial_state ARGS((BeFsm_ptr be_fsm));

static be_ptr
trace_exec_get_transition_relation ARGS((BeFsm_ptr be_fsm));

inline static Be_Cnf_ptr
bmc_add_be_into_solver ARGS((SatSolver_ptr solver,
                             SatSolverGroup group,
                             be_ptr prob, int polarity,
                             BeEnc_ptr be_enc));

inline static void
bmc_add_be_into_solver_positively ARGS((SatSolver_ptr solver,
                                        SatSolverGroup group,
                                        be_ptr prob, BeEnc_ptr be_enc));

/**AutomaticEnd***************************************************************/

/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis           [Executes a trace on the given fsm using SAT solver]

  Description        [The trace is executed using SAT solver, that is a proof
                      that the fsm is compatible with the trace is
                      built. Trace must be complete in order to
                      perform execution. If a non complete trace is
                      given, unpredictable results may occur.

                      The number of performed steps (transitions) is
                      returned. If the initial state is not compatible
                      -1 is returned.]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
int trace_sat_execute_trace(Trace_ptr trace,
                            TraceExecInfo_ptr exec_info)
{
  BeFsm_ptr be_fsm = BE_FSM(NULL);

  int res = -1; /* failure */
  TraceIter step;

  BeEnc_ptr be_enc;
  Be_Manager_ptr be_mgr;
  BddEnc_ptr bdd_enc;

  SatIncSolver_ptr solver;
  SatSolverGroup satGroup;
  SatSolverResult satResult;

  be_ptr be_current;
  be_ptr be_problem;

  /* the SAT solver instance */
  solver  = Sat_CreateIncSolver(get_sat_solver(OptsHandler_get_instance()));
  if (SAT_INC_SOLVER(NULL) == solver) {

    fprintf(nusmv_stderr,
            "Incremental sat solver '%s' is not available.\n",
            get_sat_solver(OptsHandler_get_instance()));
    return -1;
  }

  /* 0- Check If NULL */
  TRACE_CHECK_INSTANCE(trace);
  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);

  nusmv_assert(EXEC_SAT == TraceExecInfo_engine(exec_info));
  be_fsm = (BeFsm_ptr)(TraceExecInfo_fsm(exec_info));
  BE_FSM_CHECK_INSTANCE(be_fsm);

  step = trace_first_iter(trace);
  nusmv_assert(TRACE_END_ITER != step);

  /* local references to encoders */
  bdd_enc = Enc_get_bdd_encoding();
  be_enc = BeFsm_get_be_encoding(be_fsm);
  be_mgr = BeEnc_get_be_manager(be_enc);

  { /* 1- Check Start State */
    satGroup = SatIncSolver_create_group(solver);

    /* pick the initial state from the trace */
    be_current = \
      TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                             be_enc, bdd_enc);

    be_problem = Be_And(be_mgr,  be_current,
                        trace_exec_get_initial_state(be_fsm));

    /* push the problem into the SAT solver */
    bmc_add_be_into_solver_positively(SAT_SOLVER(solver), satGroup,
                                      be_problem, be_enc);

    satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));
    SatIncSolver_destroy_group(solver, satGroup);
  }

  if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
    fprintf(nusmv_stderr,
            "Error: starting state is not initial state\n");
  }
  else {
    boolean terminate = false;
    nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);

    ++ res;

    /* in SAT/SMT approach it is possibile to factorize the trans. relation */
    bmc_add_be_into_solver_positively(SAT_SOLVER(solver),
              SatSolver_get_permanent_group(SAT_SOLVER(solver)),
              trace_exec_get_transition_relation(be_fsm), be_enc);

    /* 2- Check Consecutive States are related by transition relation */
    do {

      step = TraceIter_get_next(step);
      if (TRACE_END_ITER != step) {
        /* create problem and push into the SAT solver current state,
           next input, next transitional, next state.  SAT problem for
           complete trace re-execution (untimed) */
        be_ptr be_input = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_I_SYMBOLS,
                                 be_enc, bdd_enc);
        be_ptr be_comb = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_COMBINATORIAL,
                                 be_enc, bdd_enc);
        be_ptr be_next = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                                 be_enc, bdd_enc);

        be_problem = \
          Be_And(be_mgr, be_current,
                 Be_And(be_mgr, be_comb,
                      Be_And(be_mgr, be_input,
                             BeEnc_shift_curr_to_next(be_enc, be_next))));

        if (TraceExecInfo_verbosity(exec_info)) {
          fprintf(TraceExecInfo_output_stream(exec_info),
                  "-- executing step %d ... ", 1+res);
          fflush(TraceExecInfo_output_stream(exec_info));
        }

        satGroup = SatIncSolver_create_group(solver);
        bmc_add_be_into_solver_positively(SAT_SOLVER(solver), satGroup,
                                          be_problem, be_enc);

        satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));
        SatIncSolver_destroy_group(solver, satGroup);

        if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "failed!\n");
          }
          terminate = true;
        }

        else {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "ok\n");
          }
          ++ res;

          nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);
          be_current = be_next;
        }
      } else {
        if (0 == res) {
          fprintf(nusmv_stderr, "Warning: trace has no transitions.\n");
        }
        terminate = true;
      }
    } while (!terminate); /* loop on state/input pairs */
  }

  SatIncSolver_destroy(solver);

  return res;
}


/*Function********************************************************************

  Synopsis           [Fills a partial trace on the given fsm using SAT]

  Description        [The trace is executed using SAT, that is a proof
                      that the fsm is compatible with the trace is
                      built. Uncomplete traces are filled-in with
                      compatible values for state variables. Restart
                      from complete states is not performed

                      If trace is compatible, a new complete trace is
                      registered in the TraceManager and its index is written
                      into new_trace;

                      Given trace can be either complete or
                      incomplete.

                      The number of performed steps (transitions) is
                      returned. If the initial state is not compatible
                      -1 is returned.]

  SideEffects        [A complete trace will be registered into the Trace
                      Manager upon successful completion]

  SeeAlso            [trace_sat_execute_partial_trace_restart]

****************************************************************************/
int trace_sat_execute_partial_trace(Trace_ptr trace,
                                    TraceExecInfo_ptr exec_info,
                                    int* trace_index)
{
  BeFsm_ptr be_fsm = BE_FSM(NULL);

  int res = -1; /* failure */
  SatIncSolver_ptr solver;
  SatSolverResult satResult;

  boolean success = true;

  BeEnc_ptr be_enc;
  Be_Manager_ptr be_mgr;
  BddEnc_ptr bdd_enc;

  be_ptr be_current;
  be_ptr be_trans;
  be_ptr be_problem;

  TraceIter step;

  /* the SAT solver instance */
  solver  = Sat_CreateIncSolver(get_sat_solver(OptsHandler_get_instance()));
  if (SAT_INC_SOLVER(NULL) == solver) {
    fprintf(nusmv_stderr, "Incremental sat solver '%s' is not available.\n",
            get_sat_solver(OptsHandler_get_instance()));
    return -1;
  }

  /* 0- Check If NULL */
  TRACE_CHECK_INSTANCE(trace);
  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);
  nusmv_assert( (int*) NULL != trace_index );

  nusmv_assert(EXEC_SAT == TraceExecInfo_engine(exec_info));
  be_fsm = (BeFsm_ptr)(TraceExecInfo_fsm(exec_info));
  BE_FSM_CHECK_INSTANCE(be_fsm);

  step = trace_first_iter(trace);
  nusmv_assert(TRACE_END_ITER != step);

  /* local references to encoders */
  bdd_enc =  Enc_get_bdd_encoding();
  be_enc = BeFsm_get_be_encoding(be_fsm);
  be_mgr = BeEnc_get_be_manager(be_enc);

  { /* 1- Check initial state */
    be_current = \
      TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                             be_enc, bdd_enc);
    be_problem = Be_And(be_mgr,
                        BeEnc_untimed_expr_to_timed(be_enc, be_current, 0),
                        Bmc_Model_GetInit0(be_fsm));

    /* push the problem into the SAT solver (permanent push) */
    bmc_add_be_into_solver_positively(SAT_SOLVER(solver),
       SatSolver_get_permanent_group(SAT_SOLVER(solver)), be_problem, be_enc);

    satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));
  }

  if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
    fprintf(nusmv_stderr,
            "Error: starting state is not initial state\n");
    success = false;
  }
  else {
    boolean terminate = false;
    nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);

    ++ res;

    be_trans = trace_exec_get_transition_relation(be_fsm);

    /* 2- Check Consecutive States are related by transition relation */
    do {
      be_ptr be_input;
      be_ptr be_comb;
      be_ptr be_next;

      step = TraceIter_get_next(step);
      if (TRACE_END_ITER != step) {

        if (TraceExecInfo_verbosity(exec_info)) {
          fprintf(TraceExecInfo_output_stream(exec_info),
                  "-- executing step %d ... ", 1+res);
          fflush(TraceExecInfo_output_stream(exec_info));
        }

        be_input = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_I_SYMBOLS,
                                 be_enc, bdd_enc);
        be_comb = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_COMBINATORIAL,
                                 be_enc, bdd_enc);
        be_next = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                                 be_enc, bdd_enc);

        /* create problem, time it, and push into the SAT solver */
        be_problem = \
          BeEnc_untimed_expr_to_timed(be_enc,
                                      Be_And(be_mgr, be_trans,
                                      Be_And(be_mgr,
                                             Be_And(be_mgr,
                                                    be_input,
                                                    be_comb),
            BeEnc_shift_curr_to_next(be_enc, be_next))), res);

        /* permanent push */
        bmc_add_be_into_solver_positively(SAT_SOLVER(solver),
            SatSolver_get_permanent_group(SAT_SOLVER(solver)),
                                          be_problem, be_enc);

        satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));

        if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "failed!\n");
          }
          success = false;
          terminate = true;
        }
        else {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "ok\n");
          }
          ++ res;

          nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);
        }
      }
      else {
        if (0 == res) {
          fprintf(nusmv_stderr, "Warning: trace has no transitions.\n");
        }
        terminate = true;
      }
    } while (!terminate); /* loop on input/state pairs */
  }

  /* register a new trace if execution was successful */
  if (success) {
    Trace_ptr out_trace;
    const char* trace_description = "BMC Execution";

    fprintf(TraceExecInfo_output_stream(exec_info),
            "Trace was succesfully completed.\n");

    out_trace = \
      Bmc_Utils_generate_cntexample(be_enc, SAT_SOLVER(solver), bdd_enc,
                                    be_problem, res, trace_description,
                                    TraceExecInfo_language(exec_info));

    /* can be overwritten  by caller */
    Trace_set_type(out_trace, TRACE_TYPE_EXECUTION);

    *trace_index = \
      TraceManager_register_trace(TracePkg_get_global_trace_manager(),
                                  out_trace);
  }

  SatIncSolver_destroy(solver);

  return res;
}


/*Function********************************************************************

  Synopsis           [Fills a partial trace on the given fsm using SAT]

  Description        [The trace is executed using SAT, that is a proof
                      that the fsm is compatible with the trace is
                      built. Uncomplete traces are filled-in with
                      compatible values for state variables. Restart
                      from complete states is performed.

                      If trace is compatible, a new complete trace is
                      registered in the TraceManager and its index is written
                      into trace_index;

                      The number of performed steps (transitions) is
                      returned. If the initial state is not compatible
                      -1 is returned.]

  SideEffects        [A complete trace will be registered into the Trace
                      Manager upon successful completion]

  SeeAlso            [trace_sat_execute_partial_trace]

********************************************************************************/
int trace_sat_execute_partial_trace_restart(Trace_ptr trace,
                                            TraceExecInfo_ptr exec_info,
                                            int* trace_index)
{
  BeFsm_ptr be_fsm = BE_FSM(NULL);
  Trace_ptr out_trace = TRACE(NULL);

  int res = -1, time = -1; /* failure */
  boolean success = true;

  TraceIter step;

  BeEnc_ptr be_enc;
  Be_Manager_ptr be_mgr;
  BddEnc_ptr bdd_enc;
  be_ptr be_current;
  be_ptr be_trans;
  be_ptr be_problem;

  SatIncSolver_ptr solver;
  SatSolverGroup satGroup;
  SatSolverResult satResult;

  const char* trace_description = "BMC Execution";

  nusmv_assert ( (int*) NULL != trace_index );

  /* the SAT solver instance */
  solver  = Sat_CreateIncSolver(get_sat_solver(OptsHandler_get_instance()));
  if (SAT_INC_SOLVER(NULL) == solver) {
    fprintf(nusmv_stderr,
            "Incremental sat solver '%s' is not available.\n",
            get_sat_solver(OptsHandler_get_instance()));
    return -1;
  }

  /* 0- Check If NULL */
  TRACE_CHECK_INSTANCE(trace);
  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);

  nusmv_assert(EXEC_SAT == TraceExecInfo_engine(exec_info));
  be_fsm = (BeFsm_ptr)(TraceExecInfo_fsm(exec_info));
  BE_FSM_CHECK_INSTANCE(be_fsm);

  step = trace_first_iter(trace);
  nusmv_assert(TRACE_END_ITER != step);

  /* local references to encoders */
  bdd_enc =  Enc_get_bdd_encoding();
  be_enc = BeFsm_get_be_encoding(be_fsm);
  be_mgr = BeEnc_get_be_manager(be_enc);

  { /* 1- Check initial State */

    /* pick the initial state from the trace */
    be_current = \
      TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                             be_enc, bdd_enc);

    be_problem = Be_And(be_mgr,
                        BeEnc_untimed_expr_to_timed(be_enc, be_current, 0),
                        Bmc_Model_GetInit0(be_fsm));

    /* push the problem into the SAT solver */
    satGroup = SatIncSolver_create_group(solver);
    bmc_add_be_into_solver_positively(SAT_SOLVER(solver), satGroup,
                                      be_problem, be_enc);

    satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));
  }

  if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
    fprintf(nusmv_stderr, "Error: starting state is not initial state\n");
    success = false;
  }
  else {
    boolean terminate = false;
    nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);

    ++ res;
    ++ time;

    be_trans = trace_exec_get_transition_relation(be_fsm);

    /* 2- Check Consecutive States are related by transition relation */
    do {
      step = trace_iter_get_next(step);
      if (TRACE_END_ITER != step) {

        if (TraceExecInfo_verbosity(exec_info)) {
          fprintf(TraceExecInfo_output_stream(exec_info),
                  "-- executing step %d ... ", 1+res);
          fflush(TraceExecInfo_output_stream(exec_info));
        }

        /* create problem, time it, and push into the SAT solver next
           input, next transitional, next state and the transition
           relation (previous state has already been pushed) */
         be_ptr be_input = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_I_SYMBOLS,
                                 be_enc, bdd_enc);
        be_ptr be_comb = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_COMBINATORIAL,
                                 be_enc, bdd_enc);

        be_ptr be_next = \
          TraceUtils_fetch_as_be(trace, step, TRACE_ITER_SF_SYMBOLS,
                                 be_enc, bdd_enc);

        /* SAT problem for incomplete trace re-execution (timed) */
        be_problem = \
          BeEnc_untimed_expr_to_timed(be_enc,
                                      Be_And(be_mgr, be_trans,
                                      Be_And(be_mgr,
                                             Be_And(be_mgr,
                                                    be_input,
                                                    be_comb),
            BeEnc_shift_curr_to_next(be_enc, be_next))), time);

        bmc_add_be_into_solver_positively(SAT_SOLVER(solver),
                                          satGroup, be_problem, be_enc);

        satResult = SatSolver_solve_all_groups(SAT_SOLVER(solver));

        if (SAT_SOLVER_UNSATISFIABLE_PROBLEM == satResult) {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "failed!\n");
          }
          success = false;
          terminate = true;
        }

        else {
          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info), "ok\n");
          }
          ++ res;
          ++ time;

          nusmv_assert(SAT_SOLVER_SATISFIABLE_PROBLEM == satResult);

          /* if last state was a complete one, perform restart */
          if (trace_exec_is_complete_state_assignment(trace, step)) {

            if (TraceExecInfo_verbosity(exec_info)) {
              fprintf(TraceExecInfo_output_stream(exec_info),
                      "-- complete state found, performing restart.\n");
            }

            if (TRACE(NULL) == out_trace) { /* no previous fragment exists */
              out_trace = \
                Bmc_Utils_generate_cntexample(be_enc, SAT_SOLVER(solver),
                                              bdd_enc, be_problem, time,
                                              trace_description,
                                              TraceExecInfo_language(exec_info));
            }
            else { /* append fragment to existing trace */
              Trace_ptr fragment = \
                Bmc_Utils_generate_cntexample(be_enc, SAT_SOLVER(solver),
                                              bdd_enc, be_problem, time,
                                              NIL(char),
                                              TraceExecInfo_language(exec_info));

              Trace_concat(out_trace, &fragment);
              nusmv_assert(TRACE(NULL) == fragment);
            }

            /* perform restart from last state */
            SatIncSolver_destroy_group(solver, satGroup);
            satGroup = SatIncSolver_create_group(solver);

            be_problem = \
              BeEnc_untimed_expr_to_timed(be_enc, be_next, 0);

            bmc_add_be_into_solver_positively(SAT_SOLVER(solver), satGroup,
                                              be_problem, be_enc);

            time = 0; /* restart */
          } /* is complete assignment */
        }
      }  /* TRACE_END_ITER != step */
      else {
        if (0 == res) {
          fprintf(nusmv_stderr, "Warning: trace has no transitions.\n");
        }
        terminate = true;
      }
    } while (!terminate);
  }

  /* register a new trace if execution was successful */
  if (success) {

    fprintf(TraceExecInfo_output_stream(exec_info),
            "Trace was succesfully completed.\n");

    if (0 < time) { /* last trace fragment to be extracted exists */
      if (TRACE(NULL) == out_trace) {
        out_trace = \
          Bmc_Utils_generate_cntexample(be_enc, SAT_SOLVER(solver),
                                        bdd_enc, be_problem, time,
                                        trace_description,
                                        TraceExecInfo_language(exec_info));
      }
      else { /* append this fragment to an existing trace */
        Trace_ptr fragment = \
          Bmc_Utils_generate_cntexample(be_enc, SAT_SOLVER(solver),
                                        bdd_enc, be_problem, time,
                                        NIL(char),
                                        TraceExecInfo_language(exec_info));

        Trace_concat(out_trace, &fragment);
        nusmv_assert(TRACE(NULL) == fragment);
      }
    }

    nusmv_assert(TRACE(NULL) != out_trace);
    if (opt_verbose_level_ge(OptsHandler_get_instance(), 2)) {
      fprintf(nusmv_stderr, "Registering complete trace");
    }
    *trace_index =
      TraceManager_register_trace(TracePkg_get_global_trace_manager(),
                                  out_trace);
  } /* if success */
  else { /* (!success) -> destroy trace fragment (if any) */
    if (TRACE(NULL) != out_trace) {
      Trace_destroy(out_trace);
    }
  }

  SatIncSolver_destroy(solver);

  return res;
}


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Builds the initial state formula]

  Description        [Builds the initial state formula]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
static be_ptr trace_exec_get_initial_state(BeFsm_ptr be_fsm)
{
  BeEnc_ptr be_enc = BeFsm_get_be_encoding(be_fsm);
  be_ptr init = Be_And(BeEnc_get_be_manager(be_enc),
                       BeFsm_get_init(be_fsm),
                       BeFsm_get_invar(be_fsm));

  return init;
}


/**Function********************************************************************

  Synopsis           [Builds the transition relation formula]

  Description        [Builds the transition relation formula]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
static be_ptr trace_exec_get_transition_relation(BeFsm_ptr be_fsm)
{
  BeEnc_ptr be_enc = BeFsm_get_be_encoding(be_fsm);
  Be_Manager_ptr mgr = BeEnc_get_be_manager(be_enc);

  be_ptr invar = BeFsm_get_invar(be_fsm);
  be_ptr trans = BeFsm_get_trans(be_fsm);
  be_ptr n_invar = BeEnc_shift_curr_to_next(be_enc, invar);

  return Be_And(mgr, invar, Be_And(mgr, trans, n_invar));
}

/**Function********************************************************************

  Synopsis      [Converts Be into CNF, and adds it into a group of a solver.]

  Description   [Outputs into nusmv_stdout the total time of conversion
                 and adding BE to solver. It is resposibility of the
                 invoker to destroy returned CNF (with Be_Cnf_Delete)]

  SideEffects   [creates an instance of CNF formula. (do not forget to
                 delete it)]

  SeeAlso       []

******************************************************************************/
static Be_Cnf_ptr bmc_add_be_into_solver(SatSolver_ptr solver,
                                         SatSolverGroup group,
                                         be_ptr prob,
                                         int polarity,
                                         BeEnc_ptr be_enc)
{
  Be_Manager_ptr be_mgr;
  Be_Cnf_ptr cnf;
  be_ptr inprob;

  /* Ensure that polarity is one of {1, 0, -1} */
  nusmv_assert((polarity == 1) || (polarity == 0) || (polarity == -1));

  be_mgr = BeEnc_get_be_manager(be_enc);

  /* We force inclusion of the conjunct set to guarantee soundness */
  inprob = Bmc_Utils_apply_inlining4inc(be_mgr, prob);

  cnf = Be_ConvertToCnf(be_mgr, inprob, polarity);
  SatSolver_add(solver, cnf, group);
  return cnf;
}


/**Function********************************************************************

  Synopsis      [Converts Be into CNF, and adds it into a group of a solver,
                 sets polarity to 1, and then destroys the CNF.]

  Description   [Outputs into nusmv_stdout the total time of conversion,
                  adding, setting polarity and destroying BE. ]

  SideEffects   []

  SeeAlso       []

******************************************************************************/
static void  bmc_add_be_into_solver_positively(SatSolver_ptr solver,
                                               SatSolverGroup group,
                                               be_ptr prob,
                                               BeEnc_ptr be_enc)
{
  Be_Cnf_ptr cnf;

  cnf =  bmc_add_be_into_solver(solver, group, prob, 1, be_enc);
  SatSolver_set_polarity(solver, cnf, 1, group);
  Be_Cnf_Delete(cnf);
}

