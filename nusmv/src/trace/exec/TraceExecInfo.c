/**CFile***********************************************************************

  FileName    [TraceExecInfo.c]

  PackageName [trace.exec]

  Synopsis    [Implementation of class 'TraceExecInfo']

  Description []

  SeeAlso     [TraceExecInfo.h]

  Author      [Alessandro Mariotti, Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace'' package of NuSMV version 2.
  Copyright (C) 2010 by FBK-irst.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

  Revision    [$Id: TraceExecInfo.c,v 1.1.2.3 2010-03-04 16:58:56 nusmv Exp $]

******************************************************************************/
#include "TraceExecInfo.h"
#include "trace/pkg_trace.h"
#include "trace/pkg_traceInt.h"
#include "utils/utils.h"


static char rcsid[] UTIL_UNUSED = "$Id: TraceExecInfo.c,v 1.1.2.3 2010-03-04 16:58:56 nusmv Exp $";


/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/**Struct**********************************************************************

  Synopsis    [TraceExecInfo class definition]

  Description []

  SeeAlso     []

******************************************************************************/
typedef struct TraceExecInfo_TAG
{
  /* -------------------------------------------------- */
  /*                  Private members                   */
  /* -------------------------------------------------- */
  execEngine engine;

  /* the FSM to run the trace against */
  void* fsm;

  NodeList_ptr language; /* for creating a new trace */

  /* verbose output */
  boolean verbosity;

  /* perform unique completion check (partial traces only) */ 
  boolean unique_completion;

  /* perform restart (partial traces only) */
  boolean restart;

  /* Debugging method to use upon bug detection */
  TraceExecDebuggingMethod debugging_method;

  /* MSAT UNSATCORE/INTERPOLANTS only (out param) */
  Set_t* explanation; 

  /* for output redirection */
  FILE* output_stream;
} TraceExecInfo;


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

static void trace_exec_info_init ARGS((TraceExecInfo_ptr self));
static void trace_exec_info_deinit ARGS((TraceExecInfo_ptr self));


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [The TraceExecInfo class constructor]

  Description        [The TraceExecInfo class constructor]

  SideEffects        []

  SeeAlso            [TraceExecInfo_destroy]

******************************************************************************/
TraceExecInfo_ptr TraceExecInfo_create()
{
  TraceExecInfo_ptr self = ALLOC(TraceExecInfo, 1);
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);

  trace_exec_info_init(self);
  return self;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo class destructor]

  Description        [The TraceExecInfo class destructor]

  SideEffects        []

  SeeAlso            [TraceExecInfo_create]

******************************************************************************/
void TraceExecInfo_destroy(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);

  trace_exec_info_deinit(self);
  FREE(self);
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo obfuscate field getter]

  Description        [The TraceExecInfo obfuscate field getter]

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_obfuscate]

******************************************************************************/
execEngine TraceExecInfo_engine(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->engine;
}

/**Function********************************************************************

  Synopsis           [The TraceExecInfo obfuscate field setter]

  Description        [The TraceExecInfo obfuscate field setter]

  SideEffects        []

  SeeAlso            [TraceExecInfo_obfuscate]

******************************************************************************/
void TraceExecInfo_set_engine(TraceExecInfo_ptr self, execEngine engine)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->engine = engine;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo fsm]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_fsm]

******************************************************************************/
void* TraceExecInfo_fsm(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->fsm;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo fsm setter for bdd fsm]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_obfuscate]

******************************************************************************/
void TraceExecInfo_set_fsm(TraceExecInfo_ptr self, void* fsm)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  nusmv_assert(NULL != fsm);

  self->fsm = fsm;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo restart flag]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_restart]

******************************************************************************/
boolean TraceExecInfo_restart(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->restart;
}


void TraceExecInfo_set_restart(TraceExecInfo_ptr self, boolean restart)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->restart = restart;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo unique completion flag]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_unique_completion]

******************************************************************************/
boolean TraceExecInfo_unique_completion(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->unique_completion;
}


void TraceExecInfo_set_unique_completion(TraceExecInfo_ptr self, boolean uc)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->unique_completion = uc;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo output stream]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_output_stream]

******************************************************************************/
FILE* TraceExecInfo_output_stream(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return (NIL(FILE) != self->output_stream) ? self->output_stream : nusmv_stdout;
}

void TraceExecInfo_set_output_stream(TraceExecInfo_ptr self, FILE* stream)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->output_stream = stream;
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo verbosity flag]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_verbosity]

******************************************************************************/
boolean TraceExecInfo_verbosity(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->verbosity;
}

void TraceExecInfo_set_verbosity(TraceExecInfo_ptr self, boolean verbosity)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->verbosity = verbosity;
}



/**Function********************************************************************

  Synopsis           [The TraceExecInfo debugging method]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_debugging_method]

******************************************************************************/
TraceExecDebuggingMethod
TraceExecInfo_debugging_method(TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->debugging_method;
}


void
TraceExecInfo_set_debugging_method(TraceExecInfo_ptr self,
                                   TraceExecDebuggingMethod method)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->debugging_method = method;
}

/**Function********************************************************************

  Synopsis           [The TraceExecInfo language]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_language]

******************************************************************************/
NodeList_ptr TraceExecInfo_language (TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  NODE_LIST_CHECK_INSTANCE(self->language);
  return self->language;
}

void TraceExecInfo_set_language (TraceExecInfo_ptr self, NodeList_ptr language)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  if (NODE_LIST(NULL) != self->language) { NodeList_destroy(self->language); }
  self->language = NodeList_copy(language);
}


/**Function********************************************************************

  Synopsis           [The TraceExecInfo explanation var set reference]

  Description        []

  SideEffects        []

  SeeAlso            [TraceExecInfo_set_explanation]

******************************************************************************/
Set_t* TraceExecInfo_explanation (TraceExecInfo_ptr self)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  return self->explanation;
}


EXTERN void
TraceExecInfo_set_explanation (TraceExecInfo_ptr self, Set_t* vars)
{
  TRACE_EXEC_INFO_CHECK_INSTANCE(self);
  self->explanation = vars;
}


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [The TraceExecInfo class private initializer]

  Description        [The TraceExecInfo class private initializer]

  SideEffects        []

  SeeAlso            [TraceExecInfo_create]

******************************************************************************/
static void trace_exec_info_init(TraceExecInfo_ptr self)
{
  /* members initialization */
  self->engine = EXEC_NONE;
  self->fsm = (void*)(NULL);
  self->restart = false;
  self->unique_completion = false;
  self->verbosity = false;
  self->language = NODE_LIST(NULL);
  self->output_stream = NIL(FILE);
  self->debugging_method = TRACE_DEBUG_NONE; /* by default no debugging */
  self->explanation = NIL(Set_t); /* by default, no explanation */
 }


/**Function********************************************************************

  Synopsis           [The TraceExecInfo class private deinitializer]

  Description        [The TraceExecInfo class private deinitializer]

  SideEffects        []

  SeeAlso            [TraceExecInfo_destroy]

******************************************************************************/
static void trace_exec_info_deinit(TraceExecInfo_ptr self)
{
}



/**AutomaticEnd***************************************************************/

