/**CHeaderFile*****************************************************************

  FileName    [TraceExecInfo.h]

  PackageName [trace.exec]

  Synopsis    [Public interface of class 'TraceExecInfo']

  Description []

  SeeAlso     [TraceExecInfo.c]

  Author      [Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace'' package of NuSMV version 2.
  Copyright (C) 2010 by FBK-irst.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

  Revision    [$Id: TraceOpt.h,v 1.1.2.3 2010-03-04 16:58:56 nusmv Exp $]

******************************************************************************/
#ifndef __TRACE_EXEC_INFO_H__
#define __TRACE_EXEC_INFO_H__

#include "trace/exec/traceExec.h"
#include "utils/NodeList.h"
#include "utils/utils.h"
#include "set/set.h"

typedef enum {
  TRACE_DEBUG_NONE=0, /* reserved */
  TRACE_DEBUG_SINGLE_CLAUSE=0x1,
  TRACE_DEBUG_UNIQUENESS=0x2,
  TRACE_DEBUG_UNSATCORE=0x4,
  TRACE_DEBUG_INTERPOLANTS=0x8
} TraceExecDebuggingMethod;

/**Type***********************************************************************

  Synopsis    [Definition of the public accessor for class TraceExecInfo]

  Description []

******************************************************************************/
typedef struct TraceExecInfo_TAG*  TraceExecInfo_ptr;

/**Macros**********************************************************************

  Synopsis    [To cast and check instances of class TraceExecInfo]
 Trace
  Description [These macros must be used respectively to cast and to
               check instances of class TraceExecInfo]

******************************************************************************/
#define TRACE_EXEC_INFO(self) \
         ((TraceExecInfo_ptr) self)

#define TRACE_EXEC_INFO_CHECK_INSTANCE(self) \
         (nusmv_assert(TRACE_EXEC_INFO(self) != TRACE_EXEC_INFO(NULL)))

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/

EXTERN TraceExecInfo_ptr TraceExecInfo_create ARGS((void));
EXTERN void TraceExecInfo_destroy ARGS((TraceExecInfo_ptr self));

EXTERN execEngine TraceExecInfo_engine ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_engine ARGS((TraceExecInfo_ptr self,
                                           execEngine engine));

EXTERN void* TraceExecInfo_fsm ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_fsm ARGS((TraceExecInfo_ptr self, void* fsm));

EXTERN boolean TraceExecInfo_restart ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_restart ARGS((TraceExecInfo_ptr self,
                                            boolean restart));

EXTERN boolean TraceExecInfo_verbosity ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_verbosity ARGS((TraceExecInfo_ptr self,
                                              boolean verbosity));

EXTERN boolean TraceExecInfo_check_unique ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_check_unique ARGS((TraceExecInfo_ptr self,
                                                 boolean check_unique));

EXTERN NodeList_ptr TraceExecInfo_language ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_language ARGS((TraceExecInfo_ptr self,
                                             NodeList_ptr language));

EXTERN FILE* TraceExecInfo_output_stream ARGS((TraceExecInfo_ptr self));
EXTERN void TraceExecInfo_set_output_stream ARGS((TraceExecInfo_ptr self,
                                                  FILE* stream));
EXTERN  TraceExecDebuggingMethod
TraceExecInfo_debugging_method ARGS((TraceExecInfo_ptr self));

EXTERN void
TraceExecInfo_set_debugging_method ARGS((TraceExecInfo_ptr self,
                                    TraceExecDebuggingMethod method));

EXTERN Set_t* TraceExecInfo_explanation ARGS((TraceExecInfo_ptr self));

EXTERN void
TraceExecInfo_set_explanation ARGS((TraceExecInfo_ptr self, Set_t* vars));

/**AutomaticEnd***************************************************************/

#endif /* __TRACE_EXEC_INFO_H__ */
