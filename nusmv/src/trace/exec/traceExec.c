/**CFile***********************************************************************

  FileName    [traceExec.c]

  PackageName [trace.exec]

  Synopsis    [This module contains the functions needed to support trace
               re-execution]

  Description [This module contains the functions needed to support trace
               re-execution]

  SeeAlso     []

  Author      [Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace.exec'' package of NuSMV version 2.
  Copyright (C) 2009 by FBK.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/
#include "pkg_trace.h"
#include "pkg_traceInt.h"

#include "compile/compile.h"
#include "bmc/bmc.h"
#include "Trace.h"
#include "traceExec.h"

static char rcsid[] UTIL_UNUSED = "$Id: traceExec.c,v 1.1.2.28 2010-02-12 16:25:47 nusmv Exp $";

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Converts an engine name into an engine identifier]

  Description        [Converts an engine name into an engine identifier.
                      Returns EXEC_UNDEFINED if no valid name is given.]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
execEngine TracePkg_execution_engine_from_string (const char* name)
{
  execEngine res = EXEC_NONE;
  if (NIL(const char) == name) return res;

  if (!strcmp(BDD_EXEC_ENGINE, name)) {
    res = EXEC_BDD;
  }

  else if (!strcmp(SAT_EXEC_ENGINE, name)) {
    res = EXEC_SAT;
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Complete trace re-execution]

  Description        [Complete trace re-execution.  Returns 0 if a trace is
                      executed successfully, and 1 otherwise.]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
int Trace_execute_trace(Trace_ptr trace, TraceExecInfo_ptr exec_info)
{
  int trace_len = Trace_get_length(trace);
  int res = 0, exec_res = -1;

  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);
  if (opt_verbose_level_gt(OptsHandler_get_instance(), 0)) {
    fprintf(nusmv_stderr, "Executing trace of length %d\n", trace_len);
  }

  switch (TraceExecInfo_engine(exec_info)) {
  case EXEC_BDD: /** BDD based execution engine */
    {
      exec_res = trace_bdd_execute_trace(trace, exec_info);
      break;
    }

  case EXEC_SAT: /** SAT based execution engine */
    {
      exec_res = trace_sat_execute_trace(trace, exec_info);
      break;
    }

  default: internal_error("%s:%d:%s: undefined execution mode",
                          __FILE__, __LINE__, __func__);
  } /* switch */

  /* first phase completed, now perform additional checks */
  if (trace_len == exec_res) {

    /* as a last check it is necessary to make sure loopback information
       is consistent (if trace is thawed this check is trivially
       true) */
    if (0 == res) {
      TraceIter step; int i = 1;

      if (opt_verbose_level_ge(OptsHandler_get_instance(), 4)) {
        fprintf(nusmv_stderr, "checking loopbacks...\n");
      }
      TRACE_FOREACH(trace, step) {
        if (trace_step_is_loopback(trace, step) &&
            !trace_step_test_loopback(trace, step)) {
          fprintf(nusmv_stderr, "*** Error ***\n"
                  "Inconsistent loopback information found at step %d.\n", i);
          res = 1;
          break; /*  continuing is pointless */
        }

        ++ i;
      } /* trace foreach */
    } /* check loopbacks */

    if (0 == res) {
      /* print results */
      if (trace_is_registered(trace)) {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Trace %d execution completed succesfully"
                "(%d steps performed).\n", trace_get_id(trace), trace_len);
      }
      else {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Trace execution completed succesfully"
                "(%d steps performed).\n", trace_len);
      }
    }
    else {
      if (trace_is_registered(trace)) {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Error in execution of trace %d: ",
                trace_get_id(trace));
      }
      else {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Error in execution of trace: ");
      }
    }
  } /* (trace_len == exec_res)*/
  else {
    res = 1; /* error */

    if (0 <= exec_res) {
      fprintf(TraceExecInfo_output_stream(exec_info),
              "step %d is not compatible with the model.\n",
              1 + exec_res);
    }
    else {
      fprintf(TraceExecInfo_output_stream(exec_info),
              "initial state is not compatible with the model.\n");
    }
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Partial trace re-execution and fill-in]

  Description        [Partial trace re-execution and fill-in.

                      Tries to complete the given trace using the
                      chosen incomplete trace re-execution engine.
                      If succesful, a complete trace is registered
                      into the Trace Manager.

                      The "vars" list is only needed if the "restart" flag
                      is set to true, and should be the set of vars of the model

                      0 is returned if trace could be succesfully completed.
                      1 is returned otherwise]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
int Trace_execute_partial_trace(Trace_ptr trace, TraceExecInfo_ptr exec_info)
{
  int trace_len = Trace_get_length(trace);
  int exec_res = -1;
  int res = 0; /* no errors */
  int new_trace;

  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);

  if (opt_verbose_level_gt(OptsHandler_get_instance(), 0)) {
    fprintf(nusmv_stderr, "Executing trace of length %d\n", trace_len);
  }

  switch (TraceExecInfo_engine(exec_info)) {
  case EXEC_BDD: /** BDD based execution engine */
    {
      BddFsm_ptr bdd_fsm = (BddFsm_ptr)(TraceExecInfo_fsm(exec_info));
      BDD_FSM_CHECK_INSTANCE(bdd_fsm);

      if (TraceExecInfo_restart(exec_info)) {
        internal_error ("%s:%d:%s: Restart not currently supported with "
                        "the BDD execution engine.",
                        __FILE__, __LINE__, __func__);
      }

      exec_res = trace_bdd_execute_partial_trace(trace, exec_info, &new_trace);
      break;
    }

  case EXEC_SAT: /** SAT based execution engine */
    {
      BeFsm_ptr be_fsm = (BeFsm_ptr)(TraceExecInfo_fsm(exec_info));
      BE_FSM_CHECK_INSTANCE(be_fsm);
      if (!TraceExecInfo_restart(exec_info)) { /* basic algorithm */
        exec_res = trace_sat_execute_partial_trace(trace, exec_info, &new_trace);
      }
      else { /* restart algorithm */
        exec_res = \
          trace_sat_execute_partial_trace_restart(trace, exec_info,
                                                  &new_trace);
      }

      break;
    }

  default: internal_error("%s:%d:%s: undefined execution mode",
                          __FILE__, __LINE__, __func__);
  } /* switch */

  /* first phase completed, now perform additional checks */
  if (trace_len == exec_res) {

    /* as a last check it is necessary to make sure new trace's loopback
       information is consistent with the original trace's loopbacks. */
    if (0 == res) {
      TraceIter partial_step;
      TraceIter complete_step;

      Trace_ptr complete_trace = \
        TraceManager_get_trace_at_index(TracePkg_get_global_trace_manager(),
                                        new_trace);
      int i = 1;

      partial_step = trace_first_iter(trace);
      complete_step = trace_first_iter(trace);
      if (opt_verbose_level_ge(OptsHandler_get_instance(), 4)) {
        fprintf(nusmv_stderr, "checking loopbacks...\n");
      }
      while (TRACE_END_ITER != partial_step) {
        if ((trace_step_is_loopback(trace, partial_step)) &&
            !trace_step_test_loopback(complete_trace, complete_step))  {

          fprintf(nusmv_stderr, "*** Error ***\n"
                  "Inconsistent loopback information found at step %d.\n", i);
          res = 1;
          break; /*  continuing is pointless */
        }

        ++ i;
        partial_step = trace_iter_get_next(partial_step);
        complete_step = trace_iter_get_next(complete_step);
      } /* trace foreach */
    } /* check loopbacks */

    if (0 == res) {
      /* print results */
      if (trace_is_registered(trace)) {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Trace %d execution completed succesfully"
                "(%d steps performed).\n", trace_get_id(trace), trace_len);
      }
      else {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Trace execution completed succesfully"
                "(%d steps performed).\n", trace_len);
      }

      fprintf(TraceExecInfo_output_stream(exec_info),
              "-- New complete trace is stored at %d index.\n",
              1 + new_trace);
    }
    else {
      if (trace_is_registered(trace)) {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Error in execution of trace %d: ",
                trace_get_id(trace));
      }
      else {
        fprintf(TraceExecInfo_output_stream(exec_info),
                "-- Error in execution of trace: ");
      }
    }
  } /* if (trace_len == exec_res) */
  else {
    res = 1; /* error */

    if (0 <= exec_res) {
      fprintf(TraceExecInfo_output_stream(exec_info),
              "step %d is not compatible with the model.\n",
              1 + exec_res);
    }
    else {
      fprintf(TraceExecInfo_output_stream(exec_info),
              "initial state is not compatible with the model.\n");
    }
  }

  return res;
}


/* this is shared accross all execution engines */
boolean
trace_exec_is_complete_state_assignment(const Trace_ptr trace,
                                        const TraceIter step)
{
  TraceSymbolsIter iter;
  node_ptr symb, val;

  TRACE_SYMBOLS_FOREACH(trace, TRACE_ITER_SF_VARS, iter, symb) {
    val = Trace_step_get_value(trace, step, symb);
    if (Nil == val) return false;
  }

  return true;
}

/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/
