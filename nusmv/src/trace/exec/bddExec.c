/**CFile***********************************************************************

  FileName    [bddExec.c]

  PackageName [trace.exec]

  Synopsis    [This module contains the function to support BDD basd
               trace re-execution]

  Description [optional]

  SeeAlso     [optional]

  Author      [Marco Pensallorto]

  Copyright   [
  This file is part of the ``trace.exec'' package of NuSMV version 2.
  Copyright (C) 2009 by FBK.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/
#include "mc/mc.h"
#include "mc/mcInt.h"

#include "traceExec.h"
#include "pkg_trace.h"
#include "pkg_traceInt.h"

static char rcsid[] UTIL_UNUSED = "$Id: bddExec.c,v 1.1.2.12 2010-02-12 16:25:47 nusmv Exp $";

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static Trace_ptr
extract_bdd_trace ARGS((BddFsm_ptr fsm, BddStates goal_states,
                        node_ptr fwd_image, int length, NodeList_ptr symbols,
                        const char* trace_name));

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Executes a trace on the given fsm using BDDs]

  Description        [The trace is executed using BDDs. Every transition of
                      the trace is tested for compatibility with the
                      model. Trace is assumed to be complete and
                      non-empty (i.e. at least one state exists). The
                      number of succesfully executed transitions is
                      returned.]

  SideEffects        [None]

  SeeAlso            []

******************************************************************************/
int trace_bdd_execute_trace(Trace_ptr trace,
                            TraceExecInfo_ptr exec_info)
{
  BddFsm_ptr bdd_fsm = BDD_FSM(NULL);
  BddEnc_ptr bdd_enc;
  DdManager* dd;
  BddStates trace_state;
  TraceIter step;
  int res = -1;

  /* 0- Check If NULL */
  TRACE_CHECK_INSTANCE(trace);
  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);

  /* fetch FSM and encoding from exec_info struct */
  nusmv_assert(EXEC_BDD == TraceExecInfo_engine(exec_info));
  bdd_fsm = (BddFsm_ptr)(TraceExecInfo_fsm(exec_info));

  BDD_FSM_CHECK_INSTANCE(bdd_fsm);
  bdd_enc = BddFsm_get_bdd_encoding(bdd_fsm);
  dd = BddEnc_get_dd_manager(bdd_enc);

  step = trace_first_iter(trace);
  nusmv_assert(TRACE_END_ITER != step);

  /* the set of initial states for the trace consists of just one
     state under the assumption that the trace is complete */
  trace_state = TraceUtils_fetch_as_bdd(trace, step,
                                        TRACE_ITER_SF_SYMBOLS, bdd_enc);

  /* 1- Check Start State */
  {
    bdd_ptr init_bdd = BddFsm_get_init(bdd_fsm);
    bdd_ptr invar_bdd = BddFsm_get_state_constraints(bdd_fsm);
    BddStates initial_states; /* the initial set of states for the model */
    BddStates from_state; /* last known state (just one, see above) */

    initial_states = bdd_and(dd, init_bdd, invar_bdd);
    bdd_free(dd, init_bdd);
    bdd_free(dd, invar_bdd);

    if (bdd_entailed(dd, trace_state, initial_states)) {
      boolean terminate = false;
      from_state = trace_state;
      ++ res;

      /* 2- Check Consecutive States are related by transition relation */
      do {
        BddStates forward_states;
        BddStates next_state;  /* (un-shifted) next state */

        BddInputs next_input; /* next input constraints */
        BddStatesInputsNexts next_combo; /* next combinatorials */

        step = TraceIter_get_next(step);
        if (TRACE_END_ITER != step) {

          /* fetch next bdds from trace */
          next_input = \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_I_SYMBOLS, bdd_enc);

          next_combo =  \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_COMBINATORIAL,
                                    bdd_enc);
          next_state = \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_SF_SYMBOLS,
                                    bdd_enc);

          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info),
                    "-- executing step %d ... ", 1+res);
            fflush(TraceExecInfo_output_stream(exec_info));
          }

          {
            BddStatesInputsNexts constraints = bdd_dup(next_input);
            bdd_and_accumulate(dd, &constraints, next_combo);

            forward_states =
              BddFsm_get_sins_constrained_forward_image(bdd_fsm, from_state,
                                                        constraints);
            bdd_free(dd, constraints);
          }

          /* test whether the constrained image entails the next states */
          if (bdd_entailed(dd, next_state, forward_states)) {
            if (TraceExecInfo_verbosity(exec_info)) {
              fprintf(TraceExecInfo_output_stream(exec_info), "done\n");
            }
            ++ res;
          }

          else {
            if (TraceExecInfo_verbosity(exec_info)) {
              fprintf(TraceExecInfo_output_stream(exec_info), "failed!\n");
            }
            terminate = true;
          }

          /* no longer used bdd refs */
          bdd_free(dd, forward_states);
          bdd_free(dd, next_combo);
          bdd_free(dd, next_input);

          bdd_free(dd, from_state);
          from_state = bdd_dup(next_state);
	  bdd_free(dd, next_state);
        }

        else {
          if (0 == res) {
            fprintf(nusmv_stderr,
                    "Warning: trace has no transitions.\n");
          }
          terminate = true;
        }
      } while (!terminate); /* loop on state/input pairs */

      bdd_free(dd, from_state);
    }

    else {
      fprintf(nusmv_stderr,
              "Error: starting state is not initial state.\n");
    }

    bdd_free(dd, initial_states);
  }
  return res;
}


/**Function********************************************************************

  Synopsis           [Executes a trace on the given fsm using BDDs]

  Description        [The trace is executed using BDDs, that is a proof
                      that the fsm is compatible with the trace is
                      built. Uncomplete traces are filled-in with
                      compatible values for state variables.

                      If trace is compatible, a new complete trace is
                      registered in the TraceManager and its index is written
                      into new_trace;

                      Given trace can be either complete or
                      incomplete.

                      The number of performed steps (transitions) is
                      returned. If the initial state is not compatible
                      -1 is returned.]

  SideEffects        [A complete trace will be registered into the Trace
                      Manager upon successful completion]

  SeeAlso            []

******************************************************************************/
int trace_bdd_execute_partial_trace(Trace_ptr trace,
                                    TraceExecInfo_ptr exec_info,
                                    int* trace_index)
{
  BddFsm_ptr bdd_fsm = BDD_FSM(NULL);

  int res = -1; /* failure */
  boolean success = true;

  BddEnc_ptr bdd_enc;
  DdManager* dd;
  BddStates trace_states;
  TraceIter step = TRACE_END_ITER;

  BddStates fwd_image;
  node_ptr path = Nil; /* forward constrained images will be used
                          later to compute the complete trace */

  const char* trace_description = "BDD Execution";
  Trace_ptr out_trace = TRACE(NULL);

  /* 0- Check If NULL */
  TRACE_CHECK_INSTANCE(trace);
  TRACE_EXEC_INFO_CHECK_INSTANCE(exec_info);
  nusmv_assert( (int*) NULL != trace_index );

  /* fetch FSM and encoding from exec_info struct */
  nusmv_assert(EXEC_BDD == TraceExecInfo_engine(exec_info));
  bdd_fsm = (BddFsm_ptr)(TraceExecInfo_fsm(exec_info));

  BDD_FSM_CHECK_INSTANCE(bdd_fsm);

  bdd_enc = BddFsm_get_bdd_encoding(bdd_fsm);
  dd = BddEnc_get_dd_manager(bdd_enc);

  step = trace_first_iter(trace);
  nusmv_assert(TRACE_END_ITER != step);

  trace_states = TraceUtils_fetch_as_bdd(trace, step,
                                         TRACE_ITER_SF_SYMBOLS, bdd_enc);

  /* 1- Check Start State */
  {
    bdd_ptr init_bdd = BddFsm_get_init(bdd_fsm);
    bdd_ptr invar_bdd = BddFsm_get_state_constraints(bdd_fsm);
    BddStates source_states; /* last known states */

    source_states = bdd_and(dd, init_bdd, invar_bdd);
    bdd_and_accumulate(dd, &source_states, trace_states);

    bdd_free(dd, invar_bdd);
    bdd_free(dd, init_bdd);
    bdd_free(dd, trace_states);

    if (!bdd_is_zero(dd, source_states)) {

      boolean terminate = false;
      path = cons(NODE_PTR(bdd_dup(source_states)), Nil);

      ++ res;

      /* 2- Check Consecutive States are related by transition relation */
      do {
        BddStates last_state; /* (unshifted) next state */
        BddStates next_state; /* next state constraints */

        BddInputs next_input; /* next input constraints */
        BddStatesInputsNexts next_combo; /* state-input-next constraints */

        BddStatesInputsNexts constraints;

        step = TraceIter_get_next(step);
        if (TRACE_END_ITER != step) {

          next_input = \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_I_SYMBOLS, bdd_enc);

          next_combo = \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_COMBINATORIAL,
                                    bdd_enc);
          last_state = \
            TraceUtils_fetch_as_bdd(trace, step, TRACE_ITER_SF_SYMBOLS,
                                    bdd_enc);

          next_state = BddEnc_state_var_to_next_state_var(bdd_enc, last_state);

          if (TraceExecInfo_verbosity(exec_info)) {
            fprintf(TraceExecInfo_output_stream(exec_info),
                    "-- executing step %d ... ", 1+res);
            fflush(TraceExecInfo_output_stream(exec_info));
          }

          /* building constrained fwd image */
          constraints = bdd_and(dd, next_input, next_combo);
          bdd_and_accumulate(dd, &constraints, next_state);

          fwd_image =
            BddFsm_get_sins_constrained_forward_image(bdd_fsm, source_states,
                                                      constraints);

           /* test whether the constrained fwd image is not empty */
          if (!bdd_is_zero(dd, fwd_image)) {
            if (TraceExecInfo_verbosity(exec_info)) {
              fprintf(TraceExecInfo_output_stream(exec_info), "done\n");
            }
            path = cons(NODE_PTR(fwd_image), path);
            ++ res;
          }
          else {
            if (TraceExecInfo_verbosity(exec_info)) {
              fprintf(TraceExecInfo_output_stream(exec_info), "failed!\n");
            }
            terminate = true;
            success = false;
          }

          /* no longer used bdd refs */
          bdd_free(dd, next_input);
          bdd_free(dd, next_combo);
          bdd_free(dd, last_state);
          bdd_free(dd, next_state);

          bdd_free(dd, source_states);
          source_states = bdd_dup(fwd_image);
        }

        else {
          if (0 == res) {
            fprintf(nusmv_stderr,
		    "Warning: trace has no transitions.\n");
          }
          terminate = true;
        }
      } while (!terminate); /* loop on state/input pairs */
    } /* if has initial state */

    else {
      fprintf(nusmv_stderr,
              "Error: starting state is not initial state.\n");
      success = false;
    }

    /* 3- If last state could be reached a complete trace exists */
    if (success) {
      fprintf(TraceExecInfo_output_stream(exec_info),
              "Trace was succesfully completed.\n");

      if (0<res) {
        out_trace = extract_bdd_trace(bdd_fsm, fwd_image, path, res,
                                      TraceExecInfo_language(exec_info),
                                      trace_description);
      }

      else { /* extracts a complete state of trace of length 0 */
        out_trace = extract_bdd_trace(bdd_fsm, source_states, Nil, res,
                                      TraceExecInfo_language(exec_info),
                                      trace_description);
      }


      nusmv_assert(TRACE(NULL) != out_trace);
      if (opt_verbose_level_ge(OptsHandler_get_instance(), 2)) {
        fprintf(nusmv_stderr, "Registering complete trace");
      }

      *trace_index =
        TraceManager_register_trace(TracePkg_get_global_trace_manager(),
                                    out_trace);

      /* cleanup */
      walk_dd(dd, bdd_free, path);
      free_list(path);
    }

    bdd_free(dd, source_states);
  }

  return res;
}


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************


  Synopsis           [Extracts a complete trace

  Description        [This function is a private service of
                      trace_bdd_execute_partial_trace]

  SideEffects        [None]

******************************************************************************/
Trace_ptr extract_bdd_trace(BddFsm_ptr fsm, BddStates goal_states,
                            node_ptr reachable, int length,
                            NodeList_ptr symbols,
                            const char* trace_name)
{
  Trace_ptr res;
  BddEnc_ptr enc;
  DdManager* dd;
  node_ptr path;
  bdd_ptr target;

  NODE_LIST_CHECK_INSTANCE(symbols);

  enc = BddFsm_get_bdd_encoding(fsm);
  dd = BddEnc_get_dd_manager(enc);

  target = BddEnc_pick_one_state_rand(enc, goal_states);
  path = cons((node_ptr) target, Nil);

  if (Nil != reachable) {
    reachable = cdr(reachable);
    while (0 != length) {
      bdd_ptr source;
      bdd_ptr input;
      bdd_ptr inputs;
      bdd_ptr bwd_image;
      bdd_ptr intersect;

      /* pick source state */
      bwd_image = BddFsm_get_backward_image(fsm, target);
      intersect = bdd_and(dd, bwd_image, (bdd_ptr) car(reachable));
      nusmv_assert(!bdd_is_zero(dd, intersect));
      source = BddEnc_pick_one_state(enc, intersect);
      bdd_free(dd, intersect);
      bdd_free(dd, bwd_image);

      /* pick one input s.t. source -> (input) -> target */
      inputs = BddFsm_states_to_states_get_inputs(fsm, source, target);
      input = BddEnc_pick_one_input(enc, inputs);
      nusmv_assert(!bdd_is_zero(dd, input));
      bdd_free(dd, inputs);

      /* prepend input and source state */
      path = cons((node_ptr) input, path);
      path = cons((node_ptr) source, path);

      -- length;
      target = source;
      reachable = cdr(reachable);
    }
  }
  /* make sure the trace length is correct */
  nusmv_assert(0 == length &&  Nil == reachable);

  res = Mc_create_trace_from_bdd_state_input_list(enc, symbols, trace_name,
                                                  TRACE_TYPE_EXECUTION, path);
  /* cleanup */
  walk_dd(dd, bdd_free, path);
  free_list(path);

  return res;
}
