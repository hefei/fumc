/**CFile*****************************************************************

  FileName    [SymbCache.c]

  PackageName [compile.symb_table]

  Synopsis    [The SymbCache class implementation]

  Description []

  SeeAlso     [SymbCache.h]

  Author      [Roberto Cavada]

  Copyright   [
  This file is part of the ``compile.symb_table'' package of NuSMV
  version 2.  Copyright (C) 2004 by FBK-irst.

  NuSMV version 2 is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Lesser General Public 
  License as published by the Free Software Foundation; either 
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with this library; if not, write to the Free Software 
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/

#include "symb_table_int.h"

#include "SymbCache.h"
#include "SymbCache_private.h"

#include "compile/compile.h"
#include "parser/symbols.h"
#include "utils/assoc.h"
#include "utils/error.h"
#include "compile/symb_table/NFunction.h"


static char rcsid[] UTIL_UNUSED = "$Id: SymbCache.c,v 1.1.2.19.6.12 2010/02/08 12:25:27 nusmv Exp $";


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/**Struct**********************************************************************

  Synopsis    [The SymbCache class]

  Description []
  
******************************************************************************/
typedef struct SymbCache_TAG
{
  /* Uses the symbol table to flat DEFINEs bodies */
  SymbTable_ptr symb_table;

  /* Cache for symbols names. Contains node of these kinds:
     for variables    VAR, FROZENVAR, IVAR: available slot, type
     for DEFINEs      CONTEXT: ctx, definition body 
     for constants:   NUMBER: num of instances, Nil
  */
  hash_ptr symbol_hash;

  /* hash for the memoization of flatten define body */
  hash_ptr define_flatten_body_hash;

  /* lists */
  NodeList_ptr constants;
  NodeList_ptr defines;
  NodeList_ptr parameters;
  NodeList_ptr mdefines;
  NodeList_ptr symbol_types;

  NodeList_ptr state_vars;
  NodeList_ptr frozen_vars;
  NodeList_ptr state_frozen_vars; /* a concatenation of state_vars and frozen_vars */
  NodeList_ptr input_vars;
  NodeList_ptr vars; /* all variables */

  /* Symbols (variables and DEFINEs) */
  NodeList_ptr sf_symbols; /* state and frozen symbol */
  NodeList_ptr i_symbols;  /* input symbols */
  NodeList_ptr sf_i_symbols; /* (state OR frozen) AND input symbols */

  /* DEFINEs whose body was not fully declared when the DEFINE was
     declared. These will be later processed when the set of symbols is
     required. */
  NodeList_ptr pending_defines; 

  NodeList_ptr functions; 

  /* Several operation with the same key are performed on the symbol
     hash.  To improve performance, the last operated key and the
     associated value are stored separately here. */
  node_ptr last_symbol_hash_key; 
  node_ptr last_symbol_hash_value; 
   
} SymbCache;


/*---------------------------------------------------------------------------*/
/* macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static void symb_cache_init ARGS((SymbCache_ptr self, 
                                  SymbTable_ptr symb_table));

static void symb_cache_deinit ARGS((SymbCache_ptr self));

static void 
symb_cache_new_symbol ARGS((SymbCache_ptr self, node_ptr name, 
                            node_ptr value));

static node_ptr symb_cache_lookup_symbol ARGS((const SymbCache_ptr self, 
                                               const node_ptr name));

static void
symb_cache_define_to_symbols_lists ARGS((SymbCache_ptr self, node_ptr define));

static void symb_cache_resolve_pending_defines ARGS((SymbCache_ptr self));

static void 
symb_cache_remove_symbol ARGS((SymbCache_ptr self, node_ptr name));

static node_ptr 
_resolve_define_chain ARGS((const SymbCache_ptr self,
                            const node_ptr expr));

static boolean _resolve_define_chain_is_constant ARGS((SymbCache_ptr self,
                                                       node_ptr exp));

/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis           [Class constructor]

  Description        [Callable only by the SymbTable instance that owns self.
  The caller keeps the ownership of given SymbTable instance]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
SymbCache_ptr SymbCache_create(SymbTable_ptr symb_table)
{
  SymbCache_ptr self = ALLOC(SymbCache, 1);

  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_init(self, symb_table); 
  return self;  
}


/**Function********************************************************************

  Synopsis           [Class destructor]

  Description        [Callable only by the SymbTable instance that owns self.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_destroy(SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_deinit(self);
  FREE(self);  
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared variables]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_vars(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->vars;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared state variables
  (this does not include the frozen variables) ]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_state_vars(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->state_vars;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared frozen variables]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_frozen_vars(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->frozen_vars;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared frozen and state variables]

  Description        [Returned instance belongs to self, do not destroy or 
  change it.
  This list is just a concatenation of SymbCache_get_frozen_vars(self)
  and SymbCache_get_state_vars(self).
  ]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_state_frozen_vars(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->state_frozen_vars;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared input variables]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_input_vars(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->input_vars;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all declared constants]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_constants(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->constants;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all DEFINEs]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_defines(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->defines;
}

/**Function********************************************************************

  Synopsis           [Returns the list of all NFunctions]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_functions(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->functions;
}

/**Function********************************************************************

  Synopsis           [Returns the list of all formal parameters]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_parameters(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->parameters;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all MDEFINEs]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_matrix_defines(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->mdefines;
}


/**Function********************************************************************

  Synopsis           [Returns the list of all ARRAY vars]

  Description        [Returned instance belongs to self, do not destroy or 
  change it]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr SymbCache_get_symbol_types(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  return self->symbol_types;
}


/**Function********************************************************************

  Synopsis [Returns the list of all state and frozen variables and those DEFINEs
  whose body refer directly or indirectly to that variables]

  Description [Only state and frozen variables and DEFINEs can occur within the
  returned list. Returned list belongs to self, do not destroy it]

  SideEffects        []

  See Also           []

******************************************************************************/
NodeList_ptr SymbCache_get_sf_symbols(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_resolve_pending_defines(self);
  return self->sf_symbols;
}


/**Function********************************************************************

  Synopsis [Returns the list of all input variables and those DEFINEs
  whose body refer directly or indirectly to input variables]

  Description [Only input variables and DEFINEs can occur within the
  returned list. Returned list belongs to self, do not destroy it]

  SideEffects        []

  See Also           []

******************************************************************************/
NodeList_ptr SymbCache_get_i_symbols(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_resolve_pending_defines(self);
  return self->i_symbols;
}


/**Function********************************************************************

  Synopsis [Returns the list of those DEFINEs whose body refer
  directly or indirectly to both state(OR frozen) AND input variables]

  Description [Only DEFINEs whose body contains both state(or frozen) and input
  vars can occur within the returned list. Returned list belongs to
  self, do not destroy it]

  SideEffects        []

  See Also           []

******************************************************************************/
NodeList_ptr SymbCache_get_sf_i_symbols(const SymbCache_ptr self)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_resolve_pending_defines(self);
  return self->sf_i_symbols;
}


/**Function********************************************************************

  Synopsis           [Declares a new input variable.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_input_var(SymbCache_ptr self, node_ptr var, 
                             SymbType_ptr type)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, var, new_node(IVAR, Nil, (node_ptr) type));
}


/**Function********************************************************************

  Synopsis           [Declares a new state variable.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_state_var(SymbCache_ptr self, node_ptr var, 
                             SymbType_ptr type)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, var, new_node(VAR, Nil, (node_ptr) type));
}


/**Function********************************************************************

  Synopsis           [Declares a new frozen variable.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            [SymbCache_redeclare_state_as_frozen_var]

******************************************************************************/
void SymbCache_new_frozen_var(SymbCache_ptr self, node_ptr var, 
                               SymbType_ptr type)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, var, new_node(FROZENVAR, Nil, (node_ptr) type));
}


/**Function********************************************************************

  Synopsis           [Redeclare a state variable as a frozen variable]

  Description [A variable is frozen if it is known that its value cannot
  be changed during transitions.
  The given 'name' has to be already declared state variable and not yet
  redeclared as frozen.]
  
  SideEffects        []

  SeeAlso            [SymbCache_new_frozen_var]

******************************************************************************/
void SymbCache_redeclare_state_as_frozen_var(SymbCache_ptr self, node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  node_ptr val = symb_cache_lookup_symbol(self, name);
  /* var must be state, previously declared and not set up as frozen */
  nusmv_assert(val && node_get_type(val) == VAR);
  
  node_set_type(val, FROZENVAR);

  /* move a var from state var list to frozen var list */
  ListIter_ptr iter;
  for (iter = NodeList_get_first_iter(self->state_vars); 
       iter == ListIter_get_next(iter); ListIter_is_end(iter)) {
    if (NodeList_get_elem_at(self->state_vars, iter) == name) {
      NodeList_remove_elem_at(self->state_vars, iter);
      NodeList_append(self->frozen_vars, name);
      return;
    };
  }
  nusmv_assert(false); /* the variable has to be in the state var list */
}


/**Function********************************************************************

  Synopsis           [Removes a variable from the cache of symbols, and from 
  the flattener module]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_remove_var(SymbCache_ptr self, node_ptr var)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_var(self, var));

  symb_cache_remove_symbol(self, var);
}


/**Function********************************************************************

  Synopsis           [Declares a new DEFINE.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_define(SymbCache_ptr self, node_ptr name, 
                          node_ptr ctx, node_ptr definition)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, name, new_node(CONTEXT, ctx, definition));
}


/**Function********************************************************************

  Synopsis           [Declares a new module parameter.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_parameter(SymbCache_ptr self, node_ptr formal, 
                             node_ptr ctx, node_ptr actual)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, formal, new_node(DOT, ctx, actual));
}


/**Function********************************************************************

  Synopsis           [Declares a new MDEFINE.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.
  Internally we use ARRAY_DEF node to recognize a MDEFINE.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_matrix_define(SymbCache_ptr self, node_ptr name, 
                                 node_ptr ctx, node_ptr definition)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  /* Store the given simbol using ARRAY_DEF node type to remember that
     it is a MDEFINE. The car will be the context while the cdr will
     be the definition */
  symb_cache_new_symbol(self, name, new_node(ARRAY_DEF, ctx, definition));
}


/**Function********************************************************************

  Synopsis    [Declares a new ARRAY var.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_symbol_type(SymbCache_ptr self, node_ptr name, 
                               SymbType_ptr type)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, name, new_node(ARRAY, Nil, NODE_PTR(type)));
}


/**Function********************************************************************

  Synopsis           [Removes a DEFINE from the cache of symbols, and from 
  the flattener define hash]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_remove_define(SymbCache_ptr self, node_ptr define)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_define(self, define));

  symb_cache_remove_symbol(self, define);
}

/**Function********************************************************************

  Synopsis           [Removes an NFunction from the cache of symbols]

  Description        [This (private) method can be used only by 
                      SymbLayer, otherwise the resulting status 
                      will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_remove_function(SymbCache_ptr self, node_ptr fun)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_function(self, fun));

  symb_cache_remove_symbol(self, fun);
}

/**Function********************************************************************

  Synopsis           [Removes a parameter from the cache of symbols]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_remove_parameter(SymbCache_ptr self, node_ptr formal)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_parameter(self, formal));

  symb_cache_remove_symbol(self, formal);
}

/**Function********************************************************************

  Synopsis           [Declares a new constant.]

  Description [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted. Multiple-time 
  declared constant are accepted, and a reference count is kept to deal with 
  them]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_constant(SymbCache_ptr self, node_ptr name)
{
  node_ptr c;
  SYMB_CACHE_CHECK_INSTANCE(self);
  
  c = symb_cache_lookup_symbol(self, name); 
  
  if (c == Nil) {
    symb_cache_new_symbol(self, name, find_node(NUMBER, NODE_FROM_INT(1), Nil));
  }
  else {
    int count; 
    nusmv_assert(node_get_type(c) == NUMBER);

    count = NODE_TO_INT(car(c));
    free_node(c);
    symb_cache_new_symbol(self, name, 
                          find_node(NUMBER, NODE_FROM_INT(count+1), Nil));
  }
}

/**Function********************************************************************

  Synopsis           [Declares a new NFunction.]

  Description        [This (private) method can be used only by SymbLayer,
  otherwise the resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_new_function(SymbCache_ptr self, node_ptr name,
                              node_ptr ctx, NFunction_ptr fun)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb_cache_new_symbol(self, name,
                        new_node(NFUNCTION, ctx, NODE_PTR(fun)));
}


/**Function********************************************************************

  Synopsis           [Returns the NFunction instance of the given
                      function name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NFunction_ptr SymbCache_get_function(SymbCache_ptr self, node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  return (NFunction_ptr)cdr(symb_cache_lookup_symbol(self, name));
}

/**Function********************************************************************

  Synopsis           [Returns the context of the given NFunction]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_function_context(SymbCache_ptr self, node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  return car(symb_cache_lookup_symbol(self, name));
}

/**Function********************************************************************

  Synopsis [Removes a constant from the cache of symbols, and from 
  the flattener module]

  Description [Removal is performed taking into account of reference
  counting, as constants can be shared among several layers. This
  (private) method can be used only by SymbLayer, otherwise the
  resulting status will be corrupted.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void SymbCache_remove_constant(SymbCache_ptr self, node_ptr constant)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_constant(self, constant));

  symb_cache_remove_symbol(self, constant);
}


/**Function********************************************************************

  Synopsis           [Returns the type of a given variable]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
SymbType_ptr SymbCache_get_var_type(const SymbCache_ptr self, 
                                    const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_var(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);

  return SYMB_TYPE(cdr(symb));
}


/**Function********************************************************************

  Synopsis           [Returns the body of the given DEFINE name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_define_body(const SymbCache_ptr self, 
                                   const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_define(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return cdr(symb);  
}

/**Function********************************************************************

  Synopsis           [Returns the actual param of the given formal parameter]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_actual_parameter(const SymbCache_ptr self, 
                                        const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_parameter(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return cdr(symb);  
}


/**Function********************************************************************

  Synopsis           [Returns the body of the given MDEFINE name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_matrix_define_body(const SymbCache_ptr self, 
                                          const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_matrix_define(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return cdr(symb);
}


/**Function********************************************************************

  Synopsis           [Returns the type of array variable, i.e. of symbol_type]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
SymbType_ptr SymbCache_get_symbol_type_type(const SymbCache_ptr self, 
                                            const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_symbol_type(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return SYMB_TYPE(cdr(symb));
}


/**Function********************************************************************

  Synopsis           [Returns the flattenized body of the given DEFINE name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr 
SymbCache_get_define_flatten_body(const SymbCache_ptr self,
                                  const node_ptr name)
{
  node_ptr res;

  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_define(self, name));   

  res = find_assoc(self->define_flatten_body_hash, name);
  if (res != (node_ptr) NULL) return res;

  set_definition_mode_to_expand();
  res = Flatten_GetDefinition(self->symb_table, name); 
  set_definition_mode_to_get();

  insert_assoc(self->define_flatten_body_hash, name, res);
  return res;
}


/**Function********************************************************************

  Synopsis [Returns the flattenized actual parameter of the given
  formal parameter]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr 
SymbCache_get_flatten_actual_parameter(const SymbCache_ptr self,
                                       const node_ptr name)
{
  node_ptr res;
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_parameter(self, name));   

  res = symb_cache_lookup_symbol(self, name);

  return find_node(CONTEXT, car(res), cdr(res));
}

/**Function********************************************************************

  Synopsis           [Returns the context of the given DEFINE name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_define_context(const SymbCache_ptr self, 
                                      const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_define(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return car(symb);  
}

/**Function********************************************************************

  Synopsis [Returns the context of the actual parameter associated
  with the given formal parameter ]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_actual_parameter_context(const SymbCache_ptr self, 
                                                const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_parameter(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return car(symb);  
}

/**Function********************************************************************

  Synopsis           [Returns the context of the given MDEFINE name]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr SymbCache_get_matrix_define_context(const SymbCache_ptr self, 
                                             const node_ptr name)
{
  node_ptr symb; 
 
  SYMB_CACHE_CHECK_INSTANCE(self);
  nusmv_assert(SymbCache_is_symbol_matrix_define(self, name)); 
  
  symb = symb_cache_lookup_symbol(self, name);
  return car(symb);
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is a state variable.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_state_var(const SymbCache_ptr self, 
                                      const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == VAR); 
}


/**Function********************************************************************

  Synopsis           [Returns true if the variable is frozen]

  Description        [A variable is frozen if it is known that the var cannot
  change its value during transitions.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_frozen_var(const SymbCache_ptr self,
                                       const node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  node_ptr symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == FROZENVAR); 
}


/**Function********************************************************************

  Synopsis           [Returns true if the variable is a frozen or a state
  variable]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_state_frozen_var(const SymbCache_ptr self,
                                             const node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);
  node_ptr symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && 
    (node_get_type(symb) == FROZENVAR || node_get_type(symb) == VAR); 
}


/**Function********************************************************************

  Synopsis [Returns true if the given symbol is an input
  variable.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_input_var(const SymbCache_ptr self, 
                                      const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == IVAR); 
}


/**Function********************************************************************

  Synopsis [Returns true if the given symbol is either a state, a frozen or
  an input variable.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_var(const SymbCache_ptr self, const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && ((node_get_type(symb) == VAR) 
                                       || (node_get_type(symb) == FROZENVAR)
                                       || (node_get_type(symb) == IVAR));
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is declared]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_declared(const SymbCache_ptr self, 
                                     const node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  return (symb_cache_lookup_symbol(self, name) != (node_ptr) NULL) || 
    SymbCache_is_symbol_constant(self, name);
}


/**Function********************************************************************

  Synopsis [Returns true if the given symbol is a declared
  constant]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_constant(const SymbCache_ptr self, 
                                     const node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  return NodeList_belongs_to(self->constants, name);
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is a declared
  DEFINE]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_define(const SymbCache_ptr self, 
                                   const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == CONTEXT);
}

/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is a declared
                      NFunction]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_function(const SymbCache_ptr self, 
                                       const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == NFUNCTION);
}

/**Function********************************************************************

  Synopsis [Returns true if the given symbol is a declared formal
  parameter]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_parameter(const SymbCache_ptr self, 
                                      const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == DOT);
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is a declared
  MDEFINE]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_matrix_define(const SymbCache_ptr self, 
                                          const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == ARRAY_DEF);
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbol is a declared
  array variable, i.e symbol type]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean SymbCache_is_symbol_symbol_type(const SymbCache_ptr self, 
                                        const node_ptr name)
{
  node_ptr symb;
  SYMB_CACHE_CHECK_INSTANCE(self);

  symb = symb_cache_lookup_symbol(self, name);
  return (symb != (node_ptr) NULL) && (node_get_type(symb) == ARRAY);
}


/**Function********************************************************************

  Synopsis [Returns true if var_list contains at least one input
  variable]

  Description        [The given list of variables is traversed until an input 
  variable is found]

  SideEffects        []

  See Also           []

******************************************************************************/
boolean SymbCache_list_contains_input_var(const SymbCache_ptr self,
                                          const NodeList_ptr var_list)
{
  ListIter_ptr iter;  

  SYMB_CACHE_CHECK_INSTANCE(self);

  iter = NodeList_get_first_iter(var_list);
  while (! ListIter_is_end(iter)) {
    if (SymbCache_is_symbol_input_var(self, 
                      NodeList_get_elem_at(var_list, iter))) return true;

    iter = ListIter_get_next(iter);
  }

  return false;
}


/**Function********************************************************************

  Synopsis           [Returns true if var_list contains at least one state 
  or frozen variable]

  Description        [The given list of variables is traversed until 
  a state or frozen variable is found]

  SideEffects        []

  See Also           []

******************************************************************************/
boolean SymbCache_list_contains_state_frozen_var(const SymbCache_ptr self,
                                                 const NodeList_ptr var_list)
{
  ListIter_ptr iter;  

  SYMB_CACHE_CHECK_INSTANCE(self);

  iter = NodeList_get_first_iter(var_list);
  while (! ListIter_is_end(iter)) {
    if (SymbCache_is_symbol_state_frozen_var(self, 
                NodeList_get_elem_at(var_list, iter))) return true;

    iter = ListIter_get_next(iter);
  }

  return false;
}


/**Function********************************************************************

  Synopsis           [Returns true if the given symbols list contains 
  one or more undeclared variable names]

  Description        [Iterates through the elements in var_list
  checking each one to see if it is one undeclared variable.]

  SideEffects        []

  See Also           []

******************************************************************************/
boolean SymbCache_list_contains_undef_var(const SymbCache_ptr self,
                                          const NodeList_ptr var_list)
{
  ListIter_ptr iter;  

  SYMB_CACHE_CHECK_INSTANCE(self);

  iter = NodeList_get_first_iter(var_list);
  while (! ListIter_is_end(iter)) {
    if (!SymbCache_is_symbol_var(self, 
                 NodeList_get_elem_at(var_list, iter))) return true;

    iter = ListIter_get_next(iter);
  }

  return false;
}


/**Function********************************************************************

  Synopsis           [Returns the last non atomic element of a chain
  of defines]

  Description        [Give a chain of defines, it returns the last no
  atomic element of the chain. E.g. DEFINE a := b; b := c; c := expr;
  then in this case the call on ]

  SideEffects        []

  See Also           []

******************************************************************************/
node_ptr SymbCache_resolve_define_chain(const SymbCache_ptr self,
                                        const node_ptr define) {
  node_ptr result;
  nusmv_assert(SymbCache_is_symbol_define(self, define));

  result = _resolve_define_chain(self, define);

  /* It is not atomic, then simply return the define itself */
  return result;
}


/*--------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis      [Insert a new value in the symbol hash]

  Description   [This takes into account also the caching of last accessed 
  value]
  
  SideEffects        []

******************************************************************************/
static void 
symb_cache_new_symbol(SymbCache_ptr self, node_ptr name, node_ptr value)
{  
  switch (node_get_type(value)) {
  case IVAR: /* Input var */
    /* not previously declared */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->i_symbols, name);
    NodeList_append(self->input_vars, name);
    NodeList_append(self->vars, name);
    break;

  case VAR: /* State var */
    /* not previously declared */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->sf_symbols, name);
    NodeList_append(self->state_vars, name);
    NodeList_append(self->state_frozen_vars, name);
    NodeList_append(self->vars, name);
    break;

  case FROZENVAR: /* Frozen  var */
    /* not previously declared */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->sf_symbols, name);
    NodeList_append(self->frozen_vars, name);
    NodeList_append(self->state_frozen_vars, name);
    NodeList_append(self->vars, name);
    break;

  case CONTEXT:
    /* This is a DEFINE, add it to the list of pending DEFINEs: */
    /* not previously declared */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);
    nusmv_assert(!NodeList_belongs_to(self->pending_defines, name));

    NodeList_append(self->pending_defines, name);
    NodeList_append(self->defines, name);
    break;

  case DOT: 
    /* This is a parameter, add it to the list of parameters: */
    /* not previously declared */
    nusmv_assert((node_ptr) NULL == symb_cache_lookup_symbol(self, name));
    NodeList_append(self->parameters, name);
    break;

  case ARRAY_DEF:
    /* This is a MDEFINE, add it to the list of pending MDEFINEs: */
    /* not previously declared */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->mdefines, name);
    break;

  case ARRAY:
    /* This is an array var, i.e. symbol_type  */
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->symbol_types, name);
    break;
    
  case NFUNCTION:
    nusmv_assert(symb_cache_lookup_symbol(self, name) == (node_ptr) NULL);

    NodeList_append(self->functions, name);
    break;

  case NUMBER:
    /* This is a constant: add it to the list of constants, but only
       if the ref counter is one and it is not already defined
       (i.e. it is a new constant, and not a ref counting
       decreasing) */
    nusmv_assert(NODE_TO_INT(car(value)) > 0);
    if ( (NODE_TO_INT(car(value)) == 1) && 
         !NodeList_belongs_to(self->constants, name) ) {
      NodeList_append(self->constants, name);
    }
    break;

  default:
    internal_error("symb_cache_new_symbol: unknown symbol type");
  }      

  insert_assoc(self->symbol_hash, name, value);
  self->last_symbol_hash_key = name;
  self->last_symbol_hash_value = value;
}


/**Function********************************************************************

  Synopsis           [Removes any reference of the given symbol from the 
  cache, and from the flattener internal hashes as well]

  Description        [A service of the remove_* private methods]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void symb_cache_remove_symbol(SymbCache_ptr self, node_ptr name)
{
  NodeList_ptr* iter;
  NodeList_ptr lists[] = { 
    self->defines,
    self->parameters,
    self->mdefines,
    self->symbol_types,
    self->state_vars,
    self->frozen_vars,
    self->state_frozen_vars,
    self->input_vars,
    self->vars,
    self->sf_symbols,
    self->i_symbols,
    self->sf_i_symbols,
    self->pending_defines,
    self->functions,
    NULL /* terminator */
  };    
  
  /* check if it is a define, to cleanup the flatten_body hash */
  if (SymbCache_is_symbol_define(self, name)) {
    insert_assoc(self->define_flatten_body_hash, name, (node_ptr) NULL);
  }

  /* fixes the lists: */
  for (iter = lists; *iter != NODE_LIST(NULL); ++iter) {
    if (NodeList_belongs_to(*iter, name)) {
      /* finds the occurence and removes it */
      ListIter_ptr iter2 = NodeList_get_first_iter(*iter);
      while (!ListIter_is_end(iter2)) {
        if (NodeList_get_elem_at(*iter, iter2) == name) {
          /* found it: removes the occurence: */
          NodeList_remove_elem_at(*iter, iter2);
          break;
        }
        iter2 = ListIter_get_next(iter2);
      }
    }
  }      


  {  /* fixes the hash table, and constants list */
    node_ptr val = find_assoc(self->symbol_hash, name);
    if (val != Nil) {
      if (node_get_type(val) == NUMBER) {
        /* it is a constant, removes it only if the reference counting
           goes to zero */
        int count = NODE_TO_INT(car(val));
        nusmv_assert(count > 0);

        if (count > 1) {
          /* decreases the constant reference counting */
          symb_cache_new_symbol(self, name, 
                                find_node(NUMBER, NODE_FROM_INT(count-1), Nil));
        }
        else { /* constant reaches a zero reference counting: removes it */
          ListIter_ptr iter;

          /* cleans up the symbols hash */
          insert_assoc(self->symbol_hash, name, Nil); 

          /* removes the constant from the constants list as well */
          iter = NodeList_get_first_iter(self->constants);
          while (!ListIter_is_end(iter)) {
            if (NodeList_get_elem_at(self->constants, iter) == name) {
              /* found it: removes the constant */
              NodeList_remove_elem_at(self->constants, iter);
              break;
            }
            iter = ListIter_get_next(iter);
          }
          
          /* cleans up the flattener hashes */
          Flatten_remove_symbol_info(name);
        }
      }
      else {
        insert_assoc(self->symbol_hash, name, Nil); /* not a constant */
        Flatten_remove_symbol_info(name); /* cleans up the flattener hashes */
      }

      free_node(val);
    }
    
    /* cleans up structures for lazy evaluation if it is the case */
    if (self->last_symbol_hash_key == name) {
      self->last_symbol_hash_key = Nil;
      self->last_symbol_hash_value = Nil;
    }
  } /* end of block */
}


/**Function********************************************************************

  Synopsis           [Returns the definition of the given symbol]

  Description        [Returned node can be either VAR, FROZENVAR, IVAR or CONTEXT.
  Value is searched first in the cache, then in the symbol hash]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static node_ptr symb_cache_lookup_symbol(const SymbCache_ptr self, 
                                         const node_ptr name)
{
  SYMB_CACHE_CHECK_INSTANCE(self);

  if (self->last_symbol_hash_key != name) {
    self->last_symbol_hash_value = find_assoc(self->symbol_hash, name);
    self->last_symbol_hash_key = name;
  }
  
  return self->last_symbol_hash_value;
}


/**Function********************************************************************

  Synopsis           [Private initializer]

  Description        [Private initializer, called by the constructor]

  SideEffects        []

  SeeAlso            [symb_cache_deinit]

******************************************************************************/
static void symb_cache_init(SymbCache_ptr self, SymbTable_ptr symb_table)
{
  self->symb_table = symb_table;
  self->symbol_hash = new_assoc();
  nusmv_assert(self->symbol_hash != (hash_ptr) NULL);

  self->define_flatten_body_hash = new_assoc();
  nusmv_assert(self->define_flatten_body_hash != (hash_ptr) NULL);                                     

  self->constants = NodeList_create();
  self->defines = NodeList_create();
  self->parameters = NodeList_create();
  self->functions = NodeList_create();
  self->mdefines = NodeList_create();
  self->symbol_types = NodeList_create();
  self->input_vars = NodeList_create();
  self->state_vars = NodeList_create();
  self->frozen_vars = NodeList_create();
  self->state_frozen_vars = NodeList_create();
  self->vars = NodeList_create();

  self->pending_defines = NodeList_create();
  self->sf_symbols = NodeList_create();
  self->i_symbols = NodeList_create();
  self->sf_i_symbols = NodeList_create();

  self->last_symbol_hash_key = (node_ptr) NULL;
  self->last_symbol_hash_value = (node_ptr) NULL;
}


/**Function********************************************************************

  Synopsis           [Private deinitializer]

  Description        [Private deinitializer, called by the destructor]

  SideEffects        []

  SeeAlso            [symb_cache_init]

******************************************************************************/
static void symb_cache_deinit(SymbCache_ptr self)
{
  free_assoc(self->symbol_hash);  
  free_assoc(self->define_flatten_body_hash);

  NodeList_destroy(self->vars);
  NodeList_destroy(self->state_vars);
  NodeList_destroy(self->frozen_vars);
  NodeList_destroy(self->state_frozen_vars);
  NodeList_destroy(self->input_vars);
  NodeList_destroy(self->defines);
  NodeList_destroy(self->functions);
  NodeList_destroy(self->parameters);
  NodeList_destroy(self->mdefines);
  NodeList_destroy(self->symbol_types);
  NodeList_destroy(self->constants);

  NodeList_destroy(self->sf_symbols);
  NodeList_destroy(self->i_symbols);
  NodeList_destroy(self->sf_i_symbols);
  NodeList_destroy(self->pending_defines);
}


/**Function********************************************************************

  Synopsis           [Used to update symbols lists when a DEFINE is declared. 
  The given DEFINE will be added to one of state, input or state-input symbols 
  lists.]

  Description [This method must be called after all symbols occuring
  directly or indirectly within its body are all declared. If one or
  more are undeclared, an internal error occurs.] 

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void  
symb_cache_define_to_symbols_lists(SymbCache_ptr self, node_ptr define)
{
  Set_t deps;
  node_ptr body;
  boolean state, input, constant; 

  /* check that it does not occur to the list of pending DEFINEs: */
  nusmv_assert(!NodeList_belongs_to(self->pending_defines, define));

  /* checks if already added to symbols lists: */
  if (NodeList_belongs_to(self->sf_symbols, define) || 
      NodeList_belongs_to(self->i_symbols, define) ||
      NodeList_belongs_to(self->sf_i_symbols, define)) return;

  body = SymbCache_get_define_flatten_body(self, define);
  deps = Formula_GetDependencies(self->symb_table, body, Nil);
  
  state = SymbCache_list_contains_state_frozen_var(self, Set_Set2List(deps));
  input = SymbCache_list_contains_input_var(self, Set_Set2List(deps));
  constant = (!(state || input)) && (body != (node_ptr) NULL);
  nusmv_assert(state || input || constant);

  /* If state and input vars occur, state_input list is updated */
  if (state && input)  NodeList_append(self->sf_i_symbols, define);
  else if (state) NodeList_append(self->sf_symbols, define);
  else if (input) NodeList_append(self->i_symbols, define);
  /* constants are managed within state symbols for convenience */
  else if (constant) NodeList_append(self->sf_symbols, define);

  Set_ReleaseSet(deps);
}


/**Function********************************************************************

  Synopsis           [If there are pending DEFINEs that wait for an assignment 
  within the lists of state, input and state-input symbols list, they are 
  resolved and assigned.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void symb_cache_resolve_pending_defines(SymbCache_ptr self)
{
  ListIter_ptr iter;
  
  iter = NodeList_get_first_iter(self->pending_defines);
  while (! ListIter_is_end(iter)) {
    node_ptr define;

    define = NodeList_remove_elem_at(self->pending_defines, iter);
    symb_cache_define_to_symbols_lists(self, define);
    
    iter = NodeList_get_first_iter(self->pending_defines);
  }
}


/**Function********************************************************************

  Synopsis           [Recursively get rid of chain of defines.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static node_ptr _resolve_define_chain(const SymbCache_ptr self,
                                      const node_ptr expr) {
  nusmv_assert(Nil != expr);
  if (_resolve_define_chain_is_constant(self, expr)) {
    return expr;
  }
  else if (SymbCache_is_symbol_var(self, expr)) {
    return expr;
  }
  else if (SymbCache_is_symbol_matrix_define(self, expr) ||
           SymbCache_is_symbol_symbol_type(self, expr)) {
    return expr;
  }
  else if (SymbCache_is_symbol_define(self, expr)) {
    node_ptr body, ctx;

    body = SymbCache_get_define_body(self, expr);
    ctx = SymbCache_get_define_context(self, expr);

    if ((Nil != body) && ((ATOM == node_get_type(body)) ||
                          (DOT == node_get_type(body)) ||
                          (BIT == node_get_type(body)))) {
      if (_resolve_define_chain_is_constant(self, body)) return body;
      else {
        node_ptr name;

        name = CompileFlatten_resolve_name(self->symb_table, body, ctx);

        if (SymbCache_is_symbol_define(self, name)) {
          /* It is a define, try to recur */
          return SymbCache_resolve_define_chain(self, name);
        }
        else {
          return _resolve_define_chain(self, name);
        }
      }
    }
    return expr;
  }
  else if (SymbCache_is_symbol_parameter(self, expr)) {
    node_ptr body, ctx;

    body = SymbCache_get_actual_parameter(self, expr);
    ctx = SymbCache_get_actual_parameter_context(self, expr);
        
    if ((Nil != body) && ((ATOM == node_get_type(body)) ||
                          (DOT == node_get_type(body)) ||
                          (BIT == node_get_type(body)) || 
                          _resolve_define_chain_is_constant(self, body))) {
      if (_resolve_define_chain_is_constant(self, body)) return body;
      else {
        node_ptr name;

        name = CompileFlatten_resolve_name(self->symb_table, body, ctx);
        if (SymbCache_is_symbol_define(self, name)) {
          /* It is a define, try to recur */
          return SymbCache_resolve_define_chain(self, name);
        }
        else {
          return _resolve_define_chain(self, name);
        }
      }
    }
    return expr;
  }
  else {
    nusmv_assert(false);
  }
  return expr;
}


/**Function********************************************************************

  Synopsis           [Returns true if the expression is a constant, otherwise 
  it returns false]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static boolean _resolve_define_chain_is_constant(SymbCache_ptr self,
                                                 node_ptr exp)
{
  nusmv_assert(CONTEXT != node_get_type(exp));

  return NUMBER_SIGNED_WORD == node_get_type(exp) || 
    NUMBER_UNSIGNED_WORD == node_get_type(exp) ||
    TRUEEXP == node_get_type(exp) ||
    FALSEEXP == node_get_type(exp) ||
    NUMBER == node_get_type(exp) ||
    SymbTable_is_symbol_constant(self->symb_table, exp);
}
