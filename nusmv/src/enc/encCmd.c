/**CFile***********************************************************************

  FileName    [encCmd.c]

  PackageName [enc]

  Synopsis    [The shell interface of the ENC package]

  Description [Shell interface of the ENC package.]

  SeeAlso     []

  Author      [Andrei Tchaltsev]

  Copyright   [
  This file is part of the ``enc'' package of NuSMV version 2. 
  Copyright (C) 2009 by FBK-IRST. 

  NuSMV version 2 is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Lesser General Public 
  License as published by the Free Software Foundation; either 
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with this library; if not, write to the Free Software 
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/

#include "encInt.h"
#include "prop/Prop.h"
#include "prop/propPkg.h"

#include "cmd/cmd.h" /* for Cmd_CommandAdd */
#include "compile/compile.h" /* for cmp_struct_get_encode_variables */
#include "parser/parser.h"

static char rcsid[] UTIL_UNUSED = "$Id nusmv Exp$";

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

static int CommandCleanBddCache ARGS((int argc, char **argv));
static int UsageCleanBddCache ARGS((void));

static int CommandPrintFormula ARGS((int argc, char** argv));
static int UsagePrintFormula ARGS(());

/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

void enc_add_commands()
{
  Cmd_CommandAdd("clean_bdd_cache", CommandCleanBddCache, 0, true);
  Cmd_CommandAdd("print_formula",
                 CommandPrintFormula, 0, true);
}

/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Implements the cleaning BDD cache]

  CommandName        [clean_bdd_cache]

  CommandSynopsis    [Cleans the cache used during evaluation of 
  expressions to ADD and BDD representations.]  

  CommandArguments   [\[-h\] ]  

  CommandDescription [ During conversion of symbolic (node_ptr)
  expressions to ADD and BDD representations the results of
  evaluations are normally cached (see additionally NuSMV option
  enable_bdd_cache). This allows to save time by avoid the
  construction of BDD for the same expression several time.
  
  In some situations it  may be preferable to clean the cache and
  free collected ADD and BDD. This operation can be done, for example,
  to free some memory. Another possible reason is that dynamic
  reordering may modify all existing BDDs, and cleaning the cache
  thereby freeing the BDD may speed up the reordering.

  This command is designed specifically to free the internal cache of
  evaluated expressions and their ADDs and BDDs.
  
  Note that only the cache of exp-to-bdd evaluator is freed.  BDDs of
  variables, constants and expressions collected in BDD FSM or
  anywhere else are not touched.]

  SideEffects        []

******************************************************************************/
static int CommandCleanBddCache(int argc, char **argv)
{
  BddEnc_ptr enc;
  int c;
  extern cmp_struct_ptr cmps;

  /* Parse the options. */
  util_getopt_reset();
  while ((c = util_getopt(argc, argv, "h")) != EOF) {
    switch (c) {
    case 'h': return(UsageCleanBddCache());
    default:
      return(UsageCleanBddCache());
    }
  }

  if (argc != util_optind) return(UsageCleanBddCache());

  /* pre-conditions: */
  if (!cmp_struct_get_encode_variables(cmps)) {
    fprintf(nusmv_stderr, "ERROR: BDD encoding has to be created before. "
            "Use \"encode_variables\" command.\n\n");
    return UsageCleanBddCache();
  }

  enc = Enc_get_bdd_encoding();
  nusmv_assert(enc != NULL);

  BddEnc_clean_evaluation_cache(enc);

  return 0;
}


static int UsageCleanBddCache(void)
{
  fprintf(nusmv_stderr, "usage: clean_bdd_cache [-h]\n");
  fprintf(nusmv_stderr, "   -h \t\tPrints the command usage\n"
          "The command cleans the cache and frees intermediate BDDs constructed\n"
          "during evaluation of symbolic expressions into ADD and BDD form\n");
  return 1;
}


/**Function********************************************************************

  Synopsis           [Prints a formula in canonical format.]

  CommandName        [print_formula]

  CommandSynopsis    [Prints a formula]

  CommandArguments   [\[-h\] | \[-v\] | \[-f \] <expression>]

  CommandDescription [In formula mode, the formula as the canonical
                      formula is printed.  In verbose mode, the
                      explicit assignments satisfying the formula are
                      printed. Prints the number of satsfying
                      assignments for the given formula.<p>

  Command Options:
  <dl>
  <dt> <tt>-v</tt>
  <dd> Verbosely prints the list of assignments satisfying the formula.
  <dt> <tt>-f</tt>
  <dd> Prints a canonical representation of input.
  </dl>]
]

  SideEffects        []

******************************************************************************/
int CommandPrintFormula(int argc, char **argv)
{
  int c;
  boolean verbose = false;
  boolean formula = false;
  char* str_constr = (char*) NULL;
  int parse_result;
  node_ptr constr = Nil;

  /* Parse the command line */
  util_getopt_reset();
  while ((c = util_getopt(argc, argv, "hvf")) != EOF) {
    switch (c) {
    case 'h': return UsagePrintFormula();
    case 'v': verbose = true; break;
    case 'f': formula = true; break;
    default:
      return UsagePrintFormula();
    }
  }

  if (util_optind < argc) {
    str_constr = util_strsav(argv[util_optind]);
  }

  if (verbose && formula) {
    UsagePrintFormula();
    goto CommandPrintFormula_fail;
  }

  if ((char *)NULL == str_constr || 0 == strlen(str_constr)) {
    fprintf(nusmv_stderr, "No expression given.\n\n");
    UsagePrintFormula();
    goto CommandPrintFormula_fail;
  }

  if (Compile_check_if_model_was_built(nusmv_stderr, true)) {
    goto CommandPrintFormula_fail;
  }

  { /* parsing given formula */
    const char* arv[2];
    const int arc = 2;

    arv[0] = (char*) NULL;
    arv[1] = (char*) str_constr;
    parse_result = Parser_ReadCmdFromString(arc, arv, "SIMPWFF ",
                                            ";", &constr);
  }

  if (parse_result != 0 || constr == Nil) {
    fprintf(nusmv_stderr, "Parsing error: expected an expression.\n" );
    goto CommandPrintFormula_fail;
  }

  /* check that the expression is a predicate */
  {
    BddEnc_ptr bdd_enc = Enc_get_bdd_encoding();

    SymbTable_ptr st = BaseEnc_get_symb_table(BASE_ENC(bdd_enc));
    TypeChecker_ptr tc = SymbTable_get_type_checker(st);

    if (SymbType_is_boolean_enum(
            TypeChecker_get_expression_type(tc, car(constr), Nil))) {

    fprintf(nusmv_stdout,
    "######################################################################\n");

    BddEnc_print_formula_info(bdd_enc, car(constr),
                              verbose, formula, nusmv_stdout);
    fprintf(nusmv_stdout,
    "######################################################################\n");

    } /* if SymbType_is_boolean_enum */
    else  {
      fprintf(nusmv_stderr, "expression is not a predicate.\n");
    }
  }

 CommandPrintFormula_fail:
  if ((char *)NULL != str_constr) FREE(str_constr);

  return 0;
}

static int UsagePrintFormula()
{
  fprintf(nusmv_stderr, "usage: print_formula [-h] | [-v] | [-f] <expr>\n");
  fprintf(nusmv_stderr, "   -h \t\tPrints the command usage.\n");
  fprintf(nusmv_stderr, "   -v \t\tPrints explicit models of the formula.\n");
  fprintf(nusmv_stderr,
          "   -f \t\tPrints the simplified and canonical formula.\n");
  return 1;
}

