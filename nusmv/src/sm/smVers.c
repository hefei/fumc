/**CFile***********************************************************************

  FileName    [smVers.c]

  PackageName [sm]

  Synopsis    [Supplies the compile date and version information.]

  Author      [Adapted to NuSMV by Marco Roveri]

  Copyright   [
  This file is part of the ``sm'' package of NuSMV version 2. 
  Copyright (C) 1998-2001 by CMU and FBK-irst. 

  NuSMV version 2 is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Lesser General Public 
  License as published by the Free Software Foundation; either 
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with this library; if not, write to the Free Software 
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "smInt.h"

static char rcsid[] UTIL_UNUSED = "$Id: smVers.c,v 1.3.6.2.4.2.6.1 2007-12-20 17:12:03 nusmv Exp $";

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/
#ifndef PACKAGE_BUILD_DATE
#define PACKAGE_BUILD_DATE  "<compile date not supplied>"
#endif

#ifndef PACKAGE_STRING
#define PACKAGE_STRING         "NuSMV 2.2.x"
#endif

#if HAVE_SOLVER_ZCHAFF
# define PACKAGE_STRING_POSTFIX " zchaff"
#else
# define PACKAGE_STRING_POSTFIX ""
#endif

#ifndef NUSMV_SHARE_PATH
# ifdef DATADIR
#  define NUSMV_SHARE_PATH DATADIR "/nusmv"
# else
#  define NUSMV_SHARE_PATH "/usr/share/nusmv"
# endif
#endif


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/

static char * DateReadFromDateString(char * datestr);

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis    [Returns the current NuSMV version.]

  Description [Returns a static string giving the NuSMV version and compilation
  timestamp.  The user should not free this string.]

  SideEffects []

  SeeAlso     [Sm_NuSMVObtainLibrary]

******************************************************************************/
char* Sm_NuSMVReadVersion()
{
  static char version[1024];

  (void) sprintf(version, "%s%s (compiled on %s)",
                 PACKAGE_STRING, PACKAGE_STRING_POSTFIX,
                 PACKAGE_BUILD_DATE);
  return version;
}


/**Function********************************************************************

  Synopsis    [Returns the NuSMV library path.]

  Description [Returns a string giving the directory which contains the
  standard NuSMV library.  Used to find things like the default .nusmvrc, the
  on-line help files, etc. It is the responsibility of the user to free the
  returned string.]

  SideEffects []

  SeeAlso     [Sm_NuSMVReadVersion]

******************************************************************************/
char* Sm_NuSMVObtainLibrary()
{
#if HAVE_GETENV
  char * nusmv_lib_path;

  nusmv_lib_path = getenv("NuSMV_LIBRARY_PATH");
  if (nusmv_lib_path) {
    return util_tilde_expand(nusmv_lib_path);
  } else {
    return util_tilde_expand(NUSMV_SHARE_PATH);
  }
#else
#warning "Support of Sm_NuSMVObtainLibrary is poor"
  return util_tilde_expand(NUSMV_SHARE_PATH);
#endif
}

/**Function********************************************************************

  Synopsis           [Start piping stdout through the "more" command]

  Description        [This function is  called to initialize piping
  stdout through "more". It is important to call Sm_NuSMVEndPrintMore before
  returning from your function and after
  calling Sm_NuSMVInitPrintMore (preferably at the end of your printing;
  failing to do so will cause the stdin lines not to appear).]

  SideEffects        []

  SeeAlso            [ Sm_NuSMVEndPrintMore]

******************************************************************************/
void Sm_NuSMVInitPrintMore()
{
    fflush(nusmv_stdout);
#if HAVE_POPEN
    nusmv_stdpipe = popen("more","w");
#else
#warning "Pipes are not supported"
#endif
}
/**Function********************************************************************

  Synopsis           [Stop piping stdout through the "more" command]

  Description        [This function is  called to terminate piping
  stdout through "more". It is important to call Sm_NuSMVEndPrintMore  before exiting
  your function (preferably at the end of your printing; failing to do so will cause
  the stdin lines not to appear). The function returns a 0 if it fails.]

  SideEffects        []

  SeeAlso            [ Sm_NuSMVInitPrintMore]

******************************************************************************/
int Sm_NuSMVEndPrintMore()
{
#if HAVE_POPEN
    if (nusmv_stdpipe != NIL(FILE)) {
      (void) pclose(nusmv_stdpipe);
      return 1;
    }
    return 0;
#else
    return 1;
#endif
}


/**Function********************************************************************

  Synopsis    [Prints the banner of NuSMV.]

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
void Sm_BannerPrint(FILE * file)
{
  /* Do not print the NuSMV banner if in quiet mode.. */
  if (opt_get_quiet_mode(OptsHandler_get_instance())) return;

  fprintf(file, "*** This is %s\n", Sm_NuSMVReadVersion());
# ifdef LINKED_ADDONS
  /* linked addons are printed only if not empty */
  if (strcmp(LINKED_ADDONS, "") != 0) {
    fprintf(file, "*** Enabled addons are: %s\n", LINKED_ADDONS);
  }
#endif
  fprintf(file, "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n");
  fprintf(file, "*** or email to <nusmv-users@list.fbk.eu>.\n");
  fprintf(file, "*** Please report bugs to <%s>.\n\n", PACKAGE_BUGREPORT);

  /* Cudd license */
  Sm_BannerPrint_cudd(file, "NuSMV");

# if HAVE_SOLVER_MINISAT
  Sm_BannerPrint_minisat(file, "NuSMV");
# endif

# if HAVE_SOLVER_ZCHAFF
  Sm_BannerPrint_zchaff(file, "NuSMV");
# endif

  fflush(NULL); /* to flush all the banner before any other output */
}


/**Function********************************************************************

  Synopsis    [Prints the banner of the NuSMV library.]

  Description [To be used by addons linking against the NuSMV library.]

  SideEffects []

  SeeAlso     []

******************************************************************************/
void Sm_BannerPrint_nusmv_library(FILE * file, char* name)
{
  fprintf(file, "*** This version of %s is linked to %s.\n", name, PACKAGE_STRING);
  fprintf(file, "*** For more information on NuSMV see <http://nusmv.fbk.eu>\n");
  fprintf(file, "*** or email to <nusmv-users@list.fbk.eu>.\n");
  fprintf(file, "*** Copyright (c) 2010, Fondazione Bruno Kessler\n\n");

  fflush(NULL); /* to flush all the banner before any other output */
}


/**Function********************************************************************

  Synopsis    [Prints the banner of cudd.]

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
void Sm_BannerPrint_cudd(FILE * file, char* name)
{
  fprintf(file,
          "*** This version of %s is linked to the CUDD library version %s\n",
          name,
          CUDD_VERSION);
  fprintf(file,
          "*** Copyright (c) 1995-2004, Regents of the University of Colorado\n\n");

  fflush(NULL); /* to flush all the banner before any other output */
}


/**Function********************************************************************

  Synopsis    [Prints the banner of minisat.]

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
void Sm_BannerPrint_minisat(FILE * file, char* name)
{
  fprintf(file,
          "*** This version of %s is linked to the MiniSat SAT solver. \n",
          name);
  fprintf(file,
          "*** See http://www.cs.chalmers.se/Cs/Research/FormalMethods/MiniSat\n");
  fprintf(file,
          "*** Copyright (c) 2003-2005, Niklas Een, Niklas Sorensson \n\n");

  fflush(NULL); /* to flush all the banner before any other output */
}


/**Function********************************************************************

  Synopsis    [Prints the banner of zchaff.]

  Description []

  SideEffects []

  SeeAlso     []

******************************************************************************/
void Sm_BannerPrint_zchaff(FILE * file, char* name)
{
  int i;

  fprintf(file,
          "WARNING *** This version of %s is linked to the zchaff SAT",
          name);
  for (i = 0; i < 13 - strlen(name); i++) {
    fprintf(file, " ");
  }
  fprintf(file, " ***\n");
  fprintf(file, "WARNING *** solver (see http://www.princeton.edu/~chaff/zchaff.html). ***\n");
  fprintf(file, "WARNING *** Zchaff is used in Bounded Model Checking when the         ***\n");
  fprintf(file, "WARNING *** system variable \"sat_solver\" is set to \"zchaff\".          ***\n");
  fprintf(file, "WARNING *** Notice that zchaff is for non-commercial purposes only.   ***\n");
  fprintf(file, "WARNING *** NO COMMERCIAL USE OF ZCHAFF IS ALLOWED WITHOUT WRITTEN    ***\n");
  fprintf(file, "WARNING *** PERMISSION FROM PRINCETON UNIVERSITY.                     ***\n");
  fprintf(file, "WARNING *** Please contact Sharad Malik (malik@ee.princeton.edu)      ***\n");
  fprintf(file, "WARNING *** for details.                                              ***\n\n");

  fflush(NULL); /* to flush all the banner before any other output */
}


/*---------------------------------------------------------------------------*/
/* Definition of internal functions                                          */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis    [Returns the date in a brief format assuming its coming from
  the program `date'.]

  Description [optional]

  SideEffects []

******************************************************************************/
static char *
DateReadFromDateString(
  char * datestr)
{
  static char result[25];
  char        day[10];
  char        month[10];
  char        zone[10];
  char       *at;
  int         date;
  int         hour;
  int         minute;
  int         second;
  int         year;

  if (sscanf(datestr, "%s %s %2d %2d:%2d:%2d %s %4d",
             day, month, &date, &hour, &minute, &second, zone, &year) == 8) {
    if (hour >= 12) {
      if (hour >= 13) hour -= 12;
      at = "PM";
    }
    else {
      if (hour == 0) hour = 12;
      at = "AM";
    }
    (void) sprintf(result, "%d-%3s-%02d at %d:%02d %s",
                   date, month, year % 100, hour, minute, at);
    return result;
  }
  else {
    return datestr;
  }
}





