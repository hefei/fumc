/**CSourceFile*****************************************************************

  FileName    [NodeList.c]

  PackageName [utils]

  Synopsis    [This is a class exporting a node_ptr based list, but with 
  a higher level and better performances]

  Description [You can pass from a NodeList to node_ptr by calling the
  method to_node_ptr. Viceversa is supported by calling the
  constructor create_from_list.  Notice that at the moment a minimal
  bunch of functionalities is exported ]

  SeeAlso     [NodeList.h]

  Author      [Roberto Cavada]

  Copyright   [
  This file is part of the ``utils'' package of NuSMV version 2. 
  Copyright (C) 2003 by FBK-irst.

  NuSMV version 2 is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Lesser General Public 
  License as published by the Free Software Foundation; either 
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with this library; if not, write to the Free Software 
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

******************************************************************************/

#include "NodeList.h"
#include "utils.h"
#include "assoc.h"

static char rcsid[] UTIL_UNUSED = "$Id: NodeList.c,v 1.1.2.6.4.7.4.12 2009-07-20 11:42:45 nusmv Exp $";

/*---------------------------------------------------------------------------*/
/* Types definition                                                          */
/*---------------------------------------------------------------------------*/

typedef struct NodeList_TAG 
{
  node_ptr first;

  /* other useful members */
  node_ptr last;
  int len;
  boolean list_owner; /* to control list destruction */

  /* for constant-time search */
  hash_ptr elems_hash; 

} NodeList;


/*---------------------------------------------------------------------------*/
/* Macros definition                                                         */
/*---------------------------------------------------------------------------*/
#define END_LIST_ITERATOR  LIST_ITER(Nil)


/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static void node_list_init ARGS((NodeList_ptr self, node_ptr list));
static void node_list_deinit ARGS((NodeList_ptr self));
static void node_list_copy ARGS((NodeList_ptr self, NodeList_ptr copy));

static ListIter_ptr list_iter_next_free ARGS((const ListIter_ptr self));


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis           [Creates a new list]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr NodeList_create()
{
  NodeList_ptr self = ALLOC(NodeList, 1);
  NODE_LIST_CHECK_INSTANCE(self);

  node_list_init(self, Nil);
  return self;
}


/**Function********************************************************************

  Synopsis [Constructor that creates a new NodeList that is a wrapper
  of the given list.]

  Description [self becomes a user of the given list, meaning that
  when self will be destroyed, it will not free the given list. It is a caller 
  responsability of freeing the passed list when possible.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr NodeList_create_from_list(node_ptr list)
{
  NodeList_ptr self = ALLOC(NodeList, 1);
  NODE_LIST_CHECK_INSTANCE(self);

  node_list_init(self, list);
  return self;
}


/**Function********************************************************************

  Synopsis           [Class destroyer]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void NodeList_destroy(NodeList_ptr self)
{
  NODE_LIST_CHECK_INSTANCE(self);

  node_list_deinit(self);
  FREE(self);
}


/**Function********************************************************************

  Synopsis           [Copies self and returns a new independent instance]

  Description        [Linear time]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr NodeList_copy(const NodeList_ptr self)
{
  NodeList_ptr copy;
  NODE_LIST_CHECK_INSTANCE(self);
  
  copy = ALLOC(NodeList, 1);
  NODE_LIST_CHECK_INSTANCE(copy);

  node_list_copy(self, copy);
  return copy;
}


/**Function********************************************************************

  Synopsis           [Casts to node_ptr based list]

  Description        [Returned pointer does not belong to the caller. Do not 
  change or free the returned list.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr NodeList_to_node_ptr(const NodeList_ptr self)
{
  NODE_LIST_CHECK_INSTANCE(self);
  return self->first;
}


/**Function********************************************************************

  Synopsis           [Appends a new node at the end of the list]

  Description        [Constant time]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void NodeList_append(NodeList_ptr self, node_ptr elem)
{
  node_ptr new;
  int c; 

  NODE_LIST_CHECK_INSTANCE(self);
  
  new = cons(elem, Nil);
  
  if (self->last != Nil) {
    setcdr(self->last, new);
  } 

  self->last = new;  

  if (self->first == Nil) {
    self->first = self->last;
  }
  self->len += 1;  

  c = NodeList_count_elem(self, elem);
  insert_assoc(self->elems_hash, elem, NODE_FROM_INT(c+1));
}


/**Function********************************************************************

  Synopsis           [Prepends a new node at the beginning of the list]

  Description        [Constant time]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void NodeList_prepend(NodeList_ptr self, node_ptr elem)
{
  int c; 
  NODE_LIST_CHECK_INSTANCE(self);

  self->first = cons(elem, self->first);
  if (self->last == Nil)  self->last = self->first;
  self->len += 1;  

  c = NodeList_count_elem(self, elem);
  insert_assoc(self->elems_hash, elem, NODE_FROM_INT(c+1));
}



/**Function********************************************************************

  Synopsis [Inserts the given element before the node pointed by the
  given iterator]

  Description        [Linear time]

  SideEffects        []

  SeeAlso            [insert_after]

******************************************************************************/
void NodeList_insert_before(NodeList_ptr self, ListIter_ptr iter, 
			    node_ptr elem)
{
  ListIter_ptr prev;
  node_ptr node;

  NODE_LIST_CHECK_INSTANCE(self);
  
  prev = LIST_ITER(NULL);
  if (iter != self->first) {
    /* not the first: searches the previous iter */
    prev = NodeList_get_first_iter(self);  
    while (!ListIter_is_end(prev)) {
      if (ListIter_get_next(prev) == iter) break;
       
      prev = ListIter_get_next(prev);
    }
    /* If this fails, iter is not an iterator of list self */
    nusmv_assert(!ListIter_is_end(prev)); 
  }

  if (prev != LIST_ITER(NULL)) {
    node = cons(elem, cdr(prev));
    setcdr((node_ptr) prev, node);
  }
  else {
    /* inserts as head element */
    node = cons(elem, self->first);
    self->first = node;
  }

  /* last is set if node is at the end */
  if (ListIter_is_end(iter)) {
    self->last = node;
  }

  self->len += 1;  
  insert_assoc(self->elems_hash, elem, 
	       NODE_FROM_INT(NodeList_count_elem(self, elem) + 1)); 
}


/**Function********************************************************************

  Synopsis [Inserts the given element after the node pointed by the
  given iterator]

  Description [Constant time. iter must be a valid iterator, and
  cannot point at the end of the list]

  SideEffects ]

  SeeAlso            [insert_before]

******************************************************************************/
void NodeList_insert_after(NodeList_ptr self, ListIter_ptr iter, node_ptr elem)
{
  NODE_LIST_CHECK_INSTANCE(self);
  nusmv_assert(!ListIter_is_end(iter) || (self->first == Nil));
  
  if (self->first == Nil) {
    self->first = self->last = cons(elem, Nil);
  }
  else {
    setcdr((node_ptr) iter, cons(elem, cdr(iter)));
    if ((node_ptr) iter == self->last) {
      self->last = iter;
    }
  }

  self->len += 1;  
  insert_assoc(self->elems_hash, elem, 
	       NODE_FROM_INT(NodeList_count_elem(self, elem) + 1));  
}


/**Function********************************************************************

  Synopsis           [Removes the element pointed by the given iterator]

  Description [The removed element is returned. The given iterator
  won't be usable anymore. Linear time.]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr NodeList_remove_elem_at(NodeList_ptr self, ListIter_ptr iter)
{
  node_ptr elem;
  int count;

  NODE_LIST_CHECK_INSTANCE(self);

  elem = NodeList_get_elem_at(self, iter);
  count = NodeList_count_elem(self, elem); 
  
  if (iter != self->first) {
    ListIter_ptr prev = LIST_ITER(NULL);

    /* not the first: searches the previous iter */
    prev = NodeList_get_first_iter(self);  
    while (!ListIter_is_end(prev)) {
      if (ListIter_get_next(prev) == iter) break;
       
      prev = ListIter_get_next(prev);
    }
    /* If this fails, iter is not an iterator of list self */
    nusmv_assert(!ListIter_is_end(prev)); 
    
    setcdr(prev, cdr(iter));    
    if (iter == self->last) self->last = prev;
  }
  else {
    /* iter points to the first element */
    self->first = cdr(self->first);
    if (iter == self->last) self->last = self->first;
  }

  self->len -= 1;    
  if (count > 1) insert_assoc(self->elems_hash, elem, NODE_FROM_INT(count-1));
  else insert_assoc(self->elems_hash, elem, Nil);

  return elem;
}


/**Function********************************************************************

  Synopsis           [Removes the elements that are found in other list]

  Description [Linear time on the size of self. No iteration is done
  if other is empty.  If not NULL, disposer is called on the removed
  element, passing disposer_arg. If the disposer returns true, the
  removal continues, otherwise it aborts and returns with the list as
  it is at that time. Returns the number of removed elements]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int NodeList_remove_elems(NodeList_ptr self, const NodeList_ptr other, 
                          NodeListPred disposer, void* disposer_arg)
{
  ListIter_ptr iter, prev;
  boolean keep_removing = true;
  int res = 0;

  NODE_LIST_CHECK_INSTANCE(self);

  if (NodeList_get_length(other) == 0) return 0;

  prev = LIST_ITER(NULL);
  iter = NodeList_get_first_iter(self);
  while (!ListIter_is_end(iter) && keep_removing) {
    node_ptr elem = NodeList_get_elem_at(self, iter);

    if (NodeList_belongs_to(other, elem)) { /* it has to be removed */
      int count; 
     
      if (iter == self->first) { /* iter points to the first element */
        nusmv_assert(prev == LIST_ITER(NULL));
        self->first = cdr(self->first);
        if (iter == self->last) self->last = self->first;
      }
      else { /* iter is not the first */
        nusmv_assert(prev != LIST_ITER(NULL));
        setcdr(prev, cdr(iter));    
        if (iter == self->last) self->last = prev;
      }      

      /* fix the element counter */
      count = NodeList_count_elem(self, elem); 
      self->len -= 1;    
      if (count > 1) insert_assoc(self->elems_hash, elem, NODE_FROM_INT(count-1));
      else insert_assoc(self->elems_hash, elem, Nil);
            
      /* advance iterm and frees it if needed */
      if (self->list_owner) { iter = list_iter_next_free(iter); }
      else { iter = ListIter_get_next(iter); }

      /* dispose the element if needed */
      if (disposer != NULL) { keep_removing = disposer(elem, disposer_arg); }

      /* increases the number of removed elements */
      res += 1;
    }
    else { /* the element has not to be removed. Advances iterators */     
      prev = iter;
      iter = ListIter_get_next(iter);
    } 
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Walks through the list, calling given funtion 
  for each element]

  Description        [Returns the number of visited nodes, which can be less
  than the total number of elements since foo can decide to interrupt
  the walking]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int NodeList_foreach(NodeList_ptr self, NODE_LIST_FOREACH_FUN_P foo,
                     void* user_data)
{
  ListIter_ptr iter; 
  boolean cont = true;
  int walks = 0;

  NODE_LIST_CHECK_INSTANCE(self);

  iter = NodeList_get_first_iter(self);
  while ( (! ListIter_is_end(iter)) && cont ) {
    cont = foo(self, iter, user_data);
    ++walks;
    iter = ListIter_get_next(iter);
  }  
  
  return walks;
}


/**Function********************************************************************

  Synopsis           [Returns the number of elements in the list]

  Description        [Constant time]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int NodeList_get_length(const NodeList_ptr self)
{
  NODE_LIST_CHECK_INSTANCE(self);
  return self->len;
}


/**Function********************************************************************

  Synopsis           [Reverses the list]

  Description        [Linear time]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void NodeList_reverse(NodeList_ptr self)
{
  NODE_LIST_CHECK_INSTANCE(self);

  self->last = self->first;
  self->first = reverse(self->first);
}


/**Function********************************************************************

  Synopsis           [Append all the elements in src to self]

  Description        [Cost is linear in the size of src]

  SideEffects        [Content of self will change is src is not empty]

  SeeAlso            []

******************************************************************************/
void NodeList_concat(NodeList_ptr self, const NodeList_ptr src)
{
  ListIter_ptr iter;
  NODE_LIST_CHECK_INSTANCE(self);

  iter = NodeList_get_first_iter(src);
  while (!ListIter_is_end(iter)) {
    NodeList_append(self, NodeList_get_elem_at(src, iter));
    iter = ListIter_get_next(iter);
  }
}

/**Function********************************************************************

  Synopsis           [Append all the elements in src to self, but only if 
  each element does not occur in self already]

  Description        [Cost is linear in the size of src]

  SideEffects        [Content of self may change is src is not empty]

  SeeAlso            []

******************************************************************************/
void NodeList_concat_unique(NodeList_ptr self, const NodeList_ptr src)
{
  ListIter_ptr iter;
  NODE_LIST_CHECK_INSTANCE(self);
  
  for (iter = NodeList_get_first_iter(src);
       !ListIter_is_end(iter); 
       iter = ListIter_get_next(iter)) {
    node_ptr el = NodeList_get_elem_at(src, iter);
    if (!NodeList_belongs_to(self, el)) NodeList_append(self, el);
  }
}


/**Function********************************************************************

  Synopsis           [Returns true if given element belongs to self

  Description [Constant time (cost may depend on the internal hash
  status)]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean NodeList_belongs_to(const NodeList_ptr self, node_ptr elem)
{
  NODE_LIST_CHECK_INSTANCE(self);
  
  return (find_assoc(self->elems_hash, elem) != Nil);
}


/**Function********************************************************************

  Synopsis           [Searches for an element in a list such that
  'pred'(element, 'arg') returns true.]

  Description        [Linear time search is used to
  find an element 'elem' such that function pred(elem, arg) returns 
  true.
  An iterator pointing to the found element is returned.
  If the element is not found then ListIter_is_end will be true on the
  returned iterator.

  If pred is NULL then a search for an element equal to arg will be
  done (as if pred was a pointer-equality predicate). If pred is
  NULL and the searched element does not occur in the list, the
  function returns in constant time.]

  SideEffects        []

  SeeAlso            [ListIter_is_end, NodeList_belongs_to]

******************************************************************************/
ListIter_ptr 
NodeList_search(const NodeList_ptr self, NodeListPred pred, void* arg)
{
  ListIter_ptr iter;

  if (pred == NULL) {
    if (!NodeList_belongs_to(self, (node_ptr) arg)) return END_LIST_ITERATOR;
    NODE_LIST_FOREACH(self, iter) {
      if (NodeList_get_elem_at(self, iter) == (node_ptr) arg) return iter;
    }
  }
  else {
    NODE_LIST_FOREACH(self, iter) {
      if (pred(NodeList_get_elem_at(self, iter), arg)) return iter;
    }
  }

  return iter; /* end of list */
}


/**Function********************************************************************

  Synopsis           [Returns the number of occurrences of the given element]

  Description        [Constant time (cost may depend on the internal hash
  status)]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int NodeList_count_elem(const NodeList_ptr self, node_ptr elem)
{
  int res = 0;
  node_ptr c;
    
  NODE_LIST_CHECK_INSTANCE(self);
  
  c = find_assoc(self->elems_hash, elem);
  if (c != Nil) res = NODE_TO_INT(c);
  
  return res;
}


/**Function********************************************************************

  Synopsis           [Returns the iterator pointing to the first element]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
ListIter_ptr NodeList_get_first_iter(const NodeList_ptr self)
{
  NODE_LIST_CHECK_INSTANCE(self);

  return LIST_ITER(self->first);
}


/**Function********************************************************************

  Synopsis           [Returns the element at the position pointed by iter]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
node_ptr NodeList_get_elem_at(const NodeList_ptr self, const ListIter_ptr iter)
{
  NODE_LIST_CHECK_INSTANCE(self);
  nusmv_assert(iter != END_LIST_ITERATOR);
  
  return car((node_ptr) iter);
}


/**Function********************************************************************

  Synopsis           [Returns a new list that contains all elements of 
  self, after applying function foo to each element]

  Description [Elements are not copied. Returned list must be
  freed by the caller]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr NodeList_map(const NodeList_ptr self, NPFN foo)
{
  NodeList_ptr res;
  ListIter_ptr iter;

  NODE_LIST_CHECK_INSTANCE(self); 
  res = NodeList_create();
  for (iter=NodeList_get_first_iter(self); !ListIter_is_end(iter); 
       iter=ListIter_get_next(iter)) {
    NodeList_append(res, foo(NodeList_get_elem_at(self, iter)));
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Returns a new list that contains all elements of 
  self for which function foo returned true. ]

  Description [Elements are not copied. Returned list must be
  freed by the caller]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
NodeList_ptr NodeList_filter(const NodeList_ptr self, BPFN foo)
{
  NodeList_ptr res;
  ListIter_ptr iter;

  NODE_LIST_CHECK_INSTANCE(self); 
  res = NodeList_create();
  for (iter=NodeList_get_first_iter(self); !ListIter_is_end(iter); 
       iter=ListIter_get_next(iter)) {
    node_ptr el = NodeList_get_elem_at(self, iter);
    if (foo(el)) NodeList_append(res, el);
  }

  return res;
}


/**Function********************************************************************

  Synopsis           [Prints the nodes in the list, separated by spaces]

  Description        [The list must be a list of actual node_ptr]

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void NodeList_print_nodes(const NodeList_ptr self, FILE* out)
{
	ListIter_ptr iter;
	for (iter = NodeList_get_first_iter(self); 
			 !ListIter_is_end(iter); iter = ListIter_get_next(iter)) {
		print_node(out, NodeList_get_elem_at(self, iter));
		fprintf(out, " ");
	}
}



/**Function********************************************************************

  Synopsis           [Returns the following iterator]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
ListIter_ptr ListIter_get_next(const ListIter_ptr self)
{
  nusmv_assert(self != END_LIST_ITERATOR);
  
  return LIST_ITER( cdr((node_ptr) self) );
}


/**Function********************************************************************

  Synopsis           [Returns true if the iteration is given up]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
boolean ListIter_is_end(const ListIter_ptr self)
{
  return (self == END_LIST_ITERATOR);
}


/**Function********************************************************************

  Synopsis           [Returns the end iterator]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
ListIter_ptr ListIter_get_end()
{ return END_LIST_ITERATOR; }


/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Private initializer]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void node_list_init(NodeList_ptr self, node_ptr list) 
{
  node_ptr iter;
  self->first = list;
  self->last = list;
  self->len = 0;

  self->list_owner = (list == Nil);

  self->elems_hash = new_assoc();
  nusmv_assert(self->elems_hash != (hash_ptr) NULL);

  /* finds the last node and updates the hash */
  iter = list;
  while (iter != Nil) {
    int c; 
    node_ptr elem = car(iter);
    self->last = iter; 
    self->len += 1;  

    c = NodeList_count_elem(self, elem);
    insert_assoc(self->elems_hash, elem, NODE_FROM_INT(c+1));
    iter = cdr(iter);
  }

}


/**Function********************************************************************

  Synopsis           [Private deinitializer]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void node_list_deinit(NodeList_ptr self)
{
  nusmv_assert(self->elems_hash != (hash_ptr) NULL);
  if (self->list_owner) { free_list(self->first); }

  self->first = Nil;
  self->last = Nil;
  self->len = 0;  
  free_assoc(self->elems_hash);  
  self->elems_hash = (hash_ptr) NULL;
}


/**Function********************************************************************

  Synopsis           [Private copier]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void node_list_copy(NodeList_ptr self, NodeList_ptr copy)
{
  node_list_init(copy, copy_list(self->first));
  copy->list_owner = true;
}
  


/**Function********************************************************************

  Synopsis           [Returns the following iterator, and frees the current]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static ListIter_ptr list_iter_next_free(const ListIter_ptr self)
{
  ListIter_ptr next;
  nusmv_assert(self != END_LIST_ITERATOR);
  next = ListIter_get_next(self);
  free_node((node_ptr) self);
  return next;
}
