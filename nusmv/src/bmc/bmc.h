/**CHeaderFile*****************************************************************

  FileName    [bmc.h]

  PackageName [bmc]

  Synopsis    [The header file for the <tt>bmc</tt> package]

  Description []

  SeeAlso     []

  Author      [Roberto Cavada]

  Copyright   [
  This file is part of the ``bmc'' package of NuSMV version 2.
  Copyright (C) 2000-2001 by FBK-irst and University of Trento.

  NuSMV version 2 is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.fbk.eu>
  or email to <nusmv-users@fbk.eu>.
  Please report bugs to <nusmv-users@fbk.eu>.

  To contact the NuSMV development board, email to <nusmv@fbk.eu>. ]

  Revision    [$Id: bmc.h,v 1.22.4.2.2.1.2.3.6.5 2010-01-18 14:58:30 nusmv Exp $]

******************************************************************************/

#ifndef _BMC_H
#define _BMC_H

#include "utils/utils.h" /* for EXTERN and ARGS */

/* all BMC modules: */
#include "bmcCmd.h"
#include "bmcBmc.h"
#include "bmcPkg.h"

#include "bmcGen.h"
#include "bmcDump.h"
#include "bmcTableau.h"
#include "bmcModel.h"
#include "bmcConv.h"
#include "bmcWff.h"
#include "bmcCheck.h"
#include "bmcUtils.h"


/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/**Constant********************************************************************

  Synopsis [The names for INVAR solving algorithms (incremental and
  non-incremental).]

  Description        []

  SeeAlso            []

******************************************************************************/
#define BMC_INVAR_ALG_CLASSIC       "classic"
#define BMC_INVAR_ALG_EEN_SORENSSON "een-sorensson"
#define BMC_INVAR_ALG_FALSIFICATION "falsification"
#define BMC_INC_INVAR_ALG_DUAL      "dual"
#define BMC_INC_INVAR_ALG_ZIGZAG    "zigzag"
#define BMC_INC_INVAR_ALG_FALSIFICATION "falsification"
#define BMC_INC_INVAR_ALG_INTERP_SEQ "interp_seq"
#define BMC_INC_INVAR_ALG_INTERPOLANTS "interpolants"

/**Constant********************************************************************

  Synopsis           [The names for INVAR closure strategies.]

  Description        [Currently this applies to DUAL algorithm only]

  SeeAlso            []

******************************************************************************/
#define BMC_INVAR_BACKWARD "backward"
#define BMC_INVAR_FORWARD "forward"

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/



/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/


/**AutomaticEnd***************************************************************/

#endif /* _BMC_H */
